﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountingApplication.Settings
{
    public class Config
    {
    }

    public enum ChucVu
    {
        NhanVien = 1,

        KeToanTruong = 2,

        KeToanBanHang = 3,

        GiamDoc = 4,

        TruongPhong = 5,

        PhoPhong = 6,

        NhanVienKinhDoanh = 7,

        KeToanTongHop = 8
    }

    public static class HangHoa
    {
        public const string HH = "Hàng hóa";

        public const string TP = "Thành phẩm";

        public const string VT = "Vật tư";
    }
}
