﻿using System.Linq;

namespace AccountingApplication.Settings
{
    public static class ViewForm
    {
        public const string DHB = "Đơn hàng bán";
        public const string DHM = "Phiếu thu";
        public const string XKNK = "Xuất kho - nhập kho";
        public const string HDB = "Hóa đơn bán";
        public const string HDM = "Hóa đơn mua";
        public const string GG = "Giảm giá";
        public const string NKTL = "Nhập kho trả lại";

        public static string[] NghiepVu = 
        { DHB, 
          DHM,
          XKNK,
          HDB,
          HDM,
          NKTL,
          GG
        };

        public const string DSDHM = "Danh sách đơn hàng mua";
        public const string DSDHB = "Danh sách đơn hàng bán";
        public const string DSHDM = "Danh sách hóa đơn mua";
        public const string DSHDB = "Danh sách hóa đơn bán";
        public const string DSHDGG = "Danh sách hóa đơn giảm giá";
        public const string DSPNX = "Danh sách phiếu nhập xuất";

        public static string[] TruyVan =
        {
            DSDHM,
            DSDHB,
            DSHDM,
            DSHDB,
            DSHDGG,
            DSPNX
        };

        public const string DMLNV = "Danh mục loại nghiệp vụ";
        public const string DMHH = "Danh mục hàng hóa";
        public const string DMK = "Danh mục kho";
        public const string DMPTTT = "Danh mục phương thức thanh toán";
        public const string DMNV = "Danh mục nhân viên";
        public const string DMBG = "Danh mục bảng giá";
        public const string DMTK = "Danh mục tài khoản";
        public const string DMKH = "Danh mục khách hàng";

        public static string[] DanhMuc =
        {
            DMLNV,
            DMHH,
            DMHH,
            DMK,
            DMPTTT,
            DMNV,
            DMBG,
            DMTK,
            DMKH
        };

        public const string PQ = "Phân quyền tính năng";

        public static string[] PhanQuyen =
        {
            PQ
        };

        public const string NKBH = "Nhật ký bán hàng";
        public const string BCCN = "Báo cáo công nợ";
        public const string DTBH = "Doanh thu bán hàng";
        public const string BKPBHPX = "Bảng kê phiếu bán hàng - phiếu xuất";
        public const string BCDS = "Báo cáo doanh số";
        public const string BCLG = "Báo cáo lãi gộp";
        public const string BCTN = "Báo cáo tuổi nợ";
        public const string SQTMTGNH = "Sổ quỹ tiền mặt - tiền gửi ngân hàng";
        public const string THXNT = "Tổng hợp xuất nhập tồn";
        public const string PDK = "Phiếu định khoản";
        public const string SC = "Sổ cái";

        public static string[] BaoCao =
        {
            NKBH,
            BCCN,
            DTBH,
            BKPBHPX,
            BCDS,
            BCLG,
            BCTN,
            SQTMTGNH,
            THXNT,
            PDK,
            SC
        };

        public static bool isHasPermissionInNghiepVu(string menuItem)
        {
            return NghiepVu.Contains(menuItem);
        }

        public static bool isHasPermissionInDanhMuc(string menuItem)
        {
            return DanhMuc.Contains(menuItem);
        }

        public static bool isHasPermissionInTruyVan(string menuItem)
        {
            return TruyVan.Contains(menuItem);
        }

        public static bool isHasPermissionInPhanQuyen(string menuItem)
        {
            return PhanQuyen.Contains(menuItem);
        }

        public static bool isHasPermissionInBaoCao(string menuItem)
        {
            return BaoCao.Contains(menuItem);
        }
    }
}
