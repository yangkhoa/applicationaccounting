﻿using DAL.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountingApplication.Settings
{
    public static class UserManager
    {
        private static TAIKHOANNV currentUser;

        public static TAIKHOANNV CurrentUser
        {
            get { return currentUser; }
            set { currentUser = value; }
        }
    }
}
