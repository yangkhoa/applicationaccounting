﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountingApplication.Settings
{
    public static class Common
    {
        private static dynamic currentBindingList;
        private static dynamic currentUserGet;
        private static bool changePassSuccess;
        private static List<MenuUser> listMenuByUsername;

        public static List<MenuUser> ListMenuByUsername
        {
            get { return listMenuByUsername; }
            set { listMenuByUsername = value; }
        }

        public static bool ChangePassSuccess
        {
            get { return changePassSuccess; }
            set { changePassSuccess = value; }
        }

        public static dynamic CurrentBindingList
        {
            get { return currentBindingList; }
            set { currentBindingList = value; }
        }

        public static dynamic CurrentUserGet
        {
            get { return currentUserGet; }
            set { currentUserGet = value; }
        }

        //public static dynamic currentGetObject
        //{
        //    get { return currentGetObject; }
        //    set { currentGetObject = value; }
        //}
        //public static int currentMasterForm
        //{
        //    get { return currentMasterForm; }
        //    set { currentMasterForm = value; }
        //}

        public static void Clear()
        {
            currentBindingList = null;
            currentUserGet = null;
            //currentGetObject = null;
            //currentMasterForm = 0;
        }
    }
}
