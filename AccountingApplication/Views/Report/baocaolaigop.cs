﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using BLL.Models.ReportFormModel;
using BLL.Services;
using AccountingApplication.Settings;
using AccountingApplication.Views.DocumentView;

namespace AccountingApplication.Views.Report
{
    public partial class baocaolaigop : DevExpress.XtraEditors.XtraUserControl
    {
        public baocaolaigop()
        {
            Text = ViewForm.BCLG;
            InitializeComponent();
            dateEdit1.DateTime = DateTime.Now;
            dateEdit2.DateTime = DateTime.Now;
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }

        public BindingList<LoadBCLG> GetDataSource(string _SOCT,string dateFrom,string dateTo)
        {
            List<LoadBCLG> records = ReportService.Instance.GetBCLG(_SOCT,dateFrom,dateTo);

            BindingList<LoadBCLG> recordRevenues = new BindingList<LoadBCLG>();

            foreach (LoadBCLG record in records)
            {
                recordRevenues.Add(record);
            }

            return recordRevenues;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string _SOCT = textEdit1.Text;
            string dateFrom = dateEdit1.EditValue.ToString();
            string dateTo = dateEdit2.EditValue.ToString();
            BindingList<LoadBCLG> dataSource = GetDataSource(_SOCT,dateFrom,dateTo);

            gridView.OptionsView.EnableAppearanceEvenRow = true;
            gridView.OptionsView.EnableAppearanceOddRow = true;
            gridControl.DataSource = dataSource;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }

        public List<LoadBCLG> GetReportSource(string _SOCT,string datefrom,string dateto)
        {
            List<LoadBCLG> records = ReportService.Instance.GetBCLG(_SOCT,datefrom,dateto);
            return records;
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            string _SOCT = textEdit1.Text;
            string dateFrom = dateEdit1.EditValue.ToString();
            string dateTo = dateEdit2.EditValue.ToString();
            List<LoadBCLG> records = GetReportSource(_SOCT, dateFrom, dateTo);
            //LoadPDKMaster pDKMaster = ReportService.Instance.GetPDKMaster(_SOCT);
            using (BCLGForm lD = new BCLGForm())
            {
                lD.printInvoice(records);
                lD.ShowDialog();
            }
        }
    }
}
