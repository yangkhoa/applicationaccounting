﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using BLL.Models.ReportFormModel;
using BLL.Services;
using AccountingApplication.Settings;
using AccountingApplication.Views.DocumentView;

namespace AccountingApplication.Views.Report
{
    public partial class TienmatTiengui : DevExpress.XtraEditors.XtraUserControl
    {
        public TienmatTiengui()
        {
            InitializeComponent();
            Text = ViewForm.SQTMTGNH;

        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        public BindingList<LoadTMTG> GetDataSource()
        {
            string MACT = textEditSOCT.Text;

                List<LoadTMTG> TMTG = ReportService.Instance.GETTMTG(MACT);
                BindingList<LoadTMTG> result = new BindingList<LoadTMTG>(TMTG);
                return result;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
          

                barButtonItem1.Enabled = true;
                BindingList<LoadTMTG> dataSource = GetDataSource();
                gridControl.DataSource = dataSource;
                gridView.OptionsView.EnableAppearanceEvenRow = true;
                gridView.OptionsView.EnableAppearanceOddRow = true;
                bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
           
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            string SOCT = textEditSOCT.Text;
            
            List<LoadTMTG> THNXT = ReportService.Instance.GETTMTG(SOCT);
            using (TMTGDocument lD = new TMTGDocument())
            {
                lD.printInvoice(THNXT);
                lD.ShowDialog();
            }
        }
    }
}
