﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using BLL.Services;
using BLL.Models;
using AccountingApplication.Settings;

namespace AccountingApplication.Views.Report
{
    public partial class ReportRevenueUC : DevExpress.XtraEditors.XtraUserControl
    {
        public ReportRevenueUC()
        {
            InitializeComponent();
            Text = ViewForm.BCDS;

            //BindingList<RecordRevenue> dataSource = GetDataSource();
            //gridControl.DataSource = dataSource;

            //bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;   
        }

        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }

        //public BindingList<RecordRevenue> GetDataSource()
        //{
        //    List<RecordRevenue> records = ReportService.Instance.GetBaoCaoDoanhThu();

        //    BindingList<RecordRevenue> recordRevenues = new BindingList<RecordRevenue>();

        //    foreach(RecordRevenue record in records)
        //    {
        //        recordRevenues.Add(record);
        //    }
            
        //    return recordRevenues;
        //}
    }
}
