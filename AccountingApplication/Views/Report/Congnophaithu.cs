﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using BLL.Models.ReportFormModel;
using BLL.Services;
using AccountingApplication.Settings;
using DevExpress.XtraGrid.Views.Grid;
using AccountingApplication.Views.DocumentView;

namespace AccountingApplication.Views.Report
{
    public partial class Congnophaithu : DevExpress.XtraEditors.XtraUserControl
    {
        public Congnophaithu()
        {
            InitializeComponent();
            loadForm();
            dateEdit1.DateTime = DateTime.Now;
            dateEdit2.DateTime = DateTime.Now;
            Text = ViewForm.BCCN;
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }

        private void loadForm()
        {
            lookUpEdit1.Properties.DataSource = ObjectService.Instance.GetKHACHHANGs();
            lookUpEdit1.Properties.DisplayMember = "MAKH";
            lookUpEdit1.Properties.ValueMember = "TENKH";
            lookUpEdit1.Properties.NullText = "";
        }
        public BindingList<LoadCNPT> GetDataSource()
        {
            string MAKH = lookUpEdit1.Text;

            string Datetime1 = dateEdit1.EditValue.ToString();
            string datetime2 = dateEdit2.EditValue.ToString();
            List<LoadCNPT> THNXT = ReportService.Instance.GetCNPT(MAKH, Datetime1, datetime2);
            BindingList<LoadCNPT> result = new BindingList<LoadCNPT>(THNXT);
            return result;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {


            BindingList<LoadCNPT> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn) != null && view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString() != String.Empty)
                    Clipboard.SetText(view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString());
                else
                    XtraMessageBox.Show("The value in the selected cell is null or empty!");
                e.Handled = true;
            }
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            string MAHH = lookUpEdit1.Text;

            string Datetime1 = dateEdit1.EditValue.ToString();
            string datetime2 = dateEdit2.EditValue.ToString();
            List<LoadCNPT> CNPT = ReportService.Instance.GetCNPT(MAHH, Datetime1, datetime2);
            using (CNPTDocument lD = new CNPTDocument())
            {
                lD.printInvoice(CNPT, Datetime1, datetime2);
                lD.ShowDialog();
            }
        }
    }
}
