﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using BLL.Models.ReportFormModel;
using BLL.Services;
using AccountingApplication.Settings;
using AccountingApplication.Views.DocumentView;
using DAL.DBContext;

namespace AccountingApplication.Views.Report
{
    public partial class SOCAI : DevExpress.XtraEditors.XtraUserControl
    {
        public SOCAI()
        {
            InitializeComponent();
            Text = ViewForm.SC;
            loadForm();
            labelControl1.BackColor = Color.Transparent;
            Point pos = this.PointToScreen(labelControl1.Location);
            labelControl1.Parent = ribbonControl;
            labelControl1.Location = ribbonControl.PointToClient(pos);

            dateEdit1.DateTime = DateTime.Now;
            dateEdit2.DateTime = DateTime.Now;
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }

        private void loadForm()
        {
            lookUpEdit1.Properties.DataSource = ObjectService.Instance.GetTAIKHOANs();
            lookUpEdit1.Properties.DisplayMember = "MATK";
            lookUpEdit1.Properties.ValueMember = "TENTK";
            lookUpEdit1.Properties.NullText = "111";
        }

        public BindingList<LoadSC> GetDataSource()
        {

            string MATK = lookUpEdit1.Text;

            string Datetime1 = dateEdit1.EditValue.ToString();
            string datetime2 = dateEdit2.EditValue.ToString();
            List<LoadSC> Sc = ReportService.Instance.GetSC(MATK, Datetime1, datetime2);
            BindingList<LoadSC> result = new BindingList<LoadSC>(Sc);
            return result;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

            BindingList<LoadSC> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string SOTK = lookUpEdit1.Text;

                string Datetime1 = dateEdit1.DateTime.Year.ToString();
                string datetime2 = dateEdit1.DateTime.Year.ToString();
                string Year = "";
                if (Datetime1 != datetime2)
                {
                    Year = Datetime1 + " - " + datetime2;
                }
                else
                {
                    Year = Datetime1;
                }

                TAIKHOAN _TK = ObjectService.Instance.GetCTTK(SOTK);
                string loaiTK = _TK.MATK + " - " + _TK.TENTK;
                List<LoadSC> CNPT = ReportService.Instance.GetSC(SOTK, Datetime1, datetime2);
                using (SOCAIForm lD = new SOCAIForm())
                {
                    lD.printInvoice(CNPT, loaiTK, Year);
                    lD.ShowDialog();
                }
            }
            catch (Exception)
            {

                XtraMessageBox.Show("Lỗi hệ thống");
            }
            

        }
    }
}
