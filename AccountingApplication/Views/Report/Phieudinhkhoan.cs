﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using BLL.Models.ReportFormModel;
using BLL.Services;
using AccountingApplication.Views.DocumentView;
using AccountingApplication.Settings;

namespace AccountingApplication.Views.Report
{
    public partial class Phieudinhkhoan : DevExpress.XtraEditors.XtraUserControl
    {
        public Phieudinhkhoan()
        {
            Text = ViewForm.PDK;
            InitializeComponent();

           
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        public BindingList<LoadPDK> GetDataSource()
        {
            string _SOCT = txbSP.Text;
            List<LoadPDK> PDK = ReportService.Instance.getPDK(_SOCT);
            BindingList<LoadPDK> result = new BindingList<LoadPDK>(PDK);
            return result;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            barButtonItem1.Enabled = true;
            BindingList<LoadPDK> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            gridView.OptionsView.EnableAppearanceEvenRow = true;
            gridView.OptionsView.EnableAppearanceOddRow = true;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }
        public List<LoadPDK> GetReportSource(string _SOCT)
        {
            List<LoadPDK> records = ReportService.Instance.getPDK(_SOCT);
            return records;
        }
        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            string _SOCT = txbSP.Text;

            List<LoadPDK> records = GetReportSource(_SOCT);
            LoadPDKMaster pDKMaster = ReportService.Instance.GetPDKMaster(_SOCT);
            using (PDKDocument lD = new PDKDocument())
            {
                lD.printInvoice(records,pDKMaster);
                lD.ShowDialog();
            }
        }
    }
}
