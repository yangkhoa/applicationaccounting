﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using BLL.Models.ReportFormModel;
using BLL.Services;
using AccountingApplication.Settings;
using AccountingApplication.Views.DocumentView;
using DevExpress.XtraGrid.Views.Grid;
using DAL.DBContext;

namespace AccountingApplication.Views.Report
{
    public partial class Tonghopnhatxuatton : DevExpress.XtraEditors.XtraUserControl
    {
        public Tonghopnhatxuatton()
        {
            InitializeComponent();
            Text = ViewForm.THXNT;
            loadForm();
            labelControl1.BackColor = Color.Transparent;
            Point pos = this.PointToScreen(labelControl1.Location);
            labelControl1.Parent = ribbonControl;
            labelControl1.Location = ribbonControl.PointToClient(pos);

            dateEdit1.DateTime = DateTime.Now;
            dateEdit2.DateTime = DateTime.Now;
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }

        private void loadForm()
        {
            lookUpEdit1.Properties.DataSource = ObjectService.Instance.GetHANGHOAs();
            lookUpEdit1.Properties.DisplayMember = "MAHH";
            lookUpEdit1.Properties.ValueMember = "TENHH";
            lookUpEdit1.Properties.NullText = "";
        }
        public BindingList<LoadTHNXT> GetDataSource()
        {
            string MAHH = lookUpEdit1.Text;

            string Datetime1 = dateEdit1.EditValue.ToString();
            string datetime2 = dateEdit2.EditValue.ToString();
            List<LoadTHNXT> THNXT = ReportService.Instance.GetTHNXT(MAHH,Datetime1,datetime2);
            BindingList<LoadTHNXT> result = new BindingList<LoadTHNXT>(THNXT);
            return result;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            barButtonItem1.Enabled = true;
            BindingList<LoadTHNXT> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            gridView.OptionsView.EnableAppearanceEvenRow = true;
            gridView.OptionsView.EnableAppearanceOddRow = true;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            string MAHH = lookUpEdit1.Text;

            string Datetime1 = dateEdit1.EditValue.ToString();
            string datetime2 = dateEdit2.EditValue.ToString();
            List<LoadTHNXT> THNXT = ReportService.Instance.GetTHNXT(MAHH, Datetime1, datetime2);
            using (THNXTDocument lD = new THNXTDocument())
            {
                lD.printInvoice(THNXT,Datetime1,datetime2);
                lD.ShowDialog();
            }
        }

        private void gridView_MasterRowEmpty(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventArgs e)
        {
            GridView view = sender as GridView;
            LoadTHNXT HH = view.GetRow(e.RowHandle) as LoadTHNXT;
            List<HOADON> CTHD = ReportService.Instance.GetHOADONXUATKHO(HH.MAHH.ToString());
            if (HH != null && CTHD != null)
            {
                e.IsEmpty = false;
            }
        }

        private void gridView_MasterRowGetChildList(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventArgs e)
        {
            GridView view = sender as GridView;
            LoadTHNXT HH = view.GetRow(e.RowHandle) as LoadTHNXT;
            List<HOADON> CTHD = ReportService.Instance.GetHOADONXUATKHO(HH.MAHH.ToString());
            if (HH != null)
            {
                e.ChildList = CTHD;
            }
        }

        private void gridView_MasterRowGetRelationCount(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventArgs e)
        {

            e.RelationCount = 1;
        }

        private void gridView_MasterRowGetRelationName(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs e)
        {
            e.RelationName = "Detail";
        }
    }
}
