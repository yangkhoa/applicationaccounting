﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using BLL.Models;
using BLL.Services;
using AccountingApplication.Settings;
using AccountingApplication.Views.DesignReport;
using AccountingApplication.Views.DocumentView;

namespace AccountingApplication.Views.Report
{
    public partial class SalesHistory : DevExpress.XtraEditors.XtraUserControl
    {
        public SalesHistory()
        {
            InitializeComponent();
            Text = ViewForm.NKBH;
            PermissionSetup(ViewForm.NKBH);

            dateEdit1.DateTime = DateTime.Now;
            dateEdit2.DateTime = DateTime.Now;
        }

        private void PermissionSetup(string maNQ)
        {
            List<MenuUser> menus = AccountService.Instance.GetListMenuByRole(UserManager.CurrentUser.MACHUCVU);

            MenuUser menu = menus.FirstOrDefault(x => x.TENNQ == maNQ);

            if (menu != null)
            {
                bbiNew.Enabled = menu.THEM == 1 ? true : false;
                bbiEdit.Enabled = menu.SUA == 1 ? true : false;
                bbiDelete.Enabled = menu.XOA == 1 ? true : false;
            }
        }

        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        public BindingList<RecordSalesHistory> GetDataSource()
        {

            string Datetime1 = dateEdit1.EditValue.ToString();
            string datetime2 = dateEdit2.EditValue.ToString();
            List<RecordSalesHistory> records = ReportService.Instance.GetBaoCaoBanHang(Datetime1,datetime2);

            BindingList<RecordSalesHistory> recordRevenues = new BindingList<RecordSalesHistory>();

            foreach (RecordSalesHistory record in records)
            {
                recordRevenues.Add(record);
            }

            return recordRevenues;
        }

        public List<RecordSalesHistory> GetReportSource()
        {

            string Datetime1 = dateEdit1.EditValue.ToString();
            string datetime2 = dateEdit2.EditValue.ToString();
            List<RecordSalesHistory> records = ReportService.Instance.GetBaoCaoBanHang(Datetime1, datetime2);
            return records;
        }
        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            List<RecordSalesHistory> records = GetReportSource();
            using (ListSaleDocument lD = new ListSaleDocument())
            {
                lD.printInvoice(records);
                lD.ShowDialog();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            BindingList<RecordSalesHistory> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }


    }
}
