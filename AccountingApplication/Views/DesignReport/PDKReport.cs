﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using BLL.Models.ReportFormModel;

namespace AccountingApplication.Views.DesignReport
{
    public partial class PDKReport : DevExpress.XtraReports.UI.XtraReport
    {
        public PDKReport()
        {
            InitializeComponent();
        }

        public void initdata(LoadPDKMaster loadPDKMaster)
        {
            pDIENGIAICT.Value = loadPDKMaster.DIENGIAICT;
            pSOCT.Value = loadPDKMaster.SOCT;
            pTENKH.Value = loadPDKMaster.TENKH;
            pNGAYLAPCT.Value = loadPDKMaster.NGAYLAPCT;
        }

    }
}
