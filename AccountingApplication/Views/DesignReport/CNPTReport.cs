﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace AccountingApplication.Views.DesignReport
{
    public partial class CNPTReport : DevExpress.XtraReports.UI.XtraReport
    {
        public CNPTReport()
        {
            InitializeComponent();
        }
        public void initdata(string dateFrom, string DateTo)
        {
            p_DateFrom.Value = DateTime.Parse(dateFrom);
            p_DateTo.Value = DateTo;
        }
    }
}
