﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace AccountingApplication.Views.DesignReport
{
    public partial class THNXTReport : DevExpress.XtraReports.UI.XtraReport
    {
        public THNXTReport()
        {
            InitializeComponent();
        }

        public void initdata(string dateFrom,string DateTo)
        {
            pDateFrom.Value = DateTime.Parse(dateFrom);
            pDateTo.Value = DateTo;
        }
    }
}
