﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using BLL.Models.ReportFormModel;

namespace AccountingApplication.Views.DesignReport
{
    public partial class INVReport : DevExpress.XtraReports.UI.XtraReport
    {
        public INVReport()
        {
            InitializeComponent();
        }
        public void initdata(LoadPDKMaster loadPDKMaster)
        {
            p_TDV.Value = loadPDKMaster.TENKH;
            p_MST.Value = loadPDKMaster.MST;
            p_Diachi.Value = loadPDKMaster.DIACHI;    
        }
    }
}
