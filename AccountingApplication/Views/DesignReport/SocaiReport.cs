﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace AccountingApplication.Views.DesignReport
{
    public partial class SocaiReport : DevExpress.XtraReports.UI.XtraReport
    {
        public SocaiReport()
        {
            InitializeComponent();
        }
        public void initdata(string loaiTK,string Year)
        {
            p_year.Value = Year;
            p_taikhoan.Value =  loaiTK;
        }

    }
}
