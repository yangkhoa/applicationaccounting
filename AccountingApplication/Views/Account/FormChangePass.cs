﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL.Services;
using AccountingApplication.Settings;

namespace AccountingApplication.Views.Account
{
    public partial class FormChangePass : DevExpress.XtraEditors.XtraForm
    {
        private static bool canClose;

        public FormChangePass()
        {
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            string oldPassword = passwordOld.Text;
            string _newPassword = newPassword.Text;
            string _reNewPassword = newPasswordConfirm.Text;

            if (string.IsNullOrEmpty(oldPassword))
            {
                XtraMessageBox.Show("Bạn chưa nhập mật khẩu cũ", "Thông báo");
            }

            if (string.IsNullOrEmpty(_newPassword))
            {
                XtraMessageBox.Show("Bạn chưa nhập mật khẩu mới", "Thông báo");            }

            if (string.IsNullOrEmpty(_reNewPassword))
            {
                XtraMessageBox.Show("Xin hãy xác nhận mật khẩu mới", "Thông báo");
            }

            if (!string.Equals(_newPassword, _reNewPassword))
            {
                XtraMessageBox.Show("Mật khẩu xác nhận chưa trùng khớp với mật khẩu mới", "Thông báo");
            }

            var user = AccountService.Instance.Login(UserManager.CurrentUser.TAIKHOAN, oldPassword);
            if (user != null)
            {
                bool isSuccess = AccountService.Instance.ChangePass(_newPassword, UserManager.CurrentUser.TAIKHOAN);
                if (isSuccess)
                {
                    XtraMessageBox.Show("Đổi password thành công", "Thông báo");
                    Common.ChangePassSuccess = true;
                    canClose = true;
                    this.Close();
                }
                else
                {
                    XtraMessageBox.Show("Đổi password thất bại", "Thông báo");
                }
            }
            else
            {
                XtraMessageBox.Show("Mật khẩu cũ chưa chính xác! Xin vui lòng nhập lại mật khẩu mới", "Thông báo");
            }

            passwordOld.ResetText();
            newPassword.ResetText();
            newPasswordConfirm.ResetText();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            canClose = true;

            this.Close();
        }

        private void FormChangePass_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!canClose)
            {
                e.Cancel = true;
            }
        }
    }
}