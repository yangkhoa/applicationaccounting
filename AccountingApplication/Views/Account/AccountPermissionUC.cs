﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using System.ComponentModel.DataAnnotations;
using AccountingApplication.Settings;
using BLL.Services;
using BLL.Models;
using DAL.DBContext;
using DevExpress.XtraGrid.Views.Grid;

namespace AccountingApplication.Views
{
    public partial class AccountPermissionUC : DevExpress.XtraEditors.XtraUserControl
    {
        public bool AllowEdit = false;
        public bool AllowAdd = false;
        MenuUser menu;

        public AccountPermissionUC()
        {
            InitializeComponent();

            InitDataSource((int)ChucVu.NhanVien);

            SetUpLookupChucVu();

            SetUpLookupNhomQuyen();

            InitCustomize();
        }

        private void InitCustomize()
        {
            Text = ViewForm.PQ;

            ///Phân quyền button
            List<MenuUser> menus = AccountService.Instance.GetListMenuByRole(UserManager.CurrentUser.MACHUCVU);
            menu = menus.FirstOrDefault(x => x.TENNQ == ViewForm.PQ);

            if (menu != null)
            {
                AllowAdd = menu.THEM == 1;
                AllowEdit = menu.SUA == 1;
                barButtonDeletePermission.Enabled = menu.XOA == 1 ? true : false;
            }

            gridView.OptionsView.EnableAppearanceEvenRow = true;
            gridView.OptionsView.EnableAppearanceOddRow = true;

            label1.BackColor = Color.Transparent;

            ribbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Never;
        }

        private void InitDataSource(int idCV) 
        {
            gridControl.DataSource = GetMenus(idCV);
        }

        private void SetUpLookupChucVu()
        {
            List<Role> roles = AccountService.Instance.GetListRole();
            lookUpEditRole.Properties.DataSource = roles;
            lookUpEditRole.Properties.ValueMember = "IDCV";
            lookUpEditRole.Properties.DisplayMember = "TENCHUCVU";
            lookUpEditRole.EditValue = (int)ChucVu.NhanVien;
            lookUpEditRole.Properties.NullText = roles.FirstOrDefault(x => x.IDCV == (int)ChucVu.NhanVien).TENCHUCVU;
        }

        private void SetUpLookupNhomQuyen()
        {
            List<MenuUser> menus = AccountService.Instance.GetListMenu();
            repositoryItemLookUpNhomQuyen.DataSource = menus;
            repositoryItemLookUpNhomQuyen.ValueMember = "MANQ";
            repositoryItemLookUpNhomQuyen.DisplayMember = "TENNQ";
            repositoryItemLookUpNhomQuyen.NullText = "Xin hãy chọn màn hình";
        }

        private BindingList<MenuUser> GetMenus(int role)
        {
            List<MenuUser> menus = AccountService.Instance.GetListMenuByRole((int)role);
            BindingList<MenuUser> result = new BindingList<MenuUser>(menus);
            return result;
        }

        private void lookUpEditRole_EditValueChanged(object sender, EventArgs e)
        {
            int roleId = (int)lookUpEditRole.EditValue;            
            gridControl.DataSource = GetMenus(roleId);
        }

        private void gridView_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            GridView view = sender as GridView;

            int idCV = (int)lookUpEditRole.EditValue;
          
            if (view.IsNewItemRow(e.RowHandle) && AllowAdd)
            {
                try
                {
                    string maNQ = gridView.GetRowCellValue(gridView.FocusedRowHandle, gridView.Columns[0]).ToString();

                    if (XtraMessageBox.Show("Bạn có muốn thêm dòng dữ liệu này ?", "Thông báo", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        MessageModel message = AccountService.Instance.InsertNewPermission(maNQ, idCV);
                        XtraMessageBox.Show(message.Message);
                    }
                }
                catch(Exception ex)
                {
                    XtraMessageBox.Show(ex.Message.ToString());                   
                }
                InitDataSource(idCV);
            }

            if (!view.IsNewItemRow(e.RowHandle) && AllowEdit)
            {
                string maNQ = gridView.GetRowCellValue(gridView.FocusedRowHandle, gridView.Columns[0]).ToString();

                int MOI = (int)gridView.GetRowCellValue(gridView.FocusedRowHandle, gridView.Columns[2]);
                int XOA = (int)gridView.GetRowCellValue(gridView.FocusedRowHandle, gridView.Columns[3]);
                int SUA = (int)gridView.GetRowCellValue(gridView.FocusedRowHandle, gridView.Columns[4]);
                try
                {
                    if (XtraMessageBox.Show("Bạn có muốn chỉnh sửa dòng dữ liệu này ?", "Thông báo", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        QUYEN quyen = new QUYEN
                        {
                            IDCHUCVU = idCV,
                            MANQ = maNQ,
                            THUCHIEN = 1,
                            MOI = MOI,
                            SUA = SUA,
                            XOA = XOA,
                            XEM = 1,
                            INCT = 1
                        };

                        MessageModel message = AccountService.Instance.EditPermission(idCV, quyen);
                        XtraMessageBox.Show(message.Message);
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message.ToString());
                }
                InitDataSource(idCV);
            }
        }

        private void barButtonDeletePermission_ItemClick(object sender, ItemClickEventArgs e)
        {
            int idCV = (int)lookUpEditRole.EditValue;

            if (!gridView.IsNewItemRow(gridView.FocusedRowHandle))
            {
                try
                {
                    if (XtraMessageBox.Show("Bạn có muốn xóa dòng dữ liệu này?", "Thông báo", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        string maNQ = gridView.GetRowCellValue(gridView.FocusedRowHandle, gridView.Columns[0]).ToString();

                        MessageModel message = AccountService.Instance.DeletePermission(idCV, maNQ);
                        XtraMessageBox.Show(message.Message);
                    }
                }
                catch(Exception ex)
                {
                    XtraMessageBox.Show(ex.Message.ToString());
                }
                InitDataSource(idCV);
            }
        }

        private void repositoryItemLookUpNhomQuyen_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                TextEdit textEditor = (TextEdit)sender;

                string maNQ = textEditor.EditValue.ToString();

                NHOMQUYEN nhomQuyen = AccountService.Instance.GetNhomQuyen(maNQ);

                gridView.SetRowCellValue(gridView.FocusedRowHandle, gridView.Columns[1], nhomQuyen.TENNQ);
                gridView.SetRowCellValue(gridView.FocusedRowHandle, gridView.Columns[2], 1);
                gridView.SetRowCellValue(gridView.FocusedRowHandle, gridView.Columns[3], 1);
                gridView.SetRowCellValue(gridView.FocusedRowHandle, gridView.Columns[4], 1);
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message.ToString());
            }
        }
    }
}
