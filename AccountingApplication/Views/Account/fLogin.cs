﻿using AccountingApplication.Settings;
using AccountingApplication.Views;
using BLL.Services;
using DAL.DBContext;
using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AccountingApplication.Views
{
    public partial class fLogin : Form
    {
        public fLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {          
            string username = textUserName.Text;
            string password = textPassword.Text;
            if (Login(username, password))
            {
                SplashScreenManager.ShowForm(this, typeof(fWait), true, true, false);
                fDashboard dashboard = new fDashboard();
                this.Hide();
                SplashScreenManager.CloseForm(false);
                dashboard.ShowDialog();
                this.Show();
            }
            else
            {
                XtraMessageBox.Show("Sai tên tài khoản hoặc mật khẩu!", "Thông báo");
            }
            textUserName.ResetText();
            textPassword.ResetText();
        }
            
        private bool Login(string username, string password)
        {
            TAIKHOANNV user = AccountService.Instance.Login(username, password);
            UserManager.CurrentUser = user;
            return user != null;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void fLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (XtraMessageBox.Show("Bạn có thật sự muốn thoát chương trình?", "Thông báo", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
            {
                e.Cancel = true;
            }
        }
    }
}
