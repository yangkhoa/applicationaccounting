﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL.Services;
using AccountingApplication.Settings;
using DevExpress.XtraEditors.Controls;
using BLL.Models;
using DAL.DBContext;

namespace AccountingApplication.Views.Account
{
    public partial class FormCreateUser : DevExpress.XtraEditors.XtraForm
    {
        private static bool canClose;

        public FormCreateUser()
        {
            InitializeComponent();

            SetupRole();
            SetupNhanVien();
        }

        private void SetupNhanVien()
        {
            var listNhanVien = ObjectService.Instance.GetNHANVIENs().Select(x => new NHANVIEN 
            {
                TENNV = x.TENNV,
                MANV = x.MANV
            }).ToList();

            

            maNV.Properties.DataSource = listNhanVien;
            maNV.Properties.ValueMember = "MANV";
            maNV.Properties.DisplayMember = "TENNV";
            maNV.Properties.NullText = "Xin hãy chọn nhân viên";
        }

        private void SetupRole()
        {
            List<Role> roles = AccountService.Instance.GetListRole();

            ComboBoxItemCollection itemsCollection = comboBoxRole.Properties.Items;
            itemsCollection.BeginUpdate();
            try
            {
                foreach (var item in roles)
                    itemsCollection.Add(item.TENCHUCVU);
            }
            finally
            {
                itemsCollection.EndUpdate();
            }

            comboBoxRole.Properties.NullText = "Xin hãy chọn chức vụ";
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            string maNhanVien = maNV.EditValue.ToString();
            string username = taiKhoanNhanVien.Text;
            string passwordInput = password.Text;
            string confirmPasswordInput = passwordConfirm.Text;
            string roleValue = comboBoxRole.EditValue.ToString();

            if (string.IsNullOrEmpty(roleValue))
            {
                XtraMessageBox.Show("Bạn chưa chọn chức vụ", "Thông báo");
            }

            if (string.IsNullOrEmpty(maNhanVien))
            {
                XtraMessageBox.Show("Bạn chưa nhập mã nhân viên", "Thông báo");
            }

            if (string.IsNullOrEmpty(username))
            {
                XtraMessageBox.Show("Bạn chưa nhập username", "Thông báo");
            }

            if (string.IsNullOrEmpty(passwordInput))
            {
                XtraMessageBox.Show("Bạn chưa nhập mật khẩu", "Thông báo");            }

            if (string.IsNullOrEmpty(confirmPasswordInput))
            {
                XtraMessageBox.Show("Xin hãy xác nhận mật khẩu mới", "Thông báo");
            }

            if (!string.Equals(passwordInput, confirmPasswordInput))
            {
                XtraMessageBox.Show("Mật khẩu xác nhận chưa trùng khớp", "Thông báo");
            }

            var user = AccountService.Instance.GetUserByUsername(username);
            if (user != null)
            {
                XtraMessageBox.Show("Tài khoản này đã tồn tại trong hệ thống!", "Thông báo");
            }
            else
            {
                bool isSuccess = AccountService.Instance.CreateNhanVien(maNhanVien, username, passwordInput, roleValue);
                if (isSuccess)
                {
                    XtraMessageBox.Show("Tạo tài khoản thành công", "Thông báo");
                    canClose = true;
                    this.Close();
                }
                else
                {
                    XtraMessageBox.Show("Tạo tài khoản thất bại", "Thông báo");
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            canClose = true;

            this.Close();
        }

        private void FormCreateUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!canClose)
            {
                e.Cancel = true;
            }
        }
    }
}