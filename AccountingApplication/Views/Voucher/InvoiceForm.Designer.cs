﻿namespace AccountingApplication.Views.Voucher
{
    partial class InvoiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InvoiceForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.textBoxSSR = new System.Windows.Forms.TextBox();
            this.textBoxSHD = new System.Windows.Forms.TextBox();
            this.textBoxDG = new System.Windows.Forms.TextBox();
            this.textBoxSP = new System.Windows.Forms.TextBox();
            this.lookUpEditTK = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditKH = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditNVIEN = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditNV = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gridInvoice = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMAHH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridHHS = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMAHH1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTENHH1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTENHH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridKHO = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTENKHO1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTONKHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTKDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditTK = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMATK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTENTK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSLDH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDGDH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTHANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTYLECK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTIENCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTKCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMATHUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridMT = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTIENTHUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTKTHUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTKDUTHUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGIAVON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTKGV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTIENVON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTKVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coLSODH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditTK.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditKH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditNVIEN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridHHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridKHO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditTK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridMT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1256, 110);
            this.panelControl1.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.dateEdit1);
            this.groupControl1.Controls.Add(this.textBoxSSR);
            this.groupControl1.Controls.Add(this.textBoxSHD);
            this.groupControl1.Controls.Add(this.textBoxDG);
            this.groupControl1.Controls.Add(this.textBoxSP);
            this.groupControl1.Controls.Add(this.lookUpEditTK);
            this.groupControl1.Controls.Add(this.lookUpEditKH);
            this.groupControl1.Controls.Add(this.lookUpEditNVIEN);
            this.groupControl1.Controls.Add(this.lookUpEditNV);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(6, 6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1245, 100);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin hóa đơn";
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(639, 31);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(125, 22);
            this.dateEdit1.TabIndex = 17;
            // 
            // textBoxSSR
            // 
            this.textBoxSSR.Location = new System.Drawing.Point(1101, 35);
            this.textBoxSSR.Name = "textBoxSSR";
            this.textBoxSSR.Size = new System.Drawing.Size(127, 23);
            this.textBoxSSR.TabIndex = 16;
            // 
            // textBoxSHD
            // 
            this.textBoxSHD.Location = new System.Drawing.Point(870, 34);
            this.textBoxSHD.Name = "textBoxSHD";
            this.textBoxSHD.Size = new System.Drawing.Size(133, 23);
            this.textBoxSHD.TabIndex = 15;
            // 
            // textBoxDG
            // 
            this.textBoxDG.Location = new System.Drawing.Point(870, 66);
            this.textBoxDG.Name = "textBoxDG";
            this.textBoxDG.Size = new System.Drawing.Size(358, 23);
            this.textBoxDG.TabIndex = 14;
            // 
            // textBoxSP
            // 
            this.textBoxSP.Enabled = false;
            this.textBoxSP.Location = new System.Drawing.Point(387, 31);
            this.textBoxSP.Name = "textBoxSP";
            this.textBoxSP.Size = new System.Drawing.Size(150, 23);
            this.textBoxSP.TabIndex = 13;
            // 
            // lookUpEditTK
            // 
            this.lookUpEditTK.Location = new System.Drawing.Point(639, 66);
            this.lookUpEditTK.Name = "lookUpEditTK";
            this.lookUpEditTK.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditTK.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MATK", "Mã", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENTK", "Tên tài khoản")});
            this.lookUpEditTK.Size = new System.Drawing.Size(125, 22);
            this.lookUpEditTK.TabIndex = 12;
            // 
            // lookUpEditKH
            // 
            this.lookUpEditKH.Location = new System.Drawing.Point(387, 69);
            this.lookUpEditKH.Name = "lookUpEditKH";
            this.lookUpEditKH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditKH.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MAKH", "Mã", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENKH", "Tên khách hàng", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditKH.Size = new System.Drawing.Size(150, 22);
            this.lookUpEditKH.TabIndex = 11;
            // 
            // lookUpEditNVIEN
            // 
            this.lookUpEditNVIEN.Location = new System.Drawing.Point(131, 69);
            this.lookUpEditNVIEN.Name = "lookUpEditNVIEN";
            this.lookUpEditNVIEN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditNVIEN.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MANV", "MÃ"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENNV", "Tên nhân viên")});
            this.lookUpEditNVIEN.Size = new System.Drawing.Size(125, 22);
            this.lookUpEditNVIEN.TabIndex = 10;
            // 
            // lookUpEditNV
            // 
            this.lookUpEditNV.Location = new System.Drawing.Point(131, 31);
            this.lookUpEditNV.Name = "lookUpEditNV";
            this.lookUpEditNV.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditNV.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MALOAICT", "MÃ", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENLOAICT", "TÊN CHỨNG TỪ", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditNV.Size = new System.Drawing.Size(125, 22);
            this.lookUpEditNV.TabIndex = 9;
            this.lookUpEditNV.EditValueChanged += new System.EventHandler(this.lookUpEditNV_EditValueChanged);
            // 
            // labelControl9
            // 
            this.labelControl9.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl9.ImageOptions.Image")));
            this.labelControl9.Location = new System.Drawing.Point(1009, 28);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(86, 36);
            this.labelControl9.TabIndex = 8;
            this.labelControl9.Text = "Số serial";
            // 
            // labelControl8
            // 
            this.labelControl8.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl8.ImageOptions.Image")));
            this.labelControl8.Location = new System.Drawing.Point(770, 59);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(87, 36);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = "Diễn giải";
            // 
            // labelControl7
            // 
            this.labelControl7.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl7.ImageOptions.Image")));
            this.labelControl7.Location = new System.Drawing.Point(772, 34);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(92, 20);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "Số hóa đơn";
            // 
            // labelControl6
            // 
            this.labelControl6.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl6.ImageOptions.Image")));
            this.labelControl6.Location = new System.Drawing.Point(543, 59);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(73, 36);
            this.labelControl6.TabIndex = 5;
            this.labelControl6.Text = "TK nợ";
            // 
            // labelControl5
            // 
            this.labelControl5.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftTop;
            this.labelControl5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl5.ImageOptions.Image")));
            this.labelControl5.Location = new System.Drawing.Point(262, 67);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(107, 20);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Mã khách hàng";
            // 
            // labelControl4
            // 
            this.labelControl4.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl4.ImageOptions.Image")));
            this.labelControl4.Location = new System.Drawing.Point(0, 59);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(113, 36);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Mã nhân viên";
            // 
            // labelControl3
            // 
            this.labelControl3.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl3.ImageOptions.Image")));
            this.labelControl3.Location = new System.Drawing.Point(543, 37);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 20);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Ngày phiếu";
            // 
            // labelControl2
            // 
            this.labelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl2.ImageOptions.Image")));
            this.labelControl2.Location = new System.Drawing.Point(262, 24);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 36);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Số phiếu";
            // 
            // labelControl1
            // 
            this.labelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.ImageOptions.Image")));
            this.labelControl1.Location = new System.Drawing.Point(1, 21);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(126, 36);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Loại nghiệp vụ";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.groupControl2);
            this.panelControl2.Location = new System.Drawing.Point(12, 168);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1256, 417);
            this.panelControl2.TabIndex = 1;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridInvoice);
            this.groupControl2.Location = new System.Drawing.Point(6, 6);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1245, 406);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "Chi tiết hóa đơn";
            // 
            // gridInvoice
            // 
            this.gridInvoice.Location = new System.Drawing.Point(6, 29);
            this.gridInvoice.MainView = this.gridView1;
            this.gridInvoice.Name = "gridInvoice";
            this.gridInvoice.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemGridHHS,
            this.repositoryItemGridKHO,
            this.repositoryItemGridMT,
            this.repositoryItemGridLookUpEditTK});
            this.gridInvoice.Size = new System.Drawing.Size(1234, 372);
            this.gridInvoice.TabIndex = 0;
            this.gridInvoice.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridInvoice.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridInvoice_ProcessGridKey);
            this.gridInvoice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridInvoice_KeyDown);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMAHH,
            this.colTENHH,
            this.colDVT,
            this.colKHO,
            this.colTONKHO,
            this.colTKDT,
            this.colSLDH,
            this.colDGDH,
            this.colTHANHTIEN,
            this.colTYLECK,
            this.colTIENCK,
            this.colTKCK,
            this.colMATHUE,
            this.colTIENTHUE,
            this.colTKTHUE,
            this.colTKDUTHUE,
            this.colGIAVON,
            this.colTKGV,
            this.colTIENVON,
            this.colTKVT,
            this.coLSODH});
            this.gridView1.GridControl = this.gridInvoice;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            // 
            // colMAHH
            // 
            this.colMAHH.AppearanceHeader.Options.UseTextOptions = true;
            this.colMAHH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMAHH.Caption = "Mã Hàng Hóa";
            this.colMAHH.ColumnEdit = this.repositoryItemGridHHS;
            this.colMAHH.FieldName = "MAHH";
            this.colMAHH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colMAHH.ImageOptions.Image")));
            this.colMAHH.MinWidth = 25;
            this.colMAHH.Name = "colMAHH";
            this.colMAHH.Visible = true;
            this.colMAHH.VisibleIndex = 0;
            this.colMAHH.Width = 150;
            // 
            // repositoryItemGridHHS
            // 
            this.repositoryItemGridHHS.AutoHeight = false;
            this.repositoryItemGridHHS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridHHS.Name = "repositoryItemGridHHS";
            this.repositoryItemGridHHS.PopupView = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMAHH1,
            this.colTENHH1});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colMAHH1
            // 
            this.colMAHH1.AppearanceHeader.Options.UseTextOptions = true;
            this.colMAHH1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMAHH1.Caption = "Mã";
            this.colMAHH1.FieldName = "MAHH";
            this.colMAHH1.Name = "colMAHH1";
            this.colMAHH1.Visible = true;
            this.colMAHH1.VisibleIndex = 0;
            // 
            // colTENHH1
            // 
            this.colTENHH1.AppearanceHeader.Options.UseTextOptions = true;
            this.colTENHH1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTENHH1.Caption = "Tên Hàng";
            this.colTENHH1.FieldName = "TENHH";
            this.colTENHH1.Name = "colTENHH1";
            this.colTENHH1.Visible = true;
            this.colTENHH1.VisibleIndex = 1;
            // 
            // colTENHH
            // 
            this.colTENHH.AppearanceHeader.Options.UseTextOptions = true;
            this.colTENHH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTENHH.Caption = "Tên Hàng Hóa";
            this.colTENHH.FieldName = "TENHH";
            this.colTENHH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTENHH.ImageOptions.Image")));
            this.colTENHH.MinWidth = 25;
            this.colTENHH.Name = "colTENHH";
            this.colTENHH.OptionsColumn.AllowEdit = false;
            this.colTENHH.OptionsColumn.AllowFocus = false;
            this.colTENHH.Visible = true;
            this.colTENHH.VisibleIndex = 1;
            this.colTENHH.Width = 120;
            // 
            // colDVT
            // 
            this.colDVT.AppearanceHeader.Options.UseTextOptions = true;
            this.colDVT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDVT.Caption = "Đơn vị tính";
            this.colDVT.FieldName = "MADVT";
            this.colDVT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colDVT.ImageOptions.Image")));
            this.colDVT.MinWidth = 25;
            this.colDVT.Name = "colDVT";
            this.colDVT.OptionsColumn.AllowEdit = false;
            this.colDVT.OptionsColumn.AllowFocus = false;
            this.colDVT.Visible = true;
            this.colDVT.VisibleIndex = 2;
            this.colDVT.Width = 94;
            // 
            // colKHO
            // 
            this.colKHO.AppearanceHeader.Options.UseTextOptions = true;
            this.colKHO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKHO.Caption = "Kho";
            this.colKHO.ColumnEdit = this.repositoryItemGridKHO;
            this.colKHO.FieldName = "MAKHO";
            this.colKHO.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colKHO.ImageOptions.Image")));
            this.colKHO.MinWidth = 25;
            this.colKHO.Name = "colKHO";
            this.colKHO.Visible = true;
            this.colKHO.VisibleIndex = 3;
            this.colKHO.Width = 94;
            // 
            // repositoryItemGridKHO
            // 
            this.repositoryItemGridKHO.AutoHeight = false;
            this.repositoryItemGridKHO.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridKHO.Name = "repositoryItemGridKHO";
            this.repositoryItemGridKHO.PopupView = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMK,
            this.colTENKHO1});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // colMK
            // 
            this.colMK.Caption = "Mã";
            this.colMK.FieldName = "MAKHO";
            this.colMK.Name = "colMK";
            this.colMK.Visible = true;
            this.colMK.VisibleIndex = 0;
            // 
            // colTENKHO1
            // 
            this.colTENKHO1.Caption = "Tên Kho";
            this.colTENKHO1.FieldName = "TENKHO";
            this.colTENKHO1.Name = "colTENKHO1";
            this.colTENKHO1.Visible = true;
            this.colTENKHO1.VisibleIndex = 1;
            // 
            // colTONKHO
            // 
            this.colTONKHO.Caption = "Tồn Kho";
            this.colTONKHO.FieldName = "TONKHO";
            this.colTONKHO.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTONKHO.ImageOptions.Image")));
            this.colTONKHO.MinWidth = 25;
            this.colTONKHO.Name = "colTONKHO";
            this.colTONKHO.OptionsColumn.AllowEdit = false;
            this.colTONKHO.OptionsColumn.AllowFocus = false;
            this.colTONKHO.OptionsColumn.ReadOnly = true;
            this.colTONKHO.Visible = true;
            this.colTONKHO.VisibleIndex = 4;
            this.colTONKHO.Width = 94;
            // 
            // colTKDT
            // 
            this.colTKDT.AppearanceHeader.Options.UseTextOptions = true;
            this.colTKDT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTKDT.Caption = "Tài khoản có";
            this.colTKDT.ColumnEdit = this.repositoryItemGridLookUpEditTK;
            this.colTKDT.FieldName = "TKDT";
            this.colTKDT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTKDT.ImageOptions.Image")));
            this.colTKDT.MinWidth = 25;
            this.colTKDT.Name = "colTKDT";
            this.colTKDT.Visible = true;
            this.colTKDT.VisibleIndex = 12;
            this.colTKDT.Width = 94;
            // 
            // repositoryItemGridLookUpEditTK
            // 
            this.repositoryItemGridLookUpEditTK.AutoHeight = false;
            this.repositoryItemGridLookUpEditTK.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditTK.Name = "repositoryItemGridLookUpEditTK";
            this.repositoryItemGridLookUpEditTK.PopupView = this.gridView4;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMATK,
            this.colTENTK});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // colMATK
            // 
            this.colMATK.Caption = "Mã";
            this.colMATK.FieldName = "MATK";
            this.colMATK.Name = "colMATK";
            this.colMATK.Visible = true;
            this.colMATK.VisibleIndex = 0;
            // 
            // colTENTK
            // 
            this.colTENTK.Caption = "Tên tài khoản";
            this.colTENTK.FieldName = "TENTK";
            this.colTENTK.Name = "colTENTK";
            this.colTENTK.Visible = true;
            this.colTENTK.VisibleIndex = 1;
            // 
            // colSLDH
            // 
            this.colSLDH.AppearanceHeader.Options.UseTextOptions = true;
            this.colSLDH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSLDH.Caption = "Số Lượng";
            this.colSLDH.FieldName = "SOLUONG";
            this.colSLDH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colSLDH.ImageOptions.Image")));
            this.colSLDH.MinWidth = 25;
            this.colSLDH.Name = "colSLDH";
            this.colSLDH.Visible = true;
            this.colSLDH.VisibleIndex = 5;
            this.colSLDH.Width = 94;
            // 
            // colDGDH
            // 
            this.colDGDH.AppearanceHeader.Options.UseTextOptions = true;
            this.colDGDH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDGDH.Caption = "Đơn Giá";
            this.colDGDH.FieldName = "DONGIA";
            this.colDGDH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colDGDH.ImageOptions.Image")));
            this.colDGDH.MinWidth = 25;
            this.colDGDH.Name = "colDGDH";
            this.colDGDH.Visible = true;
            this.colDGDH.VisibleIndex = 6;
            this.colDGDH.Width = 94;
            // 
            // colTHANHTIEN
            // 
            this.colTHANHTIEN.AppearanceHeader.Options.UseTextOptions = true;
            this.colTHANHTIEN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTHANHTIEN.Caption = "Thành Tiền";
            this.colTHANHTIEN.FieldName = "THANHTIEN";
            this.colTHANHTIEN.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTHANHTIEN.ImageOptions.Image")));
            this.colTHANHTIEN.MinWidth = 25;
            this.colTHANHTIEN.Name = "colTHANHTIEN";
            this.colTHANHTIEN.OptionsColumn.AllowEdit = false;
            this.colTHANHTIEN.OptionsColumn.AllowFocus = false;
            this.colTHANHTIEN.Visible = true;
            this.colTHANHTIEN.VisibleIndex = 7;
            this.colTHANHTIEN.Width = 94;
            // 
            // colTYLECK
            // 
            this.colTYLECK.AppearanceHeader.Options.UseTextOptions = true;
            this.colTYLECK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTYLECK.Caption = "Tỷ lệ CK";
            this.colTYLECK.FieldName = "TYLECK";
            this.colTYLECK.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTYLECK.ImageOptions.Image")));
            this.colTYLECK.MinWidth = 25;
            this.colTYLECK.Name = "colTYLECK";
            this.colTYLECK.Visible = true;
            this.colTYLECK.VisibleIndex = 8;
            this.colTYLECK.Width = 94;
            // 
            // colTIENCK
            // 
            this.colTIENCK.AppearanceHeader.Options.UseTextOptions = true;
            this.colTIENCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTIENCK.Caption = "Tiền Chiết Khấu";
            this.colTIENCK.FieldName = "TIENCK";
            this.colTIENCK.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTIENCK.ImageOptions.Image")));
            this.colTIENCK.MinWidth = 25;
            this.colTIENCK.Name = "colTIENCK";
            this.colTIENCK.OptionsColumn.AllowEdit = false;
            this.colTIENCK.OptionsColumn.AllowFocus = false;
            this.colTIENCK.Visible = true;
            this.colTIENCK.VisibleIndex = 9;
            this.colTIENCK.Width = 120;
            // 
            // colTKCK
            // 
            this.colTKCK.AppearanceHeader.Options.UseTextOptions = true;
            this.colTKCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTKCK.Caption = "Tài khoản chiết khấu";
            this.colTKCK.ColumnEdit = this.repositoryItemGridLookUpEditTK;
            this.colTKCK.FieldName = "TKCK";
            this.colTKCK.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTKCK.ImageOptions.Image")));
            this.colTKCK.MinWidth = 25;
            this.colTKCK.Name = "colTKCK";
            this.colTKCK.Visible = true;
            this.colTKCK.VisibleIndex = 14;
            this.colTKCK.Width = 150;
            // 
            // colMATHUE
            // 
            this.colMATHUE.AppearanceHeader.Options.UseTextOptions = true;
            this.colMATHUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMATHUE.Caption = "Mã Thuế";
            this.colMATHUE.ColumnEdit = this.repositoryItemGridMT;
            this.colMATHUE.FieldName = "MATHUE";
            this.colMATHUE.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colMATHUE.ImageOptions.Image")));
            this.colMATHUE.MinWidth = 25;
            this.colMATHUE.Name = "colMATHUE";
            this.colMATHUE.Visible = true;
            this.colMATHUE.VisibleIndex = 10;
            this.colMATHUE.Width = 94;
            // 
            // repositoryItemGridMT
            // 
            this.repositoryItemGridMT.AutoHeight = false;
            this.repositoryItemGridMT.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridMT.Name = "repositoryItemGridMT";
            this.repositoryItemGridMT.PopupView = this.gridView3;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMT,
            this.colLT});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // colMT
            // 
            this.colMT.Caption = "Mã";
            this.colMT.FieldName = "MATHUE";
            this.colMT.Name = "colMT";
            this.colMT.Visible = true;
            this.colMT.VisibleIndex = 0;
            // 
            // colLT
            // 
            this.colLT.Caption = "Loại Thuế";
            this.colLT.FieldName = "TENTHUE";
            this.colLT.Name = "colLT";
            this.colLT.Visible = true;
            this.colLT.VisibleIndex = 1;
            // 
            // colTIENTHUE
            // 
            this.colTIENTHUE.AppearanceHeader.Options.UseTextOptions = true;
            this.colTIENTHUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTIENTHUE.Caption = "Tiền Thuế";
            this.colTIENTHUE.FieldName = "TIENTHUE";
            this.colTIENTHUE.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTIENTHUE.ImageOptions.Image")));
            this.colTIENTHUE.MinWidth = 25;
            this.colTIENTHUE.Name = "colTIENTHUE";
            this.colTIENTHUE.OptionsColumn.AllowEdit = false;
            this.colTIENTHUE.OptionsColumn.AllowFocus = false;
            this.colTIENTHUE.Visible = true;
            this.colTIENTHUE.VisibleIndex = 11;
            this.colTIENTHUE.Width = 94;
            // 
            // colTKTHUE
            // 
            this.colTKTHUE.AppearanceHeader.Options.UseTextOptions = true;
            this.colTKTHUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTKTHUE.Caption = "Tài khoản thuế";
            this.colTKTHUE.ColumnEdit = this.repositoryItemGridLookUpEditTK;
            this.colTKTHUE.FieldName = "TKTHUE";
            this.colTKTHUE.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTKTHUE.ImageOptions.Image")));
            this.colTKTHUE.MinWidth = 25;
            this.colTKTHUE.Name = "colTKTHUE";
            this.colTKTHUE.Visible = true;
            this.colTKTHUE.VisibleIndex = 13;
            this.colTKTHUE.Width = 150;
            // 
            // colTKDUTHUE
            // 
            this.colTKDUTHUE.AppearanceHeader.Options.UseTextOptions = true;
            this.colTKDUTHUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTKDUTHUE.Caption = "Tài khoản dư thuế";
            this.colTKDUTHUE.ColumnEdit = this.repositoryItemGridLookUpEditTK;
            this.colTKDUTHUE.FieldName = "TKDUTHUE";
            this.colTKDUTHUE.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTKDUTHUE.ImageOptions.Image")));
            this.colTKDUTHUE.MinWidth = 25;
            this.colTKDUTHUE.Name = "colTKDUTHUE";
            this.colTKDUTHUE.Width = 150;
            // 
            // colGIAVON
            // 
            this.colGIAVON.AppearanceHeader.Options.UseTextOptions = true;
            this.colGIAVON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGIAVON.Caption = "Giá vốn";
            this.colGIAVON.FieldName = "GIAVON";
            this.colGIAVON.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colGIAVON.ImageOptions.Image")));
            this.colGIAVON.MinWidth = 25;
            this.colGIAVON.Name = "colGIAVON";
            this.colGIAVON.Width = 94;
            // 
            // colTKGV
            // 
            this.colTKGV.AppearanceHeader.Options.UseTextOptions = true;
            this.colTKGV.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTKGV.Caption = "TK giá vốn";
            this.colTKGV.ColumnEdit = this.repositoryItemGridLookUpEditTK;
            this.colTKGV.FieldName = "TKGV";
            this.colTKGV.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTKGV.ImageOptions.Image")));
            this.colTKGV.MinWidth = 25;
            this.colTKGV.Name = "colTKGV";
            this.colTKGV.Width = 150;
            // 
            // colTIENVON
            // 
            this.colTIENVON.AppearanceHeader.Options.UseTextOptions = true;
            this.colTIENVON.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTIENVON.Caption = "Tiền Vốn";
            this.colTIENVON.FieldName = "TIENVON";
            this.colTIENVON.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTIENVON.ImageOptions.Image")));
            this.colTIENVON.MinWidth = 25;
            this.colTIENVON.Name = "colTIENVON";
            this.colTIENVON.Width = 150;
            // 
            // colTKVT
            // 
            this.colTKVT.AppearanceHeader.Options.UseTextOptions = true;
            this.colTKVT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTKVT.Caption = "TK vật tư";
            this.colTKVT.ColumnEdit = this.repositoryItemGridLookUpEditTK;
            this.colTKVT.FieldName = "TKVT";
            this.colTKVT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTKVT.ImageOptions.Image")));
            this.colTKVT.MinWidth = 25;
            this.colTKVT.Name = "colTKVT";
            this.colTKVT.Width = 150;
            // 
            // coLSODH
            // 
            this.coLSODH.Caption = "Số DH";
            this.coLSODH.FieldName = "SODDH";
            this.coLSODH.MinWidth = 25;
            this.coLSODH.Name = "coLSODH";
            this.coLSODH.Width = 94;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.simpleButton1);
            this.panelControl3.Location = new System.Drawing.Point(12, 591);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1256, 56);
            this.panelControl3.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton1.ImageOptions.SvgImage")));
            this.simpleButton1.Location = new System.Drawing.Point(1132, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(113, 41);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Lưu";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.simpleButton2);
            this.panelControl4.Location = new System.Drawing.Point(12, 124);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1256, 44);
            this.panelControl4.TabIndex = 2;
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton2.ImageOptions.SvgImage")));
            this.simpleButton2.Location = new System.Drawing.Point(6, 0);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(127, 39);
            this.simpleButton2.TabIndex = 0;
            this.simpleButton2.Text = "Chọn đơn hàng";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // InvoiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 650);
            this.Controls.Add(this.panelControl4);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "InvoiceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InvoiceForm";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditTK.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditKH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditNVIEN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridHHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridKHO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditTK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridMT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.TextBox textBoxSSR;
        private System.Windows.Forms.TextBox textBoxSHD;
        private System.Windows.Forms.TextBox textBoxDG;
        private System.Windows.Forms.TextBox textBoxSP;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditTK;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditKH;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditNV;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraGrid.GridControl gridInvoice;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colMAHH;
        private DevExpress.XtraGrid.Columns.GridColumn colTENHH;
        private DevExpress.XtraGrid.Columns.GridColumn colDVT;
        private DevExpress.XtraGrid.Columns.GridColumn colKHO;
        private DevExpress.XtraGrid.Columns.GridColumn colSLDH;
        private DevExpress.XtraGrid.Columns.GridColumn colDGDH;
        private DevExpress.XtraGrid.Columns.GridColumn colTHANHTIEN;
        private DevExpress.XtraGrid.Columns.GridColumn colTYLECK;
        private DevExpress.XtraGrid.Columns.GridColumn colTIENCK;
        private DevExpress.XtraGrid.Columns.GridColumn colMATHUE;
        private DevExpress.XtraGrid.Columns.GridColumn colTIENTHUE;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridHHS;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colMAHH1;
        private DevExpress.XtraGrid.Columns.GridColumn colTENHH1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridKHO;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colMK;
        private DevExpress.XtraGrid.Columns.GridColumn colTENKHO1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridMT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colMT;
        private DevExpress.XtraGrid.Columns.GridColumn colLT;
        private DevExpress.XtraGrid.Columns.GridColumn colTKDT;
        private DevExpress.XtraGrid.Columns.GridColumn colTKCK;
        private DevExpress.XtraGrid.Columns.GridColumn colTKTHUE;
        private DevExpress.XtraGrid.Columns.GridColumn colTKDUTHUE;
        private DevExpress.XtraGrid.Columns.GridColumn colGIAVON;
        private DevExpress.XtraGrid.Columns.GridColumn colTKGV;
        private DevExpress.XtraGrid.Columns.GridColumn colTIENVON;
        private DevExpress.XtraGrid.Columns.GridColumn colTKVT;
        private DevExpress.XtraGrid.Columns.GridColumn colTONKHO;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.Columns.GridColumn coLSODH;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditNVIEN;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditTK;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colMATK;
        private DevExpress.XtraGrid.Columns.GridColumn colTENTK;
    }
}