﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL.Models.LoadForm;
using BLL.Models;
using BLL.Services;
using DevExpress.XtraSplashScreen;
using AccountingApplication.Settings;
using DevExpress.XtraGrid.Views.Grid;

namespace AccountingApplication.Views.Voucher
{
    public partial class GetVoucherForm : DevExpress.XtraEditors.XtraForm
    {
        int region = 0;
        public GetVoucherForm(int status)
        {
            InitializeComponent();
            loadform();
            region = status;
        }
        public void loadform()
        {
            lookUpEdit1.Properties.DataSource = ObjectService.Instance.GetNHANVIENs();
            lookUpEdit1.Properties.ValueMember = "MANV";
            lookUpEdit1.Properties.DisplayMember = "MANV";
            lookUpEdit1.Properties.NullText = UserManager.CurrentUser.MANV; ;
            

            dateEditFrom.DateTime = DateTime.Now;
            dateEditTo.DateTime = DateTime.Now;

        }
        public BindingList<LoadGetForm> GetDataSources(string VOtext,string DateFrom, string DateTo,string manv)
        {
            
            List<LoadGetForm> GF = VoucherService.Instance.loadGetForms(VOtext, DateFrom,DateTo,region,manv);
            
            BindingList<LoadGetForm> result = new BindingList<LoadGetForm>(GF);
            return result;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string VOtext = txbSOCT.Text;
            string Datetime1 = dateEditFrom.EditValue.ToString();
            string datetime2 = dateEditTo.EditValue.ToString();
            string MANV = lookUpEdit1.Text.ToString();

            BindingList<LoadGetForm> dataSource = GetDataSources(VOtext,Datetime1,datetime2,MANV);
            gridControl1.DataSource = dataSource;

            gridView1.OptionsView.EnableAppearanceEvenRow = true;
            gridView1.OptionsView.EnableAppearanceOddRow = true;
            
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            List<LoadFormINV> listctDH = new List<LoadFormINV>();
            int[] listSeletedRows = gridView1.GetSelectedRows();
            if (region == 0)
            {
                foreach (int rowIndex in listSeletedRows)
                {

                    LoadGetForm row = (LoadGetForm)gridView1.GetRow(rowIndex);
                    listctDH.AddRange(VoucherService.Instance.returnForm(row.SOCT));
                }
            }
            else
            {
                foreach (int rowIndex in listSeletedRows)
                {

                    LoadGetForm row = (LoadGetForm)gridView1.GetRow(rowIndex);
                    listctDH.AddRange(InvoiceService.Instance.returnForm(row.SOCT));
                }
            }
            if (listSeletedRows.Length > 0)
            {
                simpleButton2.Enabled = true;

                BindingList<LoadFormINV> data = new BindingList<LoadFormINV>(listctDH);
                gridControl2.DataSource = data;
                Common.CurrentUserGet = lookUpEdit1.Text.ToString();
                Common.CurrentBindingList = data;
            }
        }

        private void gridControl1_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn) != null && view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString() != String.Empty)
                    Clipboard.SetText(view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString());
                else
                    MessageBox.Show("The value in the selected cell is null or empty!");
                e.Handled = true;
            }
        }

        private void gridControl2_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn) != null && view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString() != String.Empty)
                    Clipboard.SetText(view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString());
                else
                    MessageBox.Show("The value in the selected cell is null or empty!");
                e.Handled = true;
            }
        }
    }
}