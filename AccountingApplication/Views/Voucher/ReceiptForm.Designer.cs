﻿namespace AccountingApplication.Views.Voucher
{
    partial class ReceiptForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReceiptForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.textEditSP = new DevExpress.XtraEditors.TextEdit();
            this.textEditDG = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEditTK = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditKH = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditNVIEN = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditNV = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridPT = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTK1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTENTK1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTHANHTIEN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSOCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditTK.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditKH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditNVIEN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridPT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Location = new System.Drawing.Point(13, 13);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1327, 116);
            this.panelControl1.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.textEditSP);
            this.groupControl1.Controls.Add(this.textEditDG);
            this.groupControl1.Controls.Add(this.dateEdit2);
            this.groupControl1.Controls.Add(this.dateEdit1);
            this.groupControl1.Controls.Add(this.lookUpEditTK);
            this.groupControl1.Controls.Add(this.lookUpEditKH);
            this.groupControl1.Controls.Add(this.lookUpEditNVIEN);
            this.groupControl1.Controls.Add(this.lookUpEditNV);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(6, 6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1316, 105);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin chính";
            // 
            // textEditSP
            // 
            this.textEditSP.Enabled = false;
            this.textEditSP.Location = new System.Drawing.Point(437, 35);
            this.textEditSP.Name = "textEditSP";
            this.textEditSP.Size = new System.Drawing.Size(155, 22);
            this.textEditSP.TabIndex = 16;
            // 
            // textEditDG
            // 
            this.textEditDG.Location = new System.Drawing.Point(999, 81);
            this.textEditDG.Name = "textEditDG";
            this.textEditDG.Size = new System.Drawing.Size(312, 22);
            this.textEditDG.TabIndex = 15;
            // 
            // dateEdit2
            // 
            this.dateEdit2.EditValue = null;
            this.dateEdit2.Location = new System.Drawing.Point(999, 35);
            this.dateEdit2.Name = "dateEdit2";
            this.dateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Size = new System.Drawing.Size(155, 22);
            this.dateEdit2.TabIndex = 13;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(710, 35);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(172, 22);
            this.dateEdit1.TabIndex = 12;
            // 
            // lookUpEditTK
            // 
            this.lookUpEditTK.Location = new System.Drawing.Point(710, 72);
            this.lookUpEditTK.Name = "lookUpEditTK";
            this.lookUpEditTK.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditTK.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MATK", "Mã tài khoản"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENTK", "Tên tài khoản")});
            this.lookUpEditTK.Size = new System.Drawing.Size(172, 22);
            this.lookUpEditTK.TabIndex = 11;
            // 
            // lookUpEditKH
            // 
            this.lookUpEditKH.Location = new System.Drawing.Point(437, 72);
            this.lookUpEditKH.Name = "lookUpEditKH";
            this.lookUpEditKH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditKH.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MAKH", "Mã khách hàng"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENKH", "Tên khách hàng")});
            this.lookUpEditKH.Size = new System.Drawing.Size(155, 22);
            this.lookUpEditKH.TabIndex = 10;
            // 
            // lookUpEditNVIEN
            // 
            this.lookUpEditNVIEN.Location = new System.Drawing.Point(142, 72);
            this.lookUpEditNVIEN.Name = "lookUpEditNVIEN";
            this.lookUpEditNVIEN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditNVIEN.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MANV", "Mã nhân viên"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENNV", "Tên nhân viên", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditNVIEN.Size = new System.Drawing.Size(155, 22);
            this.lookUpEditNVIEN.TabIndex = 9;
            // 
            // lookUpEditNV
            // 
            this.lookUpEditNV.Location = new System.Drawing.Point(142, 35);
            this.lookUpEditNV.Name = "lookUpEditNV";
            this.lookUpEditNV.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditNV.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MALOAICT", "Loại nghiệp vụ"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENLOAICT", "Tên nghiệp vụ")});
            this.lookUpEditNV.Size = new System.Drawing.Size(155, 22);
            this.lookUpEditNV.TabIndex = 8;
            this.lookUpEditNV.EditValueChanged += new System.EventHandler(this.lookUpEditNV_EditValueChanged);
            // 
            // labelControl8
            // 
            this.labelControl8.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl8.ImageOptions.Image")));
            this.labelControl8.Location = new System.Drawing.Point(888, 69);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(87, 36);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = "Diễn giải";
            // 
            // labelControl7
            // 
            this.labelControl7.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl7.ImageOptions.Image")));
            this.labelControl7.Location = new System.Drawing.Point(888, 28);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(91, 36);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "Ngày Thu";
            // 
            // labelControl6
            // 
            this.labelControl6.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl6.ImageOptions.Image")));
            this.labelControl6.Location = new System.Drawing.Point(6, 65);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(113, 36);
            this.labelControl6.TabIndex = 5;
            this.labelControl6.Text = "Mã nhân viên";
            // 
            // labelControl5
            // 
            this.labelControl5.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl5.ImageOptions.Image")));
            this.labelControl5.Location = new System.Drawing.Point(303, 63);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(123, 36);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Mã khách hàng";
            // 
            // labelControl4
            // 
            this.labelControl4.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl4.ImageOptions.Image")));
            this.labelControl4.Location = new System.Drawing.Point(598, 65);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(96, 36);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Tài khoản";
            // 
            // labelControl3
            // 
            this.labelControl3.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl3.ImageOptions.Image")));
            this.labelControl3.Location = new System.Drawing.Point(598, 29);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(106, 36);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Ngày phiếu";
            // 
            // labelControl2
            // 
            this.labelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl2.ImageOptions.Image")));
            this.labelControl2.Location = new System.Drawing.Point(303, 29);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 36);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Số phiếu";
            // 
            // labelControl1
            // 
            this.labelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.ImageOptions.Image")));
            this.labelControl1.Location = new System.Drawing.Point(6, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(126, 36);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Loại nghiệp vụ";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.groupControl2);
            this.panelControl2.Location = new System.Drawing.Point(12, 183);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1328, 350);
            this.panelControl2.TabIndex = 1;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridControl1);
            this.groupControl2.Location = new System.Drawing.Point(7, 5);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1316, 340);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Thông tin chi tiết";
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(6, 29);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemGridPT});
            this.gridControl1.Size = new System.Drawing.Size(1305, 311);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControl1_KeyDown);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTK,
            this.colTHANHTIEN,
            this.colPT,
            this.colSOCT});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            // 
            // colTK
            // 
            this.colTK.AppearanceHeader.Options.UseTextOptions = true;
            this.colTK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTK.Caption = "Tài khoản có";
            this.colTK.ColumnEdit = this.repositoryItemGridPT;
            this.colTK.FieldName = "TKCOPT";
            this.colTK.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTK.ImageOptions.Image")));
            this.colTK.MinWidth = 25;
            this.colTK.Name = "colTK";
            this.colTK.Visible = true;
            this.colTK.VisibleIndex = 1;
            this.colTK.Width = 94;
            // 
            // repositoryItemGridPT
            // 
            this.repositoryItemGridPT.AutoHeight = false;
            this.repositoryItemGridPT.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridPT.Name = "repositoryItemGridPT";
            this.repositoryItemGridPT.PopupView = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTK1,
            this.colTENTK1});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colTK1
            // 
            this.colTK1.AppearanceHeader.Options.UseTextOptions = true;
            this.colTK1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTK1.Caption = "Tài khoản";
            this.colTK1.FieldName = "MATK";
            this.colTK1.Name = "colTK1";
            this.colTK1.Visible = true;
            this.colTK1.VisibleIndex = 0;
            // 
            // colTENTK1
            // 
            this.colTENTK1.AppearanceHeader.Options.UseTextOptions = true;
            this.colTENTK1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTENTK1.Caption = "Tên Tài Khoản";
            this.colTENTK1.FieldName = "TENTK";
            this.colTENTK1.Name = "colTENTK1";
            this.colTENTK1.Visible = true;
            this.colTENTK1.VisibleIndex = 1;
            // 
            // colTHANHTIEN
            // 
            this.colTHANHTIEN.Caption = "Tiền phải thu";
            this.colTHANHTIEN.FieldName = "THANHTIEN";
            this.colTHANHTIEN.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTHANHTIEN.ImageOptions.Image")));
            this.colTHANHTIEN.MinWidth = 25;
            this.colTHANHTIEN.Name = "colTHANHTIEN";
            this.colTHANHTIEN.Visible = true;
            this.colTHANHTIEN.VisibleIndex = 2;
            this.colTHANHTIEN.Width = 94;
            // 
            // colPT
            // 
            this.colPT.AppearanceHeader.Options.UseTextOptions = true;
            this.colPT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPT.Caption = "Số tiền";
            this.colPT.FieldName = "SOTIEN";
            this.colPT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colPT.ImageOptions.Image")));
            this.colPT.MinWidth = 25;
            this.colPT.Name = "colPT";
            this.colPT.Visible = true;
            this.colPT.VisibleIndex = 3;
            this.colPT.Width = 94;
            // 
            // colSOCT
            // 
            this.colSOCT.Caption = "SOCT";
            this.colSOCT.FieldName = "SOCT";
            this.colSOCT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colSOCT.ImageOptions.Image")));
            this.colSOCT.MinWidth = 25;
            this.colSOCT.Name = "colSOCT";
            this.colSOCT.Visible = true;
            this.colSOCT.VisibleIndex = 0;
            this.colSOCT.Width = 94;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.simpleButton1);
            this.panelControl3.Location = new System.Drawing.Point(12, 135);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1327, 42);
            this.panelControl3.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton1.ImageOptions.SvgImage")));
            this.simpleButton1.Location = new System.Drawing.Point(7, 0);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(165, 42);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Chọn hóa đơn";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.simpleButton2);
            this.panelControl4.Location = new System.Drawing.Point(12, 539);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1327, 38);
            this.panelControl4.TabIndex = 2;
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton2.ImageOptions.SvgImage")));
            this.simpleButton2.Location = new System.Drawing.Point(1229, 0);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(94, 38);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Lưu";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // ReceiptForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1352, 586);
            this.Controls.Add(this.panelControl4);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "ReceiptForm";
            this.Text = "ReceiptForm";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditTK.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditKH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditNVIEN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridPT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.TextEdit textEditSP;
        private DevExpress.XtraEditors.TextEdit textEditDG;
        private DevExpress.XtraEditors.DateEdit dateEdit2;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditTK;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditKH;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditNVIEN;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditNV;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colTK;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridPT;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colTK1;
        private DevExpress.XtraGrid.Columns.GridColumn colTENTK1;
        private DevExpress.XtraGrid.Columns.GridColumn colPT;
        private DevExpress.XtraGrid.Columns.GridColumn colSOCT;
        private DevExpress.XtraGrid.Columns.GridColumn colTHANHTIEN;
    }
}