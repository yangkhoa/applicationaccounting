﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL.Services;
using DAL.DBContext;
using AccountingApplication.Settings;
using BLL.Models;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;

namespace AccountingApplication.Views.Voucher
{
    public partial class VoucherForm : DevExpress.XtraEditors.XtraForm
    {
        int Region = 0;
        public VoucherForm(int status, string SODDH)
        {
            InitializeComponent();

            if (status == 0)
            {
                LoadForm();
                List<LoadFormDDH> result = new List<LoadFormDDH>();
                gridVoucher.DataSource = new BindingList<LoadFormDDH>(result);
            }
            else
            {
                LoadForm();
                lookUpEditLHD.Enabled = false;
                DONDATHANG _DDH = VoucherService.Instance.FindVoucher(SODDH);
                lookUpEditLHD.Properties.NullText = SODDH.Substring(0, 4);
                txbNo.Text = SODDH;
                lookUpEditMKH.Properties.NullText = _DDH.MAKH;
                lookUpEditMNV.Properties.NullText = _DDH.MANV;
                lookUpEditMPTTT.Properties.NullText = _DDH.MAPTTT;
                SONGAYNO.Text = _DDH.SONGAYNO.ToString();
                DIENGIAI.Text = _DDH.DIENGIAIDH;
                dateEdit1.DateTime = Convert.ToDateTime(_DDH.NGAYHT);
                dateEdit2.DateTime = Convert.ToDateTime(_DDH.HANTT.Value.Date);


                List<LoadFormDDH> _LF = VoucherService.Instance.FindCTDH(SODDH);
                gridVoucher.DataSource = new BindingList<LoadFormDDH>(_LF);
                Region = 1;
            }


        }

        private void LoadForm()
        {
            List<HANGHOA> listHangHoa = ProductService.Instance.GetListHangHoa();
            repositoryItemsHHa.DataSource = listHangHoa;
            repositoryItemsHHa.ValueMember = "MAHH";
            repositoryItemsHHa.DisplayMember = "MAHH";
            repositoryItemsHHa.NullText = "Xin hãy chọn loại hàng hóa";
            colMAHH1.ColumnEdit = repositoryItemsHHa;

            List<KHOHANG> listKho = ProductService.Instance.GetListKho();
            repositoryItemsMKa.DataSource = listKho;
            repositoryItemsMKa.ValueMember = "MAKHO";
            repositoryItemsMKa.DisplayMember = "MAKHO";
            repositoryItemsMKa.NullText = "Xin hãy chọn kho";
            colMAKHO1.ColumnEdit = repositoryItemsMKa;

            List<THUEGTGT> listTHUE = ObjectService.Instance.GetTHUEGTGTs();
            repositoryItemsMT.DataSource = listTHUE;
            repositoryItemsMT.ValueMember = "MATHUE";
            repositoryItemsMT.DisplayMember = "MATHUE";
            repositoryItemsMT.NullText = "Xin hãy chọn mã thuế";
            colMT.ColumnEdit = repositoryItemsMT;


            lookUpEditLHD.Properties.DataSource = ObjectService.Instance.GetLoaiCTs();
            lookUpEditLHD.Properties.ValueMember = "MALOAICT";
            lookUpEditLHD.Properties.DisplayMember = "MALOAICT";
            lookUpEditLHD.Properties.NullText = "";

            lookUpEditMKH.Properties.DataSource = ObjectService.Instance.GetKHACHHANGs();
            lookUpEditMKH.Properties.ValueMember = "MAKH";
            lookUpEditMKH.Properties.DisplayMember = "MAKH";
            lookUpEditMKH.Properties.NullText = "";

            lookUpEditMNV.Properties.DataSource = ObjectService.Instance.GetNHANVIENs();
            lookUpEditMNV.Properties.ValueMember = "MANV";
            lookUpEditMNV.Properties.DisplayMember = "MANV";
            lookUpEditMNV.Properties.NullText = UserManager.CurrentUser.MANV;
            

            lookUpEditMPTTT.Properties.DataSource = ObjectService.Instance.GetPTTTs();
            lookUpEditMPTTT.Properties.ValueMember = "MAPTTT";
            lookUpEditMPTTT.Properties.DisplayMember = "MAPTTT";
            lookUpEditMPTTT.Properties.NullText = "";

            dateEdit1.DateTime = DateTime.Now;
            dateEdit2.DateTime = DateTime.Now;

        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "MAHH")
            {
                string maHH = gridView1.GetRowCellValue(e.RowHandle, e.Column).ToString();
                HANGHOA hanghoa = ProductService.Instance.GetHangHoaByMaHH(maHH);

                if (hanghoa != null)
                {
                    gridView1.SetRowCellValue(e.RowHandle, "TENHH", hanghoa.TENHH);
                    gridView1.SetRowCellValue(e.RowHandle, "MADVT", hanghoa.MADVT);
                }
            }
            double taxrate = 0;
            var tax = gridView1.GetFocusedRowCellValue(colMATHUE);
            if (e.Column.FieldName == "MATHUE")
            {

                if (tax.ToString() == "05")
                {
                    taxrate = 0.05;
                }
                else if (tax.ToString() == "10")
                {
                    taxrate = 0.1;
                }
            }
            if (e.Column.FieldName == "SOLUONGDH" ||
                e.Column.FieldName == "DONGIADH" ||
                e.Column.FieldName == "TYLECK" ||
                e.Column.FieldName == "MATHUE")
            {
                var quantity = gridView1.GetFocusedRowCellValue(colSOLUONGDH);
                var amount = gridView1.GetFocusedRowCellValue(colDONGIADH);
                var reduce = gridView1.GetFocusedRowCellValue(colTYLECK);


                if (quantity != null && amount != null)
                {
                    gridView1.SetFocusedRowCellValue(colTHANHTIENDH, Convert.ToDecimal(quantity) * Convert.ToDecimal(amount));
                }
                if (quantity != null && amount != null && reduce != null)
                {
                    gridView1.SetFocusedRowCellValue(TIENCK, Convert.ToDecimal(quantity) * Convert.ToDecimal(amount) - Convert.ToDecimal(quantity) * Convert.ToDecimal(amount) * (Convert.ToDecimal(reduce) * 0.01m));
                }
                if (quantity != null && amount != null && tax != null)
                {
                    gridView1.SetFocusedRowCellValue(colTIENTHUE,
                          Convert.ToDecimal(quantity) * Convert.ToDecimal(amount) - Convert.ToDecimal(quantity) * Convert.ToDecimal(amount) * (Convert.ToDecimal(reduce) * 0.01m) +
                       (Convert.ToDecimal(quantity) * Convert.ToDecimal(amount) - Convert.ToDecimal(quantity) * Convert.ToDecimal(amount) * (Convert.ToDecimal(reduce)) * 0.01m) * Convert.ToDecimal(taxrate));
                }
            }
        }


        private void lookUpEditMPTTT_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                PTTHANHTOAN pTTHANHTOAN = lookUpEditMPTTT.GetSelectedDataRow() as PTTHANHTOAN;
                SONGAYNO.Text = pTTHANHTOAN.SONGAYNO.ToString();
                dateEdit2.DateTime = dateEdit2.DateTime.AddDays(Convert.ToInt32(pTTHANHTOAN.SONGAYNO));
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi hệ thống", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void dateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            //PTTHANHTOAN pTTHANHTOANs = lookUpEditMPTTT.GetSelectedDataRow() as PTTHANHTOAN;
            //dateEdit2.DateTime = dateEdit1.DateTime.Date;
            // dateEdit2.DateTime =   dateEdit2.DateTime.AddDays(Convert.ToInt32(pTTHANHTOANs.SONGAYNO));
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {


            DONDATHANG DDH = new DONDATHANG()
            {
                SODDH = txbNo.Text.ToString(),
                MAKH = lookUpEditMKH.Text.ToString(),
                MANV = lookUpEditMNV.Text.ToString(),
                NGAYHT = dateEdit1.DateTime,
                DIENGIAIDH = DIENGIAI.Text.ToString(),
                TINHTRANGDH = "lập đơn hàng",
                MAPTTT = lookUpEditMPTTT.Text.ToString(),
                SONGAYNO = Convert.ToInt32(SONGAYNO.Text),
                HANTT = dateEdit2.DateTime
            };
            BindingList<LoadFormDDH> ListCTDH = gridVoucher.MainView.DataSource as BindingList<LoadFormDDH>;
            List<CTDONDATHANG> _CTDDHs = new List<CTDONDATHANG>();

            if (ListCTDH.Count == 0)
            {
                XtraMessageBox.Show("Bạn chưa chọn mặt hàng trên lưới");
            }
            else
            {
                if (XtraMessageBox.Show("Bạn có muốn lưu dữ liệu?", "Lưu", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        foreach (var item in ListCTDH)
                        {
                            CTDONDATHANG cTDONDATHANGs = new CTDONDATHANG();
                            cTDONDATHANGs.SODDH = txbNo.Text.ToString();
                            cTDONDATHANGs.MAHH = item.MAHH;
                            cTDONDATHANGs.KHO = item.MAKHO;
                            cTDONDATHANGs.SOLUONGDH = item.SOLUONGDH;
                            cTDONDATHANGs.DONGIADH = item.DONGIADH;
                            cTDONDATHANGs.THANHTIENDH = item.THANHTIENDH;
                            cTDONDATHANGs.TYLECK = item.TYLECK;
                            cTDONDATHANGs.TIENCK = item.TIENCK;
                            cTDONDATHANGs.MATHUE = item.MATHUE;
                            cTDONDATHANGs.TIENTHUE = item.TIENTHUE;
                            _CTDDHs.Add(cTDONDATHANGs);
                        }
                        string voucherType = "";
                        if (Region == 1)
                        {
                            voucherType = txbNo.Text.ToString();
                        }
                        else
                        {
                            voucherType = lookUpEditLHD.Text;
                        }

                        VoucherService.Instance.InsertDDH(_CTDDHs, DDH, voucherType, Region);
                        XtraMessageBox.Show("Lưu dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show("Không lưu được dữ liệu", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        throw;
                    }
                }
            }
        }

        private void lookUpEditLHD_EditValueChanged(object sender, EventArgs e)
        {
            var voucher = lookUpEditLHD.EditValue.ToString();
            var voucherno = voucher + "000" + VoucherService.Instance.getVoucherNo(voucher);
            txbNo.Text = voucherno;
        }

        private void gridVoucher_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn) != null && view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString() != String.Empty)
                    Clipboard.SetText(view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString());
                else
                    XtraMessageBox.Show("Không có dữ liệu", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {

        }

        private void gridVoucher_ProcessGridKey(object sender, KeyEventArgs e)
        {
            var grid = sender as GridControl;
            var view = grid.FocusedView as GridView;
            if (e.KeyData == Keys.Delete)
            {
                view.DeleteSelectedRows();
                e.Handled = true;
            }
        }
    }
}