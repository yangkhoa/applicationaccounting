﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using BLL.Models;
using BLL.Services;
using DAL.DBContext;
using AccountingApplication.Settings;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraGrid.Views.Grid;

namespace AccountingApplication.Views.Voucher
{
    public partial class ListExport : DevExpress.XtraEditors.XtraUserControl
    {
        MenuUser menu;
        public ListExport()
        {
            InitializeComponent();
            Text = ViewForm.XKNK;

            ///Phân quyền button
            List<MenuUser> menus = AccountService.Instance.GetListMenuByRole(UserManager.CurrentUser.MACHUCVU);
            menu = menus.FirstOrDefault(x => x.TENNQ == ViewForm.XKNK);
            if (menu != null)
            {
                bbiNew.Enabled = menu.THEM == 1 ? true : false;
                bbiEdit.Enabled = menu.SUA == 1 ? true : false;
                bbiDelete.Enabled = menu.XOA == 1 ? true : false;
            }
            Loadperiod();
            radioGroup1.BackColor = Color.Transparent;
            Point pos = this.PointToScreen(radioGroup1.Location);
            radioGroup1.Parent = ribbonControl;
            radioGroup1.Location = ribbonControl.PointToClient(pos);
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }

        public void Loadperiod()
        {
            List<Period> periods = VoucherService.Instance.GetPeriods();
            lookUpEditPR1.Properties.DataSource = periods;
            lookUpEditPR1.Properties.ValueMember = "MY";
            lookUpEditPR1.Properties.DisplayMember = "MY";
            lookUpEditPR1.Properties.NullText = periods.FirstOrDefault().MY.ToString();

            lookUpEditPR2.Properties.DataSource = periods;
            lookUpEditPR2.Properties.ValueMember = "MY";
            lookUpEditPR2.Properties.DisplayMember = "MY";
            lookUpEditPR2.Properties.NullText = periods.FirstOrDefault().MY.ToString();
        }

        public BindingList<HOADON> GetDataSource()
        {
            Filter filters = new Filter();
            bool a = Convert.ToBoolean(radioGroup1.Properties.Items[radioGroup1.SelectedIndex].Tag);
            var period1 = lookUpEditPR1.Text.ToString();
            var period2 = lookUpEditPR2.Text.ToString();
            if (a)
            {
                filters.periodM1 = Convert.ToInt32(period1.Substring(0, period1.IndexOf('/')));
                filters.periodY1 = Convert.ToInt32(period1.Substring(period1.IndexOf('/') + 1));
                filters.periodM2 = Convert.ToInt32(period2.Substring(0, period2.IndexOf('/')));
                filters.periodY2 = Convert.ToInt32(period2.Substring(period2.IndexOf('/') + 1));
            }
            else
            {
                filters.Datetime1 = dateEdit1.DateTime;
                filters.datetime2 = dateEdit2.DateTime;
            }

            filters.VOtext = FilterTextVO.Text.ToString();
            List<HOADON> DDH = InvoiceService.Instance.GetHOADONs(filters, "PXK");
            BindingList<HOADON> result = new BindingList<HOADON>(DDH);
            return result;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (menu != null)
            {
                bbiEdit.Enabled = menu.SUA == 1 ? true : false;
                bbiDelete.Enabled = menu.XOA == 1 ? true : false;
            }
            bbiPrintPreview.Enabled = true;

            BindingList<HOADON> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;

            gridView.OptionsView.EnableAppearanceEvenRow = true;
            gridView.OptionsView.EnableAppearanceOddRow = true;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (ExportForm Exp = new ExportForm(0,""))
            {

                SplashScreenManager.CloseForm(false);
                Exp.ShowDialog();
                this.Show();
            }
        }

        private void bbiEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "SOCT"));

                using (ExportForm dashboard = new ExportForm(2, a))
                {
                    SplashScreenManager.CloseForm(false);
                    dashboard.ShowDialog();
                    this.Show();
                }
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Lỗi hệ thống", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void bbiDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "SOCT"));
            if (XtraMessageBox.Show("Bạn có muốn xóa hóa đơn: " + a, "Remove", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    InvoiceService.Instance.removeHD(a);
                }
                catch (Exception)
                {

                    XtraMessageBox.Show("Xóa dữ liệu không thành công", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            BindingList<HOADON> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
        }

        private void gridControl_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn) != null && view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString() != String.Empty)
                    Clipboard.SetText(view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString());
                else
                    XtraMessageBox.Show("Không có dữ liệu", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
            }
        }

        private void gridView_MasterRowEmpty(object sender, MasterRowEmptyEventArgs e)
        {
            GridView view = sender as GridView;
            HOADON HD = view.GetRow(e.RowHandle) as HOADON;
            List<LoadFormINV> CTHD = InvoiceService.Instance.returnForm(HD.SOCT.ToString());
            if (HD != null && CTHD != null)
            {
                e.IsEmpty = false;
            }
        }

        private void gridView_MasterRowGetChildList(object sender, MasterRowGetChildListEventArgs e)
        {
            GridView view = sender as GridView;
            HOADON HD = view.GetRow(e.RowHandle) as HOADON;
            List<LoadFormINV> CTHD = InvoiceService.Instance.returnForm(HD.SOCT.ToString());
            if (HD != null)
            {
                e.ChildList = CTHD;
            }
        }

        private void gridView_MasterRowGetRelationCount(object sender, MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;

        }

        private void gridView_MasterRowGetRelationName(object sender, MasterRowGetRelationNameEventArgs e)
        {
            e.RelationName = "Detail";

        }
    }
}
