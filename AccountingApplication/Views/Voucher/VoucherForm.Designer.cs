﻿namespace AccountingApplication.Views.Voucher
{
    partial class VoucherForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VoucherForm));
            this.repositoryItems = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGrid2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txbNo = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEditMNV = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditMPTTT = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditMKH = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditLHD = new DevExpress.XtraEditors.LookUpEdit();
            this.DIENGIAI = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.SONGAYNO = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridVoucher = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMAHH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemsHHa = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMAHH1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTENHH1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTENHHH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDVT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMAKHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemsMKa = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMAKHO1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTENKHO1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSOLUONGDH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDONGIADH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTHANHTIENDH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTYLECK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TIENCK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMATHUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemsMT = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTIENTHUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMPTTT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMKH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditLHD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DIENGIAI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SONGAYNO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridVoucher)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemsHHa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemsMKa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemsMT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // repositoryItems
            // 
            this.repositoryItems.AutoHeight = false;
            this.repositoryItems.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItems.Name = "repositoryItems";
            // 
            // repositoryItemGrid2
            // 
            this.repositoryItemGrid2.AutoHeight = false;
            this.repositoryItemGrid2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGrid2.Name = "repositoryItemGrid2";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.txbNo);
            this.panelControl1.Controls.Add(this.dateEdit2);
            this.panelControl1.Controls.Add(this.dateEdit1);
            this.panelControl1.Controls.Add(this.lookUpEditMNV);
            this.panelControl1.Controls.Add(this.lookUpEditMPTTT);
            this.panelControl1.Controls.Add(this.lookUpEditMKH);
            this.panelControl1.Controls.Add(this.lookUpEditLHD);
            this.panelControl1.Controls.Add(this.DIENGIAI);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.SONGAYNO);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1076, 142);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControl9
            // 
            this.labelControl9.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl9.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("labelControl9.ImageOptions.SvgImage")));
            this.labelControl9.Location = new System.Drawing.Point(321, 8);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(98, 44);
            this.labelControl9.TabIndex = 26;
            this.labelControl9.Text = "Số phiếu";
            // 
            // labelControl8
            // 
            this.labelControl8.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl8.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("labelControl8.ImageOptions.SvgImage")));
            this.labelControl8.Location = new System.Drawing.Point(6, 43);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(116, 44);
            this.labelControl8.TabIndex = 25;
            this.labelControl8.Text = "Số ngày nợ";
            // 
            // labelControl7
            // 
            this.labelControl7.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl7.ImageOptions.Image")));
            this.labelControl7.Location = new System.Drawing.Point(9, 12);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(126, 36);
            this.labelControl7.TabIndex = 24;
            this.labelControl7.Text = "Loại nghiệp vụ";
            // 
            // txbNo
            // 
            this.txbNo.EditValue = "";
            this.txbNo.Enabled = false;
            this.txbNo.Location = new System.Drawing.Point(452, 18);
            this.txbNo.Name = "txbNo";
            this.txbNo.Properties.NullText = "0";
            this.txbNo.Size = new System.Drawing.Size(175, 22);
            this.txbNo.TabIndex = 23;
            // 
            // dateEdit2
            // 
            this.dateEdit2.EditValue = new System.DateTime(2020, 2, 29, 15, 39, 12, 0);
            this.dateEdit2.Location = new System.Drawing.Point(788, 90);
            this.dateEdit2.Name = "dateEdit2";
            this.dateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Size = new System.Drawing.Size(174, 22);
            this.dateEdit2.TabIndex = 21;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = new System.DateTime(2020, 2, 29, 15, 39, 12, 0);
            this.dateEdit1.Location = new System.Drawing.Point(788, 18);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(174, 22);
            this.dateEdit1.TabIndex = 20;
            this.dateEdit1.EditValueChanged += new System.EventHandler(this.dateEdit1_EditValueChanged);
            // 
            // lookUpEditMNV
            // 
            this.lookUpEditMNV.Location = new System.Drawing.Point(788, 55);
            this.lookUpEditMNV.Name = "lookUpEditMNV";
            this.lookUpEditMNV.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditMNV.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MANV", "Mã", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENNV", "Tên Nhân Viên", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditMNV.Size = new System.Drawing.Size(174, 22);
            this.lookUpEditMNV.TabIndex = 19;
            // 
            // lookUpEditMPTTT
            // 
            this.lookUpEditMPTTT.Location = new System.Drawing.Point(452, 93);
            this.lookUpEditMPTTT.Name = "lookUpEditMPTTT";
            this.lookUpEditMPTTT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditMPTTT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MAPTTT", "Mã", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENPTTT", "Tên Phương Thức", 250, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditMPTTT.Size = new System.Drawing.Size(174, 22);
            this.lookUpEditMPTTT.TabIndex = 18;
            this.lookUpEditMPTTT.EditValueChanged += new System.EventHandler(this.lookUpEditMPTTT_EditValueChanged);
            // 
            // lookUpEditMKH
            // 
            this.lookUpEditMKH.Location = new System.Drawing.Point(452, 55);
            this.lookUpEditMKH.Name = "lookUpEditMKH";
            this.lookUpEditMKH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditMKH.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MAKH", "Mã", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENKH", "Tên Khách Hàng", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditMKH.Size = new System.Drawing.Size(174, 22);
            this.lookUpEditMKH.TabIndex = 17;
            // 
            // lookUpEditLHD
            // 
            this.lookUpEditLHD.Location = new System.Drawing.Point(141, 18);
            this.lookUpEditLHD.Name = "lookUpEditLHD";
            this.lookUpEditLHD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditLHD.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MALOAICT", "Mã", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENLOAICT", "Tên loại chứng từ", 250, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditLHD.Size = new System.Drawing.Size(174, 22);
            this.lookUpEditLHD.TabIndex = 16;
            this.lookUpEditLHD.EditValueChanged += new System.EventHandler(this.lookUpEditLHD_EditValueChanged);
            // 
            // DIENGIAI
            // 
            this.DIENGIAI.Location = new System.Drawing.Point(140, 93);
            this.DIENGIAI.Name = "DIENGIAI";
            this.DIENGIAI.Size = new System.Drawing.Size(175, 22);
            this.DIENGIAI.TabIndex = 13;
            // 
            // labelControl6
            // 
            this.labelControl6.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl6.ImageOptions.Image")));
            this.labelControl6.Location = new System.Drawing.Point(638, 87);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(126, 36);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Ngày Đáo Hạn";
            // 
            // labelControl5
            // 
            this.labelControl5.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl5.ImageOptions.Image")));
            this.labelControl5.Location = new System.Drawing.Point(321, 86);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(89, 36);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "Mã PTTT";
            // 
            // labelControl4
            // 
            this.labelControl4.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl4.ImageOptions.Image")));
            this.labelControl4.Location = new System.Drawing.Point(9, 84);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(88, 36);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Diễn Giải";
            // 
            // labelControl3
            // 
            this.labelControl3.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl3.ImageOptions.Image")));
            this.labelControl3.Location = new System.Drawing.Point(638, 51);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(116, 36);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Mã Nhân Viên";
            // 
            // labelControl2
            // 
            this.labelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl2.ImageOptions.Image")));
            this.labelControl2.Location = new System.Drawing.Point(321, 51);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(125, 36);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Mã Khách Hàng";
            // 
            // labelControl1
            // 
            this.labelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.ImageOptions.Image")));
            this.labelControl1.Location = new System.Drawing.Point(638, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(106, 36);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Ngày phiếu";
            // 
            // SONGAYNO
            // 
            this.SONGAYNO.Enabled = false;
            this.SONGAYNO.Location = new System.Drawing.Point(140, 55);
            this.SONGAYNO.Name = "SONGAYNO";
            this.SONGAYNO.Properties.NullText = "0";
            this.SONGAYNO.Size = new System.Drawing.Size(175, 22);
            this.SONGAYNO.TabIndex = 3;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridVoucher);
            this.panelControl2.Location = new System.Drawing.Point(12, 161);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1076, 338);
            this.panelControl2.TabIndex = 1;
            // 
            // gridVoucher
            // 
            this.gridVoucher.Location = new System.Drawing.Point(6, 6);
            this.gridVoucher.MainView = this.gridView1;
            this.gridVoucher.Name = "gridVoucher";
            this.gridVoucher.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemsHHa,
            this.repositoryItemsMKa,
            this.repositoryItemsMT});
            this.gridVoucher.Size = new System.Drawing.Size(1063, 327);
            this.gridVoucher.TabIndex = 0;
            this.gridVoucher.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridVoucher.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridVoucher_KeyDown);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMAHH,
            this.colTENHHH,
            this.colDVT,
            this.colMAKHO,
            this.colSOLUONGDH,
            this.colDONGIADH,
            this.colTHANHTIENDH,
            this.colTYLECK,
            this.TIENCK,
            this.colMATHUE,
            this.colTIENTHUE});
            this.gridView1.GridControl = this.gridVoucher;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            // 
            // colMAHH
            // 
            this.colMAHH.AppearanceHeader.Options.UseTextOptions = true;
            this.colMAHH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMAHH.Caption = "Mã Hàng Hóa";
            this.colMAHH.ColumnEdit = this.repositoryItemsHHa;
            this.colMAHH.FieldName = "MAHH";
            this.colMAHH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colMAHH.ImageOptions.Image")));
            this.colMAHH.MinWidth = 25;
            this.colMAHH.Name = "colMAHH";
            this.colMAHH.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "MAHH", "Số hàng: {0}")});
            this.colMAHH.Visible = true;
            this.colMAHH.VisibleIndex = 0;
            this.colMAHH.Width = 150;
            // 
            // repositoryItemsHHa
            // 
            this.repositoryItemsHHa.AutoHeight = false;
            this.repositoryItemsHHa.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemsHHa.Name = "repositoryItemsHHa";
            this.repositoryItemsHHa.PopupView = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMAHH1,
            this.colTENHH1});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colMAHH1
            // 
            this.colMAHH1.AppearanceHeader.Options.UseTextOptions = true;
            this.colMAHH1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMAHH1.Caption = "Mã";
            this.colMAHH1.FieldName = "MAHH";
            this.colMAHH1.Name = "colMAHH1";
            this.colMAHH1.Visible = true;
            this.colMAHH1.VisibleIndex = 0;
            // 
            // colTENHH1
            // 
            this.colTENHH1.AppearanceHeader.Options.UseTextOptions = true;
            this.colTENHH1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTENHH1.Caption = "Tên hàng hóa";
            this.colTENHH1.FieldName = "TENHH";
            this.colTENHH1.Name = "colTENHH1";
            this.colTENHH1.Visible = true;
            this.colTENHH1.VisibleIndex = 1;
            // 
            // colTENHHH
            // 
            this.colTENHHH.Caption = "Tên Hàng Hóa";
            this.colTENHHH.FieldName = "TENHH";
            this.colTENHHH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTENHHH.ImageOptions.Image")));
            this.colTENHHH.MinWidth = 25;
            this.colTENHHH.Name = "colTENHHH";
            this.colTENHHH.Visible = true;
            this.colTENHHH.VisibleIndex = 2;
            this.colTENHHH.Width = 94;
            // 
            // colDVT
            // 
            this.colDVT.Caption = "ĐVT";
            this.colDVT.FieldName = "MADVT";
            this.colDVT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colDVT.ImageOptions.Image")));
            this.colDVT.MinWidth = 25;
            this.colDVT.Name = "colDVT";
            this.colDVT.Visible = true;
            this.colDVT.VisibleIndex = 3;
            this.colDVT.Width = 94;
            // 
            // colMAKHO
            // 
            this.colMAKHO.AppearanceHeader.Options.UseTextOptions = true;
            this.colMAKHO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMAKHO.Caption = "Kho Hàng";
            this.colMAKHO.ColumnEdit = this.repositoryItemsMKa;
            this.colMAKHO.FieldName = "MAKHO";
            this.colMAKHO.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colMAKHO.ImageOptions.Image")));
            this.colMAKHO.MinWidth = 25;
            this.colMAKHO.Name = "colMAKHO";
            this.colMAKHO.Visible = true;
            this.colMAKHO.VisibleIndex = 1;
            this.colMAKHO.Width = 94;
            // 
            // repositoryItemsMKa
            // 
            this.repositoryItemsMKa.AutoHeight = false;
            this.repositoryItemsMKa.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemsMKa.Name = "repositoryItemsMKa";
            this.repositoryItemsMKa.PopupView = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMAKHO1,
            this.colTENKHO1});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // colMAKHO1
            // 
            this.colMAKHO1.Caption = "Mã";
            this.colMAKHO1.FieldName = "MAKHO";
            this.colMAKHO1.Name = "colMAKHO1";
            this.colMAKHO1.Visible = true;
            this.colMAKHO1.VisibleIndex = 0;
            // 
            // colTENKHO1
            // 
            this.colTENKHO1.Caption = "Tên Kho Hàng";
            this.colTENKHO1.FieldName = "TENKHO";
            this.colTENKHO1.Name = "colTENKHO1";
            this.colTENKHO1.Visible = true;
            this.colTENKHO1.VisibleIndex = 1;
            // 
            // colSOLUONGDH
            // 
            this.colSOLUONGDH.AppearanceHeader.Options.UseTextOptions = true;
            this.colSOLUONGDH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSOLUONGDH.Caption = "Số lượng đơn hàng";
            this.colSOLUONGDH.DisplayFormat.FormatString = "#,#";
            this.colSOLUONGDH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSOLUONGDH.FieldName = "SOLUONGDH";
            this.colSOLUONGDH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colSOLUONGDH.ImageOptions.Image")));
            this.colSOLUONGDH.MinWidth = 25;
            this.colSOLUONGDH.Name = "colSOLUONGDH";
            this.colSOLUONGDH.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SOLUONGDH", "Tổng SL: {0:#,#}")});
            this.colSOLUONGDH.Visible = true;
            this.colSOLUONGDH.VisibleIndex = 4;
            this.colSOLUONGDH.Width = 150;
            // 
            // colDONGIADH
            // 
            this.colDONGIADH.AppearanceHeader.Options.UseTextOptions = true;
            this.colDONGIADH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDONGIADH.Caption = "Đơn giá đơn hàng";
            this.colDONGIADH.DisplayFormat.FormatString = "#,#";
            this.colDONGIADH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDONGIADH.FieldName = "DONGIADH";
            this.colDONGIADH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colDONGIADH.ImageOptions.Image")));
            this.colDONGIADH.MinWidth = 25;
            this.colDONGIADH.Name = "colDONGIADH";
            this.colDONGIADH.Visible = true;
            this.colDONGIADH.VisibleIndex = 5;
            this.colDONGIADH.Width = 150;
            // 
            // colTHANHTIENDH
            // 
            this.colTHANHTIENDH.AppearanceHeader.Options.UseTextOptions = true;
            this.colTHANHTIENDH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTHANHTIENDH.Caption = "Thành tiền đơn hàng";
            this.colTHANHTIENDH.DisplayFormat.FormatString = "#,#";
            this.colTHANHTIENDH.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTHANHTIENDH.FieldName = "THANHTIENDH";
            this.colTHANHTIENDH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTHANHTIENDH.ImageOptions.Image")));
            this.colTHANHTIENDH.MinWidth = 25;
            this.colTHANHTIENDH.Name = "colTHANHTIENDH";
            this.colTHANHTIENDH.OptionsColumn.AllowEdit = false;
            this.colTHANHTIENDH.OptionsColumn.AllowFocus = false;
            this.colTHANHTIENDH.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "THANHTIENDH", "Tổng Tiền: {0:#,#}")});
            this.colTHANHTIENDH.Visible = true;
            this.colTHANHTIENDH.VisibleIndex = 6;
            this.colTHANHTIENDH.Width = 150;
            // 
            // colTYLECK
            // 
            this.colTYLECK.AppearanceHeader.Options.UseTextOptions = true;
            this.colTYLECK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTYLECK.Caption = "Tỷ lệ chiết khấu";
            this.colTYLECK.DisplayFormat.FormatString = "#,#";
            this.colTYLECK.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTYLECK.FieldName = "TYLECK";
            this.colTYLECK.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTYLECK.ImageOptions.Image")));
            this.colTYLECK.MinWidth = 25;
            this.colTYLECK.Name = "colTYLECK";
            this.colTYLECK.Visible = true;
            this.colTYLECK.VisibleIndex = 7;
            this.colTYLECK.Width = 150;
            // 
            // TIENCK
            // 
            this.TIENCK.AppearanceHeader.Options.UseTextOptions = true;
            this.TIENCK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TIENCK.Caption = "Thành Tiền Sau Chiết Khấu";
            this.TIENCK.DisplayFormat.FormatString = "#,#";
            this.TIENCK.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TIENCK.FieldName = "TIENCK";
            this.TIENCK.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("TIENCK.ImageOptions.Image")));
            this.TIENCK.MinWidth = 25;
            this.TIENCK.Name = "TIENCK";
            this.TIENCK.OptionsColumn.AllowEdit = false;
            this.TIENCK.OptionsColumn.AllowFocus = false;
            this.TIENCK.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TIENCK", "Tổng tiền CK: {0:#,#}")});
            this.TIENCK.Visible = true;
            this.TIENCK.VisibleIndex = 8;
            this.TIENCK.Width = 200;
            // 
            // colMATHUE
            // 
            this.colMATHUE.AppearanceHeader.Options.UseTextOptions = true;
            this.colMATHUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMATHUE.Caption = "Mã Thuế";
            this.colMATHUE.ColumnEdit = this.repositoryItemsMT;
            this.colMATHUE.FieldName = "MATHUE";
            this.colMATHUE.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colMATHUE.ImageOptions.Image")));
            this.colMATHUE.MinWidth = 25;
            this.colMATHUE.Name = "colMATHUE";
            this.colMATHUE.Visible = true;
            this.colMATHUE.VisibleIndex = 9;
            this.colMATHUE.Width = 150;
            // 
            // repositoryItemsMT
            // 
            this.repositoryItemsMT.AutoHeight = false;
            this.repositoryItemsMT.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemsMT.Name = "repositoryItemsMT";
            this.repositoryItemsMT.PopupView = this.gridView3;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMT,
            this.colTT});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // colMT
            // 
            this.colMT.Caption = "Mã";
            this.colMT.FieldName = "MATHUE";
            this.colMT.Name = "colMT";
            this.colMT.Visible = true;
            this.colMT.VisibleIndex = 0;
            // 
            // colTT
            // 
            this.colTT.Caption = "Loại Thuế";
            this.colTT.FieldName = "TENTHUE";
            this.colTT.Name = "colTT";
            this.colTT.Visible = true;
            this.colTT.VisibleIndex = 1;
            // 
            // colTIENTHUE
            // 
            this.colTIENTHUE.AppearanceHeader.Options.UseTextOptions = true;
            this.colTIENTHUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTIENTHUE.Caption = "Thành Tiền Sau Thuế";
            this.colTIENTHUE.DisplayFormat.FormatString = "#,#";
            this.colTIENTHUE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTIENTHUE.FieldName = "TIENTHUE";
            this.colTIENTHUE.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTIENTHUE.ImageOptions.Image")));
            this.colTIENTHUE.MinWidth = 25;
            this.colTIENTHUE.Name = "colTIENTHUE";
            this.colTIENTHUE.OptionsColumn.AllowEdit = false;
            this.colTIENTHUE.OptionsColumn.AllowFocus = false;
            this.colTIENTHUE.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TIENTHUE", "Tổng tiền thuế: SUM={0:#,#}")});
            this.colTIENTHUE.Visible = true;
            this.colTIENTHUE.VisibleIndex = 10;
            this.colTIENTHUE.Width = 200;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton1.ImageOptions.SvgImage")));
            this.simpleButton1.Location = new System.Drawing.Point(957, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(114, 42);
            this.simpleButton1.TabIndex = 2;
            this.simpleButton1.Text = "Lưu";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.simpleButton1);
            this.panelControl3.Location = new System.Drawing.Point(12, 500);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1076, 53);
            this.panelControl3.TabIndex = 4;
            // 
            // VoucherForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1098, 565);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "VoucherForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VoucherForm";
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txbNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMPTTT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMKH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditLHD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DIENGIAI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SONGAYNO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridVoucher)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemsHHa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemsMKa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemsMT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridVoucher;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit DIENGIAI;
        private DevExpress.XtraGrid.Columns.GridColumn colMAHH;
        private DevExpress.XtraGrid.Columns.GridColumn colMAKHO;
        private DevExpress.XtraGrid.Columns.GridColumn colSOLUONGDH;
        private DevExpress.XtraGrid.Columns.GridColumn colDONGIADH;
        private DevExpress.XtraGrid.Columns.GridColumn colTHANHTIENDH;
        private DevExpress.XtraGrid.Columns.GridColumn colTYLECK;
        private DevExpress.XtraGrid.Columns.GridColumn TIENCK;
        private DevExpress.XtraGrid.Columns.GridColumn colMATHUE;
        private DevExpress.XtraGrid.Columns.GridColumn colTIENTHUE;
        private DevExpress.XtraGrid.Columns.GridColumn colTENHHH;
        private DevExpress.XtraGrid.Columns.GridColumn colDVT;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditMNV;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditMPTTT;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditMKH;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditLHD;
        private DevExpress.XtraEditors.TextEdit SONGAYNO;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItems;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGrid2;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemsHHa;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colMAHH1;
        private DevExpress.XtraGrid.Columns.GridColumn colTENHH1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemsMKa;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colMAKHO1;
        private DevExpress.XtraGrid.Columns.GridColumn colTENKHO1;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.DateEdit dateEdit2;
        private DevExpress.XtraEditors.TextEdit txbNo;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemsMT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colMT;
        private DevExpress.XtraGrid.Columns.GridColumn colTT;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
    }
}