﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using BLL.Models;
using BLL.Services;
using DAL.DBContext;
using AccountingApplication.Settings;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraGrid.Views.Grid;
using AccountingApplication.Views.DocumentView;
using BLL.Models.ReportFormModel;

namespace AccountingApplication.Views.Voucher
{
    public partial class ListSalesInvoice : DevExpress.XtraEditors.XtraUserControl
    {
        MenuUser menu;

        public ListSalesInvoice()
        {
            InitializeComponent();
            Text = ViewForm.HDB;

            ///Phân quyền button
            List<MenuUser> menus = AccountService.Instance.GetListMenuByRole(UserManager.CurrentUser.MACHUCVU);
            menu = menus.FirstOrDefault(x => x.TENNQ == ViewForm.HDB);
            if (menu != null)
            {
                bbiNew.Enabled = menu.THEM == 1 ? true : false;
                bbiEdit.Enabled = menu.SUA == 1 ? true : false;
                bbiDelete.Enabled = menu.XOA == 1 ? true : false;
            }

            Loadperiod();
            radioGroup1.BackColor = Color.Transparent;
            Point pos = this.PointToScreen(radioGroup1.Location);
            radioGroup1.Parent = ribbonControl;
            radioGroup1.Location = ribbonControl.PointToClient(pos);

        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }

        public void Loadperiod()
        {
            List<Period> periods = VoucherService.Instance.GetPeriods();
            lookUpEdit1.Properties.DataSource = periods;
            lookUpEdit1.Properties.ValueMember = "MY";
            lookUpEdit1.Properties.DisplayMember = "MY";
            lookUpEdit1.Properties.NullText = periods.FirstOrDefault().MY.ToString();

            lookUpEdit2.Properties.DataSource = periods;
            lookUpEdit2.Properties.ValueMember = "MY";
            lookUpEdit2.Properties.DisplayMember = "MY";
            lookUpEdit2.Properties.NullText = periods.FirstOrDefault().MY.ToString();
        }

        public BindingList<HOADON> GetDataSource()
        {
            Filter filters = new Filter();
            bool a = Convert.ToBoolean(radioGroup1.Properties.Items[radioGroup1.SelectedIndex].Tag);
            var period1 = lookUpEdit1.Text.ToString();
            var period2 = lookUpEdit2.Text.ToString();
            if (a)
            {
                filters.periodM1 = Convert.ToInt32(period1.Substring(0, period1.IndexOf('/')));
                filters.periodY1 = Convert.ToInt32(period1.Substring(period1.IndexOf('/') + 1));
                filters.periodM2 = Convert.ToInt32(period2.Substring(0, period2.IndexOf('/')));
                filters.periodY2 = Convert.ToInt32(period2.Substring(period2.IndexOf('/') + 1));
            }
            else
            {
                filters.Datetime1 = dateEdit1.DateTime;
                filters.datetime2 = dateEdit2.DateTime;
            }

            filters.VOtext = textEdit1.Text.ToString();
            List<HOADON> DDH = InvoiceService.Instance.GetHOADONs(filters,"HDB");
            BindingList<HOADON> result = new BindingList<HOADON>(DDH);
            return result;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (menu != null)
            {
                bbiEdit.Enabled = menu.SUA == 1 ? true : false;
                bbiDelete.Enabled = menu.XOA == 1 ? true : false;
            }

            bbiPrintPreview.Enabled = true;
           
            BindingList<HOADON> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            
            gridView.OptionsView.EnableAppearanceEvenRow = true;
            gridView.OptionsView.EnableAppearanceOddRow = true;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (InvoiceForm inv = new InvoiceForm(0,""))
            {
                SplashScreenManager.CloseForm(false);
                inv.ShowDialog();
                this.Show();
            }
        }

        private void bbiEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "SOCT"));

            using (InvoiceForm dashboard = new InvoiceForm(2, a))
            {
                SplashScreenManager.CloseForm(false);
                dashboard.ShowDialog();
                this.Show();
            }
        }

     

        private void bbiDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "SOCT"));
            if (XtraMessageBox.Show("Bạn có muốn xóa hóa đơn: " + a, "Remove", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    InvoiceService.Instance.removeHD(a);
                }
                catch (Exception)
                {

                    XtraMessageBox.Show("Xóa dữ liệu không thành công");
                }

            }
           BindingList<HOADON> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
        }


        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn) != null && view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString() != String.Empty)
                    Clipboard.SetText(view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString());
                else
                    XtraMessageBox.Show("The value in the selected cell is null or empty!");
                e.Handled = true;
            }
        }

        private void gridView_MasterRowEmpty(object sender, MasterRowEmptyEventArgs e)
        {
            GridView view = sender as GridView;
            HOADON HD = view.GetRow(e.RowHandle) as HOADON;
            List<LoadFormINV> CTHD = InvoiceService.Instance.returnForm(HD.SOCT.ToString());
            if (HD != null && CTHD != null)
            {
                e.IsEmpty = false;
            }
        }

        private void gridView_MasterRowGetChildList(object sender, MasterRowGetChildListEventArgs e)
        {
            GridView view = sender as GridView;
            HOADON HD = view.GetRow(e.RowHandle) as HOADON;
            List<LoadFormINV> CTHD = InvoiceService.Instance.returnForm(HD.SOCT.ToString());
            if (HD != null)
            {
                e.ChildList = CTHD;
            }
        }

        private void gridView_MasterRowGetRelationCount(object sender, MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;

        }

        private void gridView_MasterRowGetRelationName(object sender, MasterRowGetRelationNameEventArgs e)
        {
            e.RelationName = "Detail";

        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "SOCT"));
            List<LoadFormINV> data = InvoiceService.Instance.returnFormInvoice(a);
            LoadPDKMaster pDKMaster = ReportService.Instance.GetPDKMaster(a);
            using (Invoice lD = new Invoice())
            {
                lD.printInvoice(data,pDKMaster);
                lD.ShowDialog();
            }

        }

        private void gridView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
            {
                return;
            }
            var rowR = gridView.FocusedRowHandle;
            var focus = gridView.GetFocusedRow();
            if (focus == null )
            {
                return;
            }
            if (rowR >= 0)
            {
                popupMenu1.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
            else
            {
                popupMenu1.HidePopup();
            }
        }
    }
}
