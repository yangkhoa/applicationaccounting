﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL.Services;
using BLL.Models;
using AccountingApplication.Settings;
using DevExpress.XtraGrid.Views.Grid;

namespace AccountingApplication.Views.Voucher
{
    public partial class GetInvoiceForm : DevExpress.XtraEditors.XtraForm
    {
        public GetInvoiceForm()
        {
            InitializeComponent();
            LoadForm();
        }

        public void LoadForm()
        {
            lookUpEditKH.Properties.DataSource = ObjectService.Instance.GetKHACHHANGs();
            lookUpEditKH.Properties.ValueMember = "MAKH";
            lookUpEditKH.Properties.DisplayMember = "MAKH";
            lookUpEditKH.Properties.NullText = "KH0001";


            dateEditFrom.DateTime = DateTime.Now;
            dateEditTo.DateTime = DateTime.Now;

        }

        public BindingList<LoadReceipForm> GetDataSources(string VOtext, string DateFrom, string DateTo, string makh)
        {

            List<LoadReceipForm> data = ReceiptService.Instance.loadGetForms(VOtext, DateFrom, DateTo, 0, makh);

            BindingList<LoadReceipForm> dataSource = new BindingList<LoadReceipForm>(data);
            return dataSource;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string VOtext = txbSOCT.Text;
            string Datetime1 = dateEditFrom.EditValue.ToString();
            string datetime2 = dateEditTo.EditValue.ToString();
            string MAKH = lookUpEditKH.Text.ToString();

            BindingList<LoadReceipForm> dataSource = GetDataSources(VOtext, Datetime1, datetime2, MAKH);
            gridControl1.DataSource = dataSource;

            gridView1.OptionsView.EnableAppearanceEvenRow = true;
            gridView1.OptionsView.EnableAppearanceOddRow = true;
            simpleButton2.Enabled = true;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            List<LoadReceipForm> listctDH = new List<LoadReceipForm>();
            int[] listSeletedRows = gridView1.GetSelectedRows();
           
                foreach (int rowIndex in listSeletedRows)
                {
                    LoadReceipForm row = (LoadReceipForm)gridView1.GetRow(rowIndex);
                    listctDH.Add(row);
                }
            if (listSeletedRows.Length > 0)
            {
                Common.CurrentBindingList = listctDH;
            }
            else
            {
                XtraMessageBox.Show("Bạn chưa chọn phiếu trên lưới");
            }

            this.Close();
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if  (e.Column.FieldName == "SOTIEN")
            {
                int Phaithu = Convert.ToInt32(gridView1.GetFocusedRowCellValue(colPHAITHU));
                int sotien = Convert.ToInt32(gridView1.GetFocusedRowCellValue(colSOTIEN));

                if (sotien > Phaithu)
                {
                    XtraMessageBox.Show("Sô tiền lập phiếu thu không hợp lệ");
                    gridView1.SetFocusedRowCellValue(colSOTIEN, 0);
                }
            }
        }

        private void gridControl1_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn) != null && view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString() != String.Empty)
                    Clipboard.SetText(view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString());
                else
                    XtraMessageBox.Show("The value in the selected cell is null or empty!");
                e.Handled = true;
            }
        }
    }
}