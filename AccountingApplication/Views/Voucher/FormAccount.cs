﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL.DBContext;
using BLL.Services;
using BLL.Models.LoadFormModel;

namespace AccountingApplication.Views.Voucher
{
    public partial class FormAccount : DevExpress.XtraEditors.XtraForm
    {
        int statusForm = 0;
        public FormAccount(int status, string MATK)
        {
            InitializeComponent();
            statusForm = status;
            dataSource();
            if (status == 1 || status == 2)
            {
                if (status == 2)
                {
                    simpleButton1.Enabled = false;
                }
                TAIKHOAN TKDetail = ObjectService.Instance.GetCTTK(MATK);
                textEdit1.Text = TKDetail.MATK;
                textEdit2.Text = TKDetail.TENTK;
                lookUpEdit1.Properties.NullText = TKDetail.TKME;
                lookUpEdit2.Properties.NullText = Convert.ToString(TKDetail.BACTK);
                textEdit1.Enabled = false;
            }

        }
        public void dataSource()
        {
            List<GroupAccount> GA = new List<GroupAccount>();
            GA.Add(new GroupAccount() {
                IDGroup = 1,
                Tengroup = "Nguồn vốn"
            });
            GA.Add(new GroupAccount()
            {
                IDGroup = 2,
                Tengroup = "Tài sản"
            });
            GA.Add(new GroupAccount()
            {
                IDGroup = 3,
                Tengroup = "Doanh thu"
            });
            GA.Add(new GroupAccount()
            {
                IDGroup = 4,
                Tengroup = "Chi phí"
            });
            lookUpEdit1.Properties.DataSource = ObjectService.Instance.GetTAIKHOANs();
            lookUpEdit1.Properties.DisplayMember = "MATK";
            lookUpEdit1.Properties.ValueMember = "MATK";
            lookUpEdit1.Properties.NullText = "";

            lookUpEdit2.Properties.DataSource = GA;
            lookUpEdit2.Properties.DisplayMember = "Tengroup";
            lookUpEdit2.Properties.ValueMember = "IDGroup";
            lookUpEdit2.Properties.NullText = "";
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                TAIKHOAN TK = new TAIKHOAN()
                {
                    MATK = textEdit1.Text,
                    TENTK = textEdit2.Text,
                    TKME = lookUpEdit1.Text.ToString(),
                    BACTK = Convert.ToInt32(lookUpEdit2.EditValue)

                };
                if (statusForm == 0)
                {

                    ObjectService.Instance.InsertTK(TK);
                }
                else
                {
                    ObjectService.Instance.UpdateTK(TK);

                }
                simpleButton1.Enabled = false;
                XtraMessageBox.Show("Lưu dữ liệu thành công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Không lưu được dữ liệu, bạn nhập thông tin sai", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}