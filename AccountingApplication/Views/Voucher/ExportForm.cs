﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL.DBContext;
using BLL.Services;
using BLL.Models;
using DevExpress.XtraSplashScreen;
using AccountingApplication.Settings;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;

namespace AccountingApplication.Views.Voucher
{
    public partial class ExportForm : DevExpress.XtraEditors.XtraForm
    {
        int region = 0;
        public ExportForm(int status, string _SOCT)
        {
            InitializeComponent();
            LoadForm();
            if (status == 1)
            {
            }
            else if (status == 2)
            {
                lookUpEditNV.Enabled = false;
                lookUpEditNVIEN.Enabled = false;
                lookUpEditKH.Enabled = false;
                lookUpEditTK.Enabled = false;
                dateEdit1.Enabled = false;
                colMAHH.OptionsColumn.ReadOnly = true;
                colMAKHO.OptionsColumn.ReadOnly = true;
                colTKVT.OptionsColumn.ReadOnly = true;
                HOADON HD = InvoiceService.Instance.FindVoucher(_SOCT);
                lookUpEditNV.Properties.NullText = HD.MALOAICT;
                textEdit1.Text = HD.SOCT;
                lookUpEditNVIEN.Properties.NullText = HD.MANV;
                lookUpEditKH.Properties.NullText = HD.MAKH;
                lookUpEditTK.Properties.NullText = HD.TKNO;
                textEdit2.Text = HD.DIENGIAICT;
                dateEdit1.DateTime = Convert.ToDateTime(HD.NGAYLAPCT);
                List<LoadFormINV> _LF = InvoiceService.Instance.returnForm(_SOCT);
                gridControl1.DataSource = new BindingList<LoadFormINV>(_LF);
                region = 1;

            }
            else
            {
                List<LoadFormINV> result = new List<LoadFormINV>();
                gridControl1.DataSource = new BindingList<LoadFormINV>(result);
            }
        }
        public void loadgetbinddinglist()
        {
            try
            {

                gridControl1.DataSource = Common.CurrentBindingList;
                lookUpEditNVIEN.Text = Common.CurrentUserGet;
                var a = Common.CurrentBindingList[0].SODDH;
                HOADON HD = InvoiceService.Instance.FindVoucher(a);
                lookUpEditKH.Text = HD.MAKH.ToString();
            }
            catch (Exception)
            {
                
            }
            Common.Clear();

        }
        private void LoadForm()
        {
            dateEdit1.DateTime = DateTime.Now;
            List<HANGHOA> listHangHoa = ProductService.Instance.GetListHangHoa();
            repositoryItemGridHHs.DataSource = listHangHoa;
            repositoryItemGridHHs.ValueMember = "MAHH";
            repositoryItemGridHHs.DisplayMember = "MAHH";
            repositoryItemGridHHs.NullText = "Xin hãy chọn loại hàng hóa";
            colMAHH1.ColumnEdit = repositoryItemGridHHs;

            List<KHOHANG> listKho = ProductService.Instance.GetListKho();
            repositoryItemGridKHO.DataSource = listKho;
            repositoryItemGridKHO.ValueMember = "MAKHO";
            repositoryItemGridKHO.DisplayMember = "MAKHO";
            repositoryItemGridKHO.NullText = "Xin hãy chọn kho";
            colMAKHO1.ColumnEdit = repositoryItemGridKHO;

            List<TAIKHOAN> listTK = ObjectService.Instance.GetTAIKHOANs();
            repositoryItemGridTK.DataSource = listTK;
            repositoryItemGridTK.ValueMember = "MATK";
            repositoryItemGridTK.DisplayMember = "MATK";
            repositoryItemGridTK.NullText = "";
            colTK1.ColumnEdit = repositoryItemGridTK;

            lookUpEditNV.Properties.DataSource = ObjectService.Instance.GetLoaiCTs();
            lookUpEditNV.Properties.ValueMember = "MALOAICT";
            lookUpEditNV.Properties.DisplayMember = "MALOAICT";
            lookUpEditNV.Properties.NullText = "";

            lookUpEditKH.Properties.DataSource = ObjectService.Instance.GetKHACHHANGs();
            lookUpEditKH.Properties.ValueMember = "MAKH";
            lookUpEditKH.Properties.DisplayMember = "MAKH";
            lookUpEditKH.Properties.NullText = "";

            lookUpEditNVIEN.Properties.DataSource = ObjectService.Instance.GetNHANVIENs();
            lookUpEditNVIEN.Properties.ValueMember = "MANV";
            lookUpEditNVIEN.Properties.DisplayMember = "MANV";
            lookUpEditNVIEN.Properties.NullText = UserManager.CurrentUser.MANV; ;

            lookUpEditTK.Properties.DataSource = listTK;
            lookUpEditTK.Properties.ValueMember = "MATK";
            lookUpEditTK.Properties.DisplayMember = "MATK";
            lookUpEditTK.Properties.NullText = "632";

        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "MAHH")
            {
                string maHH = gridView1.GetRowCellValue(e.RowHandle, e.Column).ToString();
                HANGHOA hanghoa = ProductService.Instance.GetHangHoaByMaHH(maHH);
                if (hanghoa != null)
                {
                    gridView1.SetRowCellValue(e.RowHandle, "TENHH", hanghoa.TENHH);
                    gridView1.SetRowCellValue(e.RowHandle, "MADVT", hanghoa.MADVT);
                    gridView1.SetRowCellValue(e.RowHandle, "MAKHO", "KHO001");
                    gridView1.SetRowCellValue(e.RowHandle, "TKVT", hanghoa.TKKHO);
                }
                string makho = gridView1.GetFocusedRowCellValue(colMAKHO).ToString();
                double _tonKho = ProductService.Instance.getTonkho(maHH, makho);
                gridView1.SetRowCellValue(e.RowHandle, "TONKHO", _tonKho);


            }
            if (e.Column.FieldName == "MAKHO")
            {
                string maHH = gridView1.GetRowCellValue(e.RowHandle, e.Column).ToString();

                string makho = gridView1.GetRowCellValue(e.RowHandle, e.Column).ToString();
                if (maHH != null)
                {
                    double _tonKho = ProductService.Instance.getTonkho(maHH, makho);
                    gridView1.SetRowCellValue(e.RowHandle, "TONKHO", _tonKho);
                }
            }

            if (e.Column.FieldName == "SOLUONG" ||
                e.Column.FieldName == "DONGIA")
            {
                var quantity = gridView1.GetFocusedRowCellValue(colSL);
                var amount = gridView1.GetFocusedRowCellValue(colDG);


                if (quantity != null && amount != null)
                {
                    gridView1.SetFocusedRowCellValue(colTHANHTIEN, Convert.ToDecimal(quantity) * Convert.ToDecimal(amount));
                }

            }

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            using (GetVoucherForm gvc = new GetVoucherForm(1))
            {
                this.Hide();
                gvc.ShowDialog();
                loadgetbinddinglist();
                this.Show();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            HOADON HD = new HOADON()
            {
                SOCT = textEdit1.Text,
                MALOAICT = lookUpEditNV.Text.ToString(),
                MAKH = lookUpEditKH.Text.ToString(),
                MANV = lookUpEditNVIEN.Text.ToString(),
                NGAYHT = dateEdit1.DateTime,
                NGAYLAPCT = dateEdit1.DateTime,
                DIENGIAICT = textEdit2.Text.ToString(),
                TKNO = lookUpEditTK.Text.ToString()
            };
            BindingList<LoadFormINV> ListCTDH = gridControl1.MainView.DataSource as BindingList<LoadFormINV>;
            List<CTHOADON> _CTHDs = new List<CTHOADON>();

            if (ListCTDH.Count == 0)
            {
                XtraMessageBox.Show("Bạn chưa chọn mặt hàng trên lưới");
            }
            else
            {
                if (XtraMessageBox.Show("Bạn có muốn lưu dữ liệu?", "Lưu", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        foreach (var item in ListCTDH)
                        {
                            CTHOADON cTHOADON = new CTHOADON();
                            cTHOADON.SOCT = textEdit1.Text.ToString();
                            cTHOADON.MAHH = item.MAHH;
                            cTHOADON.MAKHO = item.MAKHO;
                            cTHOADON.SOLUONG = item.SOLUONG;
                            cTHOADON.DONGIA = item.DONGIA;
                            cTHOADON.THANHTIEN = item.THANHTIEN;
                            cTHOADON.TYLECK = item.TYLECK;
                            cTHOADON.TIENCK = item.TIENCK;
                            cTHOADON.MATHUE = item.MATHUE;
                            cTHOADON.TIENTHUE = item.TIENTHUE;
                            cTHOADON.TKDT = item.TKDT;
                            cTHOADON.GIAVON = item.GIAVON;
                            cTHOADON.TIENVON = item.TIENVON;
                            cTHOADON.TKVT = item.TKVT;
                            cTHOADON.TKGV = item.TKGV;
                            cTHOADON.TKCK = item.TKCK;
                            cTHOADON.TKTHUE = item.TKTHUE;
                            cTHOADON.TKDUTHUE = item.TKDUTHUE;
                            cTHOADON.TONKHO = item.TONKHO;
                            cTHOADON.SODDH = item.SODDH;
                            _CTHDs.Add(cTHOADON);
                        }
                        HD.TONGSLUONG = _CTHDs.Sum(x => x.SOLUONG);
                        HD.TONGTHANHTOAN = _CTHDs.Sum(x => x.THANHTIEN);
                        string voucherType = "";
                        if (region == 1)
                        {
                            voucherType = textEdit1.Text.ToString();
                        }
                        else
                        {
                            voucherType = lookUpEditNV.Text;
                        }
                        InvoiceService.Instance.InsertHoadon(_CTHDs, HD, voucherType, region);
                        XtraMessageBox.Show("Lưu dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        simpleButton1.Enabled = false;

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show("Không lưu được dữ liệu", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
            }
        }


        private void lookUpEditNV_EditValueChanged_1(object sender, EventArgs e)
        {
            try
            {
                var voucher = lookUpEditNV.EditValue.ToString();
                var voucherno = voucher + "000" + VoucherService.Instance.getVoucherNo(voucher);
                textEdit1.Text = voucherno;
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Lỗi hệ thống", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridControl1_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn) != null && view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString() != String.Empty)
                    Clipboard.SetText(view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString());
                else
                    XtraMessageBox.Show("Không có dữ liệ    u", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Handled = true;
            }
        }

        private void gridControl1_ProcessGridKey(object sender, KeyEventArgs e)
        {
            var grid = sender as GridControl;
            var view = grid.FocusedView as GridView;
            if (e.KeyData == Keys.Delete)
            {
                view.DeleteSelectedRows();
                e.Handled = true;
            }
        }
    }
}