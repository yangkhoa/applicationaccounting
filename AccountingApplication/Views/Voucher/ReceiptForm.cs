﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL.DBContext;
using BLL.Services;
using AccountingApplication.Settings;
using BLL.Models;
using DevExpress.XtraGrid.Views.Grid;

namespace AccountingApplication.Views.Voucher
{
    public partial class ReceiptForm : DevExpress.XtraEditors.XtraForm
    {
        //region= 0: thêm mới, 1: sửa
        int region = 0;
        //status = 0: form mới, 1: gọi từ form chọn hóa đơn
        public ReceiptForm(int status,string _SOPT )
        {
            InitializeComponent();
            loadform();

            colTHANHTIEN.OptionsColumn.ReadOnly = true;
            colSOCT.OptionsColumn.ReadOnly = true;
            if (status == 1)
            {
                simpleButton1.Enabled = false;
                colPT.OptionsColumn.ReadOnly = true;
                colTK.OptionsColumn.ReadOnly = false;
                PHIEUTHU _pt = ReceiptService.Instance.FindReceipt(_SOPT);
                lookUpEditNV.Properties.NullText = _pt.MALOAICT;
                lookUpEditKH.Properties.NullText = _pt.MAKH;
                lookUpEditNVIEN.Properties.NullText = _pt.MANV;
                lookUpEditTK.Properties.NullText = _pt.TKNOPTHU;
                dateEdit1.DateTime = Convert.ToDateTime(_pt.NGAYHT);
                dateEdit2.DateTime = Convert.ToDateTime(_pt.NGAYPTHU);
                textEditSP.Text = _pt.SOPTHU;
                textEditDG.Text = _pt.DIENGIAIPT;

                List<LoadReceipForm> loadform = ReceiptService.Instance.returnForm(_SOPT);
                gridControl1.DataSource = new BindingList<LoadReceipForm>(loadform);
            }
            else
            {

                List<CTPHIEUTHU> result = new List<CTPHIEUTHU>();
                gridControl1.DataSource = new BindingList<CTPHIEUTHU>(result);
            }
      
        }

        public void LoadFromCommonCurrent()
        {
            try
            {
                colTK.OptionsColumn.ReadOnly = false;
                colPT.OptionsColumn.ReadOnly = false;
                List<LoadReceipForm> result = Common.CurrentBindingList;
                var SOCT = result[0].SOCT.ToString();
                HOADON HD = InvoiceService.Instance.FindVoucher(SOCT);
                lookUpEditKH.Text = HD.MAKH;
                foreach (var item in result)
                {
                    item.TKCOPT = HD.TKNO;
                }
                if (result != null)
                {
                    gridControl1.DataSource = new BindingList<LoadReceipForm>(result);
                }
            }
            catch (Exception)
            {
               
            }
           
          
            Common.Clear();
        }

        private void loadform()
        {
            List<TAIKHOAN> taikhoans = ObjectService.Instance.GetTAIKHOANs();
            lookUpEditTK.Properties.DataSource = taikhoans;
            lookUpEditTK.Properties.ValueMember = "MATK";
            lookUpEditTK.Properties.DisplayMember = "MATK";
            lookUpEditTK.Properties.NullText = "";

            lookUpEditNV.Properties.DataSource = ObjectService.Instance.GetLoaiCTs();
            lookUpEditNV.Properties.ValueMember = "MALOAICT";
            lookUpEditNV.Properties.DisplayMember = "MALOAICT";
            lookUpEditNV.Properties.NullText = "";

            lookUpEditKH.Properties.DataSource = ObjectService.Instance.GetKHACHHANGs();
            lookUpEditKH.Properties.ValueMember = "MAKH";
            lookUpEditKH.Properties.DisplayMember = "MAKH";
            lookUpEditKH.Properties.NullText = "";

            lookUpEditNVIEN.Properties.DataSource = ObjectService.Instance.GetNHANVIENs();
            lookUpEditNVIEN.Properties.ValueMember = "MANV";
            lookUpEditNVIEN.Properties.DisplayMember = "MANV";
            lookUpEditNVIEN.Properties.NullText = UserManager.CurrentUser.MANV;


            repositoryItemGridPT.DataSource = taikhoans;
            repositoryItemGridPT.ValueMember = "MATK";
            repositoryItemGridPT.DisplayMember = "MATK";
            repositoryItemGridPT.NullText = "";
            colTK1.ColumnEdit = repositoryItemGridPT;

            dateEdit1.DateTime = DateTime.Now;
            dateEdit2.DateTime = DateTime.Now;

        }

        

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            PHIEUTHU PT = new PHIEUTHU()
            {
                SOPTHU = textEditSP.Text,
                MALOAICT = lookUpEditNV.Text.ToString(),
                MAKH = lookUpEditKH.Text.ToString(),
                MANV = lookUpEditNVIEN.Text.ToString(),
                NGAYHT = dateEdit1.DateTime,
                NGAYPTHU = dateEdit1.DateTime,
                DIENGIAIPT = textEditDG.Text.ToString(),
                TKNOPTHU = lookUpEditTK.Text.ToString()
            };

            dynamic ListCTDH = gridControl1.MainView.DataSource;
            List<CTPHIEUTHU> _CTPTs = new List<CTPHIEUTHU>();

            if (ListCTDH.Count == 0)
            {
                XtraMessageBox.Show("Bạn chưa chọn mặt hàng trên lưới");
            }
            else
            {
                if (XtraMessageBox.Show("Bạn có muốn lưu dữ liệu?", "Lưu", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        foreach (var item in ListCTDH)
                        {
                            CTPHIEUTHU cTPHIEUTHU = new CTPHIEUTHU();
                            cTPHIEUTHU.TKCOPT = item.TKCOPT;
                            cTPHIEUTHU.SOPTHU = textEditSP.Text.ToString();
                            cTPHIEUTHU.DATHU = item.SOTIEN;
                            cTPHIEUTHU.CONPHAITHU = item.THANHTIEN - item.SOTIEN; 
                            cTPHIEUTHU.SOCT = item.SOCT;
                            _CTPTs.Add(cTPHIEUTHU);
                        }
                        PT.TONGTIENPTHU = _CTPTs.Sum(x => x.DATHU);
                        ReceiptService.Instance.InsertPhieuThu(_CTPTs, PT, textEditSP.Text, region);
                        XtraMessageBox.Show("Lưu dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        simpleButton1.Enabled = false;
                        simpleButton2.Enabled = false;

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show("Không lưu được dữ liệu");

                    }
                }
            }
        }
       
        private void lookUpEditNV_EditValueChanged(object sender, EventArgs e)
        {
            var voucher = lookUpEditNV.EditValue.ToString();
            var voucherno = voucher + "000" + VoucherService.Instance.getVoucherNo(voucher);
            textEditSP.Text = voucherno;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            using (GetInvoiceForm gvc = new GetInvoiceForm())
            {
                this.Hide();
                gvc.ShowDialog();
                LoadFromCommonCurrent();
                this.Show();
            }
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "SOTIEN")
            {
                int Phaithu = Convert.ToInt32(gridView1.GetFocusedRowCellValue(colTHANHTIEN));
                int sotien = Convert.ToInt32(gridView1.GetFocusedRowCellValue(colPT));

                if (sotien > Phaithu)
                {
                    XtraMessageBox.Show("Sô tiền lập phiếu thu không hợp lệ");
                    gridView1.SetFocusedRowCellValue(colPT, 0);
                }
            }
        }

        private void gridControl1_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn) != null && view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString() != String.Empty)
                    Clipboard.SetText(view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString());
                else
                    XtraMessageBox.Show("The value in the selected cell is null or empty!");
                e.Handled = true;
            }
        }
    }
}