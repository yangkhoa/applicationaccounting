﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL.DBContext;
using BLL.Services;
using BLL.Models;
using DevExpress.XtraSplashScreen;
using AccountingApplication.Settings;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;

namespace AccountingApplication.Views.Voucher
{
    public partial class InvoiceForm : DevExpress.XtraEditors.XtraForm
    {
        int region = 0;
        public InvoiceForm(int Status, string _SOCT)
        {
            InitializeComponent();
            LoadForm();

            if (Status == 1)
            {
                //int ReloadMaster = Common.currentMasterForm;

                //HOADON SetHD = Common.currentGetObject;
                //if (ReloadMaster == 1)
                //{
                //    LoadHistoryForm(SetHD);
                //}

            }
            else if (Status == 2)
            {
                simpleButton2.Enabled = false;
                lookUpEditNV.Enabled = false;
                lookUpEditNVIEN.Enabled = false;
                lookUpEditNV.Enabled = false;
                lookUpEditTK.Enabled = false;
                lookUpEditKH.Enabled = false;
                dateEdit1.Enabled = false;
                textBoxSHD.Enabled = false;
                textBoxSSR.Enabled = false;
                HOADON HD = InvoiceService.Instance.FindVoucher(_SOCT);
                lookUpEditNV.Properties.NullText = HD.MALOAICT;
                textBoxSP.Text = HD.SOCT;
                lookUpEditNVIEN.Properties.NullText = HD.MANV;
                lookUpEditKH.Properties.NullText = HD.MAKH;
                lookUpEditTK.Properties.NullText = HD.TKNO;
                textBoxDG.Text = HD.DIENGIAICT;
                dateEdit1.DateTime = Convert.ToDateTime(HD.NGAYLAPCT);
                textBoxSSR.Text = HD.SOSERIHD;
                textBoxSHD.Text = HD.KYHIEUHD;

                List<LoadFormINV> _LF = InvoiceService.Instance.returnForm(_SOCT);
                gridInvoice.DataSource = new BindingList<LoadFormINV>(_LF);
                colMAHH.OptionsColumn.ReadOnly = true;
                colMATHUE.OptionsColumn.ReadOnly = true;
                colMK.OptionsColumn.ReadOnly = true;
                colLT.OptionsColumn.ReadOnly = true;
                colTYLECK.OptionsColumn.ReadOnly = true;
                colTKDUTHUE.OptionsColumn.ReadOnly = true;
                colTKGV.OptionsColumn.ReadOnly = true;
                colTKDT.OptionsColumn.ReadOnly = true;
                colTKCK.OptionsColumn.ReadOnly = true;
                colTKTHUE.OptionsColumn.ReadOnly = true;
                colGIAVON.OptionsColumn.ReadOnly = true;
                colTIENVON.OptionsColumn.ReadOnly = true;
                colTKVT.OptionsColumn.ReadOnly = true;
                colKHO.OptionsColumn.ReadOnly = true;
                region = 1;

            }
            else
            {
                if (Status == 3)
                {
                    simpleButton2.Enabled = false;
                }
                List<LoadFormINV> result = new List<LoadFormINV>();
                gridInvoice.DataSource = new BindingList<LoadFormINV>(result);
            }

        }
        public void loadGetbindinglist()
        {
            try
            {
                gridInvoice.DataSource = Common.CurrentBindingList;
                var a = Common.CurrentBindingList[0].SODDH;
                DONDATHANG DDH = VoucherService.Instance.FindVoucher(a);
                lookUpEditKH.Text = DDH.MAKH.ToString();
                lookUpEditNVIEN.Text = Common.CurrentUserGet;
                Common.Clear();
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Thao tác dữ liệu không hợp lệ");
            }
        }
        private void LoadHistoryForm(HOADON _HD)
        {
            lookUpEditNV.Text = _HD.MALOAICT;
            lookUpEditNVIEN.Text = _HD.MANV;
            lookUpEditKH.Text = _HD.MAKH;
            lookUpEditTK.Text = _HD.TKNO;
            textBoxSP.Text = _HD.SOCT;
            textBoxDG.Text = _HD.DIENGIAICT;
            textBoxSHD.Text = _HD.KYHIEUHD;
            textBoxSSR.Text = _HD.SOSERIHD;
            dateEdit1.DateTime = Convert.ToDateTime(_HD.NGAYLAPCT);

        }
        private void LoadForm()
        {
            List<HANGHOA> listHangHoa = ProductService.Instance.GetListHangHoa();
            repositoryItemGridHHS.DataSource = listHangHoa;
            repositoryItemGridHHS.ValueMember = "MAHH";
            repositoryItemGridHHS.DisplayMember = "MAHH";
            repositoryItemGridHHS.NullText = "Xin hãy chọn loại hàng hóa";
            colMAHH1.ColumnEdit = repositoryItemGridHHS;

            List<KHOHANG> listKho = ProductService.Instance.GetListKho();
            repositoryItemGridKHO.DataSource = listKho;
            repositoryItemGridKHO.ValueMember = "MAKHO";
            repositoryItemGridKHO.DisplayMember = "MAKHO";
            repositoryItemGridKHO.NullText = "Xin hãy chọn kho";
            colMK.ColumnEdit = repositoryItemGridKHO;

            List<THUEGTGT> listTHUE = ObjectService.Instance.GetTHUEGTGTs();
            repositoryItemGridMT.DataSource = listTHUE;
            repositoryItemGridMT.ValueMember = "MATHUE";
            repositoryItemGridMT.DisplayMember = "MATHUE";
            repositoryItemGridMT.NullText = "Xin hãy chọn mã thuế";
            colMT.ColumnEdit = repositoryItemGridMT;


            lookUpEditNV.Properties.DataSource = ObjectService.Instance.GetLoaiCTs();
            lookUpEditNV.Properties.ValueMember = "MALOAICT";
            lookUpEditNV.Properties.DisplayMember = "MALOAICT";
            lookUpEditNV.Properties.NullText = "";

            lookUpEditKH.Properties.DataSource = ObjectService.Instance.GetKHACHHANGs();
            lookUpEditKH.Properties.ValueMember = "MAKH";
            lookUpEditKH.Properties.DisplayMember = "MAKH";
            lookUpEditKH.Properties.NullText = "";

            lookUpEditNVIEN.Properties.DataSource = ObjectService.Instance.GetNHANVIENs();
            lookUpEditNVIEN.Properties.ValueMember = "MANV";
            lookUpEditNVIEN.Properties.DisplayMember = "MANV";
            lookUpEditNVIEN.Properties.NullText = UserManager.CurrentUser.MANV; ;

            List<TAIKHOAN> tAIKHOANs = ObjectService.Instance.GetTAIKHOANs();
            lookUpEditTK.Properties.DataSource = tAIKHOANs;
            lookUpEditTK.Properties.ValueMember = "MATK";
            lookUpEditTK.Properties.DisplayMember = "MATK";
            lookUpEditTK.Properties.NullText = "";


            repositoryItemGridLookUpEditTK.DataSource = tAIKHOANs;
            repositoryItemGridLookUpEditTK.ValueMember = "MATK";
            repositoryItemGridLookUpEditTK.DisplayMember = "MATK";
            repositoryItemGridLookUpEditTK.NullText = "Xin hãy chọn tài khoản";
            colMATK.ColumnEdit = repositoryItemGridLookUpEditTK;

            dateEdit1.DateTime = DateTime.Now;
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "MAHH")
            {
                string maHH = gridView1.GetRowCellValue(e.RowHandle, e.Column).ToString();
                HANGHOA hanghoa = ProductService.Instance.GetHangHoaByMaHH(maHH);
                if (hanghoa != null)
                {
                    gridView1.SetRowCellValue(e.RowHandle, "TENHH", hanghoa.TENHH);
                    gridView1.SetRowCellValue(e.RowHandle, "MADVT", hanghoa.MADVT);
                    gridView1.SetRowCellValue(e.RowHandle, "MAKHO", "KHO001");
                    gridView1.SetRowCellValue(e.RowHandle, "TKDT", hanghoa.TKDOANHTHU);
                }
                string makho = gridView1.GetFocusedRowCellValue(colKHO).ToString();
                double _tonKho = ProductService.Instance.getTonkho(maHH, makho);
                double _xuat = ProductService.Instance.getTongxuatkho(maHH, makho);
                double _NHTL = ProductService.Instance.getNhapkhotralai(maHH, makho);
                double _toncuoi = _tonKho - _xuat + _NHTL;
                gridView1.SetRowCellValue(e.RowHandle, "TONKHO", _toncuoi);


            }
            if (e.Column.FieldName == "MAKHO")
            {
                string maHH = gridView1.GetRowCellValue(e.RowHandle, e.Column).ToString();

                string makho = gridView1.GetRowCellValue(e.RowHandle, e.Column).ToString();
                if (maHH != null)
                {
                    double _tonKho = ProductService.Instance.getTonkho(maHH, makho);
                    gridView1.SetRowCellValue(e.RowHandle, "TONKHO", _tonKho);
                }
            }
            Double taxrate = 0;
            var tax = gridView1.GetFocusedRowCellValue(colMATHUE);
            if (e.Column.FieldName == "MATHUE")
            {

                if (tax.ToString() == "05")
                {
                    taxrate = 0.05;
                }
                else if (tax.ToString() == "10")
                {
                    taxrate = 0.1;
                }
            }
            if (e.Column.FieldName == "SOLUONG" ||
                e.Column.FieldName == "DONGIA" ||
                e.Column.FieldName == "TYLECK" ||
                e.Column.FieldName == "MATHUE")
            {
                var quantity = gridView1.GetFocusedRowCellValue(colSLDH);
                var amount = gridView1.GetFocusedRowCellValue(colDGDH);
                var reduce = gridView1.GetFocusedRowCellValue(colTYLECK);


                if (quantity != null && amount != null)
                {
                    gridView1.SetFocusedRowCellValue(colTHANHTIEN, Convert.ToDecimal(quantity) * Convert.ToDecimal(amount));
                }
                if (quantity != null && amount != null && reduce != null)
                {
                    gridView1.SetFocusedRowCellValue(colTIENCK, Convert.ToDecimal(quantity) * Convert.ToDecimal(amount) - Convert.ToDecimal(quantity) * Convert.ToDecimal(amount) * (Convert.ToDecimal(reduce) * 0.01m));
                }
                if (quantity != null && amount != null && tax != null)
                {
                    gridView1.SetFocusedRowCellValue(colTIENTHUE,
                         Convert.ToDecimal(quantity) * Convert.ToDecimal(amount) - Convert.ToDecimal(quantity) * Convert.ToDecimal(amount) * (Convert.ToDecimal(reduce) * 0.01m) +
                       (Convert.ToDecimal(quantity) * Convert.ToDecimal(amount) - Convert.ToDecimal(quantity) * Convert.ToDecimal(amount) * (Convert.ToDecimal(reduce)) * 0.01m) * Convert.ToDecimal(taxrate));
                }
            }
        }



        private void lookUpEditLHD_EditValueChanged(object sender, EventArgs e)
        {
            var voucher = lookUpEditNV.EditValue.ToString();
            var voucherno = voucher + "000" + VoucherService.Instance.getVoucherNo(voucher);
            textBoxSP.Text = voucherno;
        }


        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (textBoxSP.Text != "")
            {
                HOADON HD = new HOADON()
                {
                    MALOAICT = lookUpEditNV.Text,
                    SOCT = textBoxSP.Text,
                    MAKH = lookUpEditKH.Text,
                    MANV = lookUpEditNVIEN.Text,
                    NGAYLAPCT = dateEdit1.DateTime,
                    DIENGIAICT = textBoxDG.Text,
                    KYHIEUHD = textBoxSHD.Text,
                    SOSERIHD = textBoxSSR.Text,
                    TKNO = lookUpEditTK.Text
                };
                //Common.currentGetObject = HD;
                //Common.currentMasterForm = 1;
            }
            else
            {
                //Common.currentMasterForm = 0;
            }
            using (GetVoucherForm gvc = new GetVoucherForm(0))
            {

                this.Hide();
                gvc.ShowDialog();
                loadGetbindinglist();
                this.Show();

            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            HOADON HD = new HOADON()
            {
                SOCT = textBoxSP.Text,
                MALOAICT = lookUpEditNV.Text.ToString(),
                MAKH = lookUpEditKH.Text.ToString(),
                MANV = lookUpEditNVIEN.Text.ToString(),
                NGAYHT = dateEdit1.DateTime,
                NGAYLAPCT = dateEdit1.DateTime,
                DIENGIAICT = textBoxDG.Text.ToString(),
                KYHIEUHD = textBoxSHD.Text,
                SOSERIHD = textBoxSSR.Text,
                TKNO = lookUpEditTK.Text.ToString()
            };
            BindingList<LoadFormINV> ListCTDH = gridInvoice.MainView.DataSource as BindingList<LoadFormINV>;
            List<CTHOADON> _CTHDs = new List<CTHOADON>();

            if (ListCTDH.Count == 0)
            {
                XtraMessageBox.Show("Bạn chưa chọn mặt hàng trên lưới");
            }
            else
            {
                if (XtraMessageBox.Show("Bạn có muốn lưu dữ liệu?", "Lưu", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {

                        foreach (var item in ListCTDH)
                        {
                            CTHOADON cTHOADON = new CTHOADON();
                            cTHOADON.SOCT = textBoxSP.Text.ToString();
                            cTHOADON.MAHH = item.MAHH;
                            cTHOADON.MAKHO = item.MAKHO;
                            cTHOADON.SOLUONG = item.SOLUONG;
                            cTHOADON.DONGIA = item.DONGIA;
                            cTHOADON.THANHTIEN = item.THANHTIEN;
                            cTHOADON.TYLECK = item.TYLECK;
                            cTHOADON.TIENCK = item.TIENCK;
                            cTHOADON.MATHUE = item.MATHUE;
                            cTHOADON.TIENTHUE = item.TIENTHUE;
                            cTHOADON.TKDT = item.TKDT;
                            cTHOADON.GIAVON = item.GIAVON;
                            cTHOADON.TIENVON = item.TIENVON;
                            cTHOADON.TKVT = item.TKVT;
                            cTHOADON.TKGV = item.TKGV;
                            cTHOADON.TKCK = item.TKCK;
                            cTHOADON.TKTHUE = item.TKTHUE;
                            cTHOADON.TKDUTHUE = item.TKDUTHUE;
                            cTHOADON.TONKHO = item.TONKHO;
                            cTHOADON.SODDH = item.SODDH;
                            _CTHDs.Add(cTHOADON);
                        }
                        HD.TONGSLUONG = _CTHDs.Sum(x => x.SOLUONG);
                        double? TONGTHANHTIEN = _CTHDs.Sum(x => x.THANHTIEN);
                        double? TIENCK = _CTHDs.Sum(x => x.TIENCK) ?? 0;
                        double? TIENTHUE = _CTHDs.Sum(x => x.TIENTHUE) ?? 0;
                        if (TIENCK != 0)
                        {
                            HD.TONGTIENCK = TONGTHANHTIEN - TIENCK;
                        }
                        else
                        {
                            HD.TONGTIENCK = TIENCK;
                        }
                        if (TIENTHUE != 0 && TIENCK != 0)
                        {
                            HD.TONGTIENTHUE = TIENTHUE - TIENCK;
                        }
                        else if (TIENTHUE != 0)
                        {
                            HD.TONGTIENTHUE = TIENTHUE - TONGTHANHTIEN;
                        }
                        else
                        {
                            HD.TONGTIENTHUE = TIENTHUE;
                        }
                        HD.TONGTIENSAUCK = TIENCK;
                        if (TIENCK != 0 && TIENTHUE == 0)
                        {

                            HD.TONGTHANHTOAN = TIENCK;
                        }
                        else if (TIENCK == 0 && TIENTHUE != 0)
                        {
                            HD.TONGTHANHTOAN = TIENTHUE;
                        }
                        else if (TIENCK != 0 && TIENTHUE != 0)
                        {
                            HD.TONGTHANHTOAN = TIENTHUE;
                        }
                        else
                        {
                            HD.TONGTHANHTOAN = TONGTHANHTIEN;
                        }

                        string voucherType = "";
                        if (region == 1)
                        {
                            voucherType = textBoxSP.Text.ToString();
                        }
                        else
                        {
                            voucherType = lookUpEditNV.Text;
                        }
                        InvoiceService.Instance.InsertHoadon(_CTHDs, HD, voucherType, region);
                        XtraMessageBox.Show("Lưu dữ liệu thành công", "Thành công", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        simpleButton1.Enabled = false;

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show("Không lưu được dữ liệu");

                    }
                }
            }
        }

        private void lookUpEditNV_EditValueChanged(object sender, EventArgs e)
        {

            var voucher = lookUpEditNV.EditValue.ToString();
            var voucherno = voucher + "000" + VoucherService.Instance.getVoucherNo(voucher);
            textBoxSP.Text = voucherno;
        }

        private void gridInvoice_KeyDown(object sender, KeyEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn) != null && view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString() != String.Empty)
                    Clipboard.SetText(view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString());
                else
                    XtraMessageBox.Show("The value in the selected cell is null or empty!");
                e.Handled = true;
            }
        }

        private void gridInvoice_ProcessGridKey(object sender, KeyEventArgs e)
        {
            var grid = sender as GridControl;
            var view = grid.FocusedView as GridView;
            if (e.KeyData == Keys.Delete)
            {
                view.DeleteSelectedRows();
                e.Handled = true;
            }
        }
    }
}