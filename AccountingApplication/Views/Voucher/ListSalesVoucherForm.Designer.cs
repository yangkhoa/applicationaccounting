﻿namespace AccountingApplication.Views.Voucher
{
    partial class ListSalesVoucherForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.grdDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMAHH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKHO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSOLUONGDH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDONGIADH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTTDH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTYLECK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMATHUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTIENTHUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSODDH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMAKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMANV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNgayHT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDGDH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTHDH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPTTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSNN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHANTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bbiPrintPreview = new DevExpress.XtraBars.BarButtonItem();
            this.bsiRecordsCount = new DevExpress.XtraBars.BarStaticItem();
            this.bbiNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemRadioGroup2 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemRadioGroup1 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.repositoryItemRadioGroup5 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.repositoryItemRadioGroup3 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.repositoryItemRadioGroup4 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEditPR2 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditPR1 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.FilterTextVO = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grdDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPR2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPR1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FilterTextVO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // grdDetail
            // 
            this.grdDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMAHH,
            this.colKHO,
            this.colSOLUONGDH,
            this.colDONGIADH,
            this.colTTDH,
            this.colTYLECK,
            this.colMATHUE,
            this.colTIENTHUE});
            this.grdDetail.GridControl = this.gridControl;
            this.grdDetail.Name = "grdDetail";
            // 
            // colMAHH
            // 
            this.colMAHH.AppearanceHeader.Options.UseTextOptions = true;
            this.colMAHH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMAHH.Caption = "Mã hàng hóa";
            this.colMAHH.FieldName = "MAHH";
            this.colMAHH.MinWidth = 25;
            this.colMAHH.Name = "colMAHH";
            this.colMAHH.OptionsColumn.AllowEdit = false;
            this.colMAHH.Visible = true;
            this.colMAHH.VisibleIndex = 0;
            this.colMAHH.Width = 94;
            // 
            // colKHO
            // 
            this.colKHO.AppearanceHeader.Options.UseTextOptions = true;
            this.colKHO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKHO.Caption = "Kho";
            this.colKHO.FieldName = "KHO";
            this.colKHO.MinWidth = 25;
            this.colKHO.Name = "colKHO";
            this.colKHO.OptionsColumn.AllowEdit = false;
            this.colKHO.Visible = true;
            this.colKHO.VisibleIndex = 1;
            this.colKHO.Width = 94;
            // 
            // colSOLUONGDH
            // 
            this.colSOLUONGDH.AppearanceHeader.Options.UseTextOptions = true;
            this.colSOLUONGDH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSOLUONGDH.Caption = "Số lượng";
            this.colSOLUONGDH.FieldName = "SOLUONGDH";
            this.colSOLUONGDH.MinWidth = 25;
            this.colSOLUONGDH.Name = "colSOLUONGDH";
            this.colSOLUONGDH.OptionsColumn.AllowEdit = false;
            this.colSOLUONGDH.Visible = true;
            this.colSOLUONGDH.VisibleIndex = 2;
            this.colSOLUONGDH.Width = 94;
            // 
            // colDONGIADH
            // 
            this.colDONGIADH.AppearanceHeader.Options.UseTextOptions = true;
            this.colDONGIADH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDONGIADH.Caption = "Đơn giá";
            this.colDONGIADH.FieldName = "DONGIADH";
            this.colDONGIADH.MinWidth = 25;
            this.colDONGIADH.Name = "colDONGIADH";
            this.colDONGIADH.OptionsColumn.AllowEdit = false;
            this.colDONGIADH.Visible = true;
            this.colDONGIADH.VisibleIndex = 3;
            this.colDONGIADH.Width = 94;
            // 
            // colTTDH
            // 
            this.colTTDH.AppearanceHeader.Options.UseTextOptions = true;
            this.colTTDH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTTDH.Caption = "Thành tiền";
            this.colTTDH.FieldName = "THANHTIENDH";
            this.colTTDH.MinWidth = 25;
            this.colTTDH.Name = "colTTDH";
            this.colTTDH.OptionsColumn.AllowEdit = false;
            this.colTTDH.Visible = true;
            this.colTTDH.VisibleIndex = 4;
            this.colTTDH.Width = 94;
            // 
            // colTYLECK
            // 
            this.colTYLECK.AppearanceHeader.Options.UseTextOptions = true;
            this.colTYLECK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTYLECK.Caption = "Tỷ lệ CK";
            this.colTYLECK.FieldName = "TYLECK";
            this.colTYLECK.MinWidth = 25;
            this.colTYLECK.Name = "colTYLECK";
            this.colTYLECK.OptionsColumn.AllowEdit = false;
            this.colTYLECK.Visible = true;
            this.colTYLECK.VisibleIndex = 5;
            this.colTYLECK.Width = 94;
            // 
            // colMATHUE
            // 
            this.colMATHUE.AppearanceHeader.Options.UseTextOptions = true;
            this.colMATHUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMATHUE.Caption = "Mã thuế";
            this.colMATHUE.FieldName = "MATHUE";
            this.colMATHUE.MinWidth = 25;
            this.colMATHUE.Name = "colMATHUE";
            this.colMATHUE.OptionsColumn.AllowEdit = false;
            this.colMATHUE.Visible = true;
            this.colMATHUE.VisibleIndex = 6;
            this.colMATHUE.Width = 94;
            // 
            // colTIENTHUE
            // 
            this.colTIENTHUE.AppearanceHeader.Options.UseTextOptions = true;
            this.colTIENTHUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTIENTHUE.Caption = "Tiền thuế";
            this.colTIENTHUE.FieldName = "TIENTHUE";
            this.colTIENTHUE.MinWidth = 25;
            this.colTIENTHUE.Name = "colTIENTHUE";
            this.colTIENTHUE.OptionsColumn.AllowEdit = false;
            this.colTIENTHUE.Visible = true;
            this.colTIENTHUE.VisibleIndex = 7;
            this.colTIENTHUE.Width = 94;
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            gridLevelNode1.LevelTemplate = this.grdDetail;
            gridLevelNode1.RelationName = "Detail";
            this.gridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl.Location = new System.Drawing.Point(0, 145);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl.MenuManager = this.ribbonControl;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(933, 593);
            this.gridControl.TabIndex = 2;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView,
            this.grdDetail});
            // 
            // gridView
            // 
            this.gridView.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSODDH,
            this.colMAKH,
            this.colMANV,
            this.colNgayHT,
            this.colDGDH,
            this.colTHDH,
            this.colPTTT,
            this.colSNN,
            this.colHANTT});
            this.gridView.DetailHeight = 431;
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsBehavior.ReadOnly = true;
            this.gridView.OptionsDetail.ShowDetailTabs = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.MasterRowEmpty += new DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventHandler(this.gridView_MasterRowEmpty);
            this.gridView.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.gridView_MasterRowGetChildList);
            this.gridView.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gridView_MasterRowGetRelationName);
            this.gridView.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.gridView_MasterRowGetRelationCount);
            this.gridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView_KeyDown);
            // 
            // colSODDH
            // 
            this.colSODDH.Caption = "Số Phiếu";
            this.colSODDH.FieldName = "SODDH";
            this.colSODDH.MinWidth = 25;
            this.colSODDH.Name = "colSODDH";
            this.colSODDH.Visible = true;
            this.colSODDH.VisibleIndex = 0;
            this.colSODDH.Width = 138;
            // 
            // colMAKH
            // 
            this.colMAKH.Caption = "Mã Khách Hàng";
            this.colMAKH.FieldName = "MAKH";
            this.colMAKH.MinWidth = 25;
            this.colMAKH.Name = "colMAKH";
            this.colMAKH.Visible = true;
            this.colMAKH.VisibleIndex = 1;
            this.colMAKH.Width = 138;
            // 
            // colMANV
            // 
            this.colMANV.Caption = "Mã Nhân Viên";
            this.colMANV.FieldName = "MANV";
            this.colMANV.MinWidth = 25;
            this.colMANV.Name = "colMANV";
            this.colMANV.Visible = true;
            this.colMANV.VisibleIndex = 2;
            this.colMANV.Width = 138;
            // 
            // colNgayHT
            // 
            this.colNgayHT.Caption = "Ngày Đơn Hàng";
            this.colNgayHT.FieldName = "NGAYHT";
            this.colNgayHT.MinWidth = 25;
            this.colNgayHT.Name = "colNgayHT";
            this.colNgayHT.Visible = true;
            this.colNgayHT.VisibleIndex = 3;
            this.colNgayHT.Width = 138;
            // 
            // colDGDH
            // 
            this.colDGDH.Caption = "Diễn Giải";
            this.colDGDH.FieldName = "DIENGIAIDH";
            this.colDGDH.MinWidth = 25;
            this.colDGDH.Name = "colDGDH";
            this.colDGDH.Visible = true;
            this.colDGDH.VisibleIndex = 4;
            this.colDGDH.Width = 119;
            // 
            // colTHDH
            // 
            this.colTHDH.Caption = "Trạng Thái Đơn Hàng";
            this.colTHDH.FieldName = "TINHTRANGDH";
            this.colTHDH.MinWidth = 25;
            this.colTHDH.Name = "colTHDH";
            this.colTHDH.Visible = true;
            this.colTHDH.VisibleIndex = 5;
            this.colTHDH.Width = 119;
            // 
            // colPTTT
            // 
            this.colPTTT.Caption = "Mã Phương Thức Thanh Toán";
            this.colPTTT.FieldName = "MAPTTT";
            this.colPTTT.MinWidth = 25;
            this.colPTTT.Name = "colPTTT";
            this.colPTTT.Visible = true;
            this.colPTTT.VisibleIndex = 6;
            this.colPTTT.Width = 150;
            // 
            // colSNN
            // 
            this.colSNN.Caption = "Số Ngày Nợ";
            this.colSNN.FieldName = "SONGAYNO";
            this.colSNN.MinWidth = 25;
            this.colSNN.Name = "colSNN";
            this.colSNN.Visible = true;
            this.colSNN.VisibleIndex = 7;
            this.colSNN.Width = 94;
            // 
            // colHANTT
            // 
            this.colHANTT.Caption = "Hạn Thanh Toán";
            this.colHANTT.FieldName = "HANTT";
            this.colHANTT.MinWidth = 25;
            this.colHANTT.Name = "colHANTT";
            this.colHANTT.Visible = true;
            this.colHANTT.VisibleIndex = 8;
            this.colHANTT.Width = 94;
            // 
            // ribbonControl
            // 
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.ribbonControl.SearchEditItem,
            this.bbiPrintPreview,
            this.bsiRecordsCount,
            this.bbiNew,
            this.bbiEdit,
            this.bbiDelete,
            this.bbiRefresh,
            this.barEditItem1});
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbonControl.MaxItemId = 32;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemRadioGroup1,
            this.repositoryItemRadioGroup5});
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl.Size = new System.Drawing.Size(933, 145);
            this.ribbonControl.StatusBar = this.ribbonStatusBar;
            this.ribbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // bbiPrintPreview
            // 
            this.bbiPrintPreview.Caption = "Print Preview";
            this.bbiPrintPreview.Enabled = false;
            this.bbiPrintPreview.Id = 14;
            this.bbiPrintPreview.ImageOptions.ImageUri.Uri = "Preview";
            this.bbiPrintPreview.Name = "bbiPrintPreview";
            this.bbiPrintPreview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPrintPreview_ItemClick);
            // 
            // bsiRecordsCount
            // 
            this.bsiRecordsCount.Caption = "RECORDS : 0";
            this.bsiRecordsCount.Id = 15;
            this.bsiRecordsCount.Name = "bsiRecordsCount";
            // 
            // bbiNew
            // 
            this.bbiNew.Caption = "New";
            this.bbiNew.Id = 16;
            this.bbiNew.ImageOptions.ImageUri.Uri = "New";
            this.bbiNew.Name = "bbiNew";
            this.bbiNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNew_ItemClick);
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "Edit";
            this.bbiEdit.Enabled = false;
            this.bbiEdit.Id = 17;
            this.bbiEdit.ImageOptions.ImageUri.Uri = "Edit";
            this.bbiEdit.Name = "bbiEdit";
            this.bbiEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEdit_ItemClick);
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Delete";
            this.bbiDelete.Enabled = false;
            this.bbiDelete.Id = 18;
            this.bbiDelete.ImageOptions.ImageUri.Uri = "Delete";
            this.bbiDelete.Name = "bbiDelete";
            this.bbiDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDelete_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 19;
            this.bbiRefresh.ImageOptions.ImageUri.Uri = "Refresh";
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemRadioGroup2;
            this.barEditItem1.Id = 22;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemRadioGroup2
            // 
            this.repositoryItemRadioGroup2.Name = "repositoryItemRadioGroup2";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2});
            this.ribbonPage1.MergeOrder = 0;
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Home";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.AllowTextClipping = false;
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiNew);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiEdit);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiDelete);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiRefresh);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Tasks";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.AllowTextClipping = false;
            this.ribbonPageGroup2.ItemLinks.Add(this.bbiPrintPreview);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "Print and Export";
            // 
            // repositoryItemRadioGroup1
            // 
            this.repositoryItemRadioGroup1.Name = "repositoryItemRadioGroup1";
            // 
            // repositoryItemRadioGroup5
            // 
            this.repositoryItemRadioGroup5.Name = "repositoryItemRadioGroup5";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.bsiRecordsCount);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 704);
            this.ribbonStatusBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbonControl;
            this.ribbonStatusBar.Size = new System.Drawing.Size(933, 34);
            // 
            // repositoryItemRadioGroup3
            // 
            this.repositoryItemRadioGroup3.Name = "repositoryItemRadioGroup3";
            // 
            // repositoryItemRadioGroup4
            // 
            this.repositoryItemRadioGroup4.Name = "repositoryItemRadioGroup4";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemRadioGroup5;
            this.barEditItem2.Id = 28;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(556, 51);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(5, 16);
            this.labelControl3.TabIndex = 35;
            this.labelControl3.Text = "-";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(556, 88);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(5, 16);
            this.labelControl2.TabIndex = 34;
            this.labelControl2.Text = "-";
            // 
            // lookUpEditPR2
            // 
            this.lookUpEditPR2.Location = new System.Drawing.Point(567, 45);
            this.lookUpEditPR2.MenuManager = this.ribbonControl;
            this.lookUpEditPR2.Name = "lookUpEditPR2";
            this.lookUpEditPR2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPR2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Month", "Kỳ"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Year", "Năm", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MY", "Quý")});
            this.lookUpEditPR2.Size = new System.Drawing.Size(105, 22);
            this.lookUpEditPR2.TabIndex = 33;
            // 
            // lookUpEditPR1
            // 
            this.lookUpEditPR1.Location = new System.Drawing.Point(445, 45);
            this.lookUpEditPR1.MenuManager = this.ribbonControl;
            this.lookUpEditPR1.Name = "lookUpEditPR1";
            this.lookUpEditPR1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPR1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Month", "Kỳ"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Year", "Năm", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MY", "Quý", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditPR1.Size = new System.Drawing.Size(105, 22);
            this.lookUpEditPR1.TabIndex = 32;
            // 
            // dateEdit2
            // 
            this.dateEdit2.EditValue = new System.DateTime(2020, 2, 22, 14, 6, 23, 715);
            this.dateEdit2.Location = new System.Drawing.Point(567, 85);
            this.dateEdit2.MenuManager = this.ribbonControl;
            this.dateEdit2.Name = "dateEdit2";
            this.dateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Size = new System.Drawing.Size(105, 22);
            this.dateEdit2.TabIndex = 31;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = new System.DateTime(2020, 2, 29, 0, 0, 0, 0);
            this.dateEdit1.Location = new System.Drawing.Point(445, 85);
            this.dateEdit1.MenuManager = this.ribbonControl;
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(105, 22);
            this.dateEdit1.TabIndex = 30;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(821, 81);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(94, 29);
            this.simpleButton1.TabIndex = 28;
            this.simpleButton1.Text = "Lọc";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // FilterTextVO
            // 
            this.FilterTextVO.Location = new System.Drawing.Point(766, 45);
            this.FilterTextVO.MenuManager = this.ribbonControl;
            this.FilterTextVO.Name = "FilterTextVO";
            this.FilterTextVO.Size = new System.Drawing.Size(149, 22);
            this.FilterTextVO.TabIndex = 27;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(695, 45);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(53, 17);
            this.labelControl1.TabIndex = 26;
            this.labelControl1.Text = "Số phiếu";
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(381, 36);
            this.radioGroup1.MenuManager = this.ribbonControl;
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroup1.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroup1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Kỳ", true, true),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Ngày", true, false)});
            this.radioGroup1.Size = new System.Drawing.Size(58, 83);
            this.radioGroup1.TabIndex = 42;
            // 
            // popupMenu1
            // 
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.Ribbon = this.ribbonControl;
            // 
            // ListSalesVoucherForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radioGroup1);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.lookUpEditPR2);
            this.Controls.Add(this.lookUpEditPR1);
            this.Controls.Add(this.dateEdit2);
            this.Controls.Add(this.dateEdit1);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.FilterTextVO);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.ribbonControl);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ListSalesVoucherForm";
            this.Size = new System.Drawing.Size(933, 738);
            ((System.ComponentModel.ISupportInitialize)(this.grdDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPR2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPR1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FilterTextVO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem bbiPrintPreview;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarStaticItem bsiRecordsCount;
        private DevExpress.XtraBars.BarButtonItem bbiNew;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraGrid.Columns.GridColumn colSODDH;
        private DevExpress.XtraGrid.Columns.GridColumn colMAKH;
        private DevExpress.XtraGrid.Columns.GridColumn colMANV;
        private DevExpress.XtraGrid.Columns.GridColumn colNgayHT;
        private DevExpress.XtraGrid.Columns.GridColumn colDGDH;
        private DevExpress.XtraGrid.Columns.GridColumn colTHDH;
        private DevExpress.XtraGrid.Columns.GridColumn colPTTT;
        private DevExpress.XtraGrid.Columns.GridColumn colSNN;
        private DevExpress.XtraGrid.Columns.GridColumn colHANTT;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup1;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup2;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup3;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup4;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup5;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPR2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPR1;
        private DevExpress.XtraEditors.DateEdit dateEdit2;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.TextEdit FilterTextVO;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraGrid.Views.Grid.GridView grdDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colMAHH;
        private DevExpress.XtraGrid.Columns.GridColumn colKHO;
        private DevExpress.XtraGrid.Columns.GridColumn colSOLUONGDH;
        private DevExpress.XtraGrid.Columns.GridColumn colDONGIADH;
        private DevExpress.XtraGrid.Columns.GridColumn colTTDH;
        private DevExpress.XtraGrid.Columns.GridColumn colTYLECK;
        private DevExpress.XtraGrid.Columns.GridColumn colMATHUE;
        private DevExpress.XtraGrid.Columns.GridColumn colTIENTHUE;
    }
}
