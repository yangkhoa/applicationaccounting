﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using DAL.DBContext;
using BLL.Services;
using AccountingApplication.Settings;

namespace AccountingApplication.Views.Voucher
{
    public partial class ListVoucherForm : DevExpress.XtraEditors.XtraUserControl
    {
        public ListVoucherForm()
        {
            InitializeComponent();

            Text = ViewForm.DHB;

            BindingList<DONDATHANG> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;

            gridView.OptionsView.EnableAppearanceEvenRow = true;
            gridView.OptionsView.EnableAppearanceOddRow = true;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        public BindingList<DONDATHANG> GetDataSource()
        {
            List<DONDATHANG> DDH = VoucherService.Instance.GetDONDATHANGs();
            BindingList<DONDATHANG> result = new  BindingList<DONDATHANG>(DDH);
            return result;
        }
        public class Customer
        {
            [Key, Display(AutoGenerateField = false)]
            public int ID { get; set; }
            [Required]
            public string Name { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            [Display(Name = "Zip Code")]
            public string ZipCode { get; set; }
            public string Phone { get; set; }
        }
    }
}
