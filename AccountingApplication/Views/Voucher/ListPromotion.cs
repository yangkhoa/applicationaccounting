﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using AccountingApplication.Settings;
using BLL.Models;
using BLL.Services;
using DAL.DBContext;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraSplashScreen;

namespace AccountingApplication.Views.Voucher
{
    public partial class ListPromotion : DevExpress.XtraEditors.XtraUserControl
    {
        MenuUser menu;
        public ListPromotion()
        {
            InitializeComponent();
            Text = ViewForm.GG;

            ///Phân quyền button
            List<MenuUser> menus = AccountService.Instance.GetListMenuByRole(UserManager.CurrentUser.MACHUCVU);
            menu = menus.FirstOrDefault(x => x.TENNQ == ViewForm.GG);
            if (menu != null)
            {
                bbiNew.Enabled = menu.THEM == 1 ? true : false;
                bbiEdit.Enabled = menu.SUA == 1 ? true : false;
                bbiDelete.Enabled = menu.XOA == 1 ? true : false;
            }

            Loadperiod();
            radioGroup1.BackColor = Color.Transparent;
            Point pos = this.PointToScreen(radioGroup1.Location);
            radioGroup1.Parent = ribbonControl;
            radioGroup1.Location = ribbonControl.PointToClient(pos);
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }

        public void Loadperiod()
        {
            List<Period> periods = VoucherService.Instance.GetPeriods();
            lookUpEditPR1.Properties.DataSource = periods;
            lookUpEditPR1.Properties.ValueMember = "MY";
            lookUpEditPR1.Properties.DisplayMember = "MY";
            lookUpEditPR1.Properties.NullText = periods.FirstOrDefault().MY.ToString();

            lookUpEditPR2.Properties.DataSource = periods;
            lookUpEditPR2.Properties.ValueMember = "MY";
            lookUpEditPR2.Properties.DisplayMember = "MY";
            lookUpEditPR2.Properties.NullText = periods.FirstOrDefault().MY.ToString();
        }
        public BindingList<HOADON> GetDataSource()
        {
            Filter filters = new Filter();
            bool a = Convert.ToBoolean(radioGroup1.Properties.Items[radioGroup1.SelectedIndex].Tag);
            var period1 = lookUpEditPR1.Text.ToString();
            var period2 = lookUpEditPR2.Text.ToString();
            if (a)
            {
                filters.periodM1 = Convert.ToInt32(period1.Substring(0, period1.IndexOf('/')));
                filters.periodY1 = Convert.ToInt32(period1.Substring(period1.IndexOf('/') + 1));
                filters.periodM2 = Convert.ToInt32(period2.Substring(0, period2.IndexOf('/')));
                filters.periodY2 = Convert.ToInt32(period2.Substring(period2.IndexOf('/') + 1));
            }
            else
            {
                filters.Datetime1 = dateEdit1.DateTime;
                filters.datetime2 = dateEdit2.DateTime;
            }

            filters.VOtext = FilterTextVO.Text.ToString();
            List<HOADON> HD = InvoiceService.Instance.GetHOADONs(filters,"HDGG");
            BindingList<HOADON> result = new BindingList<HOADON>(HD);
            return result;
        }

  

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            if (menu != null)
            {
                bbiEdit.Enabled = menu.SUA == 1 ? true : false;
                bbiDelete.Enabled = menu.XOA == 1 ? true : false;
            }
            bbiPrintPreview.Enabled = true;




            BindingList<HOADON> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            var abba = gridView.Columns[5];
            gridView.OptionsView.EnableAppearanceEvenRow = true;
            gridView.OptionsView.EnableAppearanceOddRow = true;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }

        private void gridView_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn) != null && view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString() != String.Empty)
                    Clipboard.SetText(view.GetRowCellValue(view.FocusedRowHandle, view.FocusedColumn).ToString());
                else
                    XtraMessageBox.Show("The value in the selected cell is null or empty!");
                e.Handled = true;
            }
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (InvoiceForm inv = new InvoiceForm(3, ""))
            {
                SplashScreenManager.CloseForm(false);
                inv.ShowDialog();
                this.Show();
            }
        }

        private void bbiEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "SOCT"));

            using (InvoiceForm dashboard = new InvoiceForm(2, a))
            {
                SplashScreenManager.CloseForm(false);
                dashboard.ShowDialog();
                this.Show();
            }
        }

        private void bbiDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "SOCT"));
            if (XtraMessageBox.Show("Bạn có muốn xóa hóa đơn: " + a, "Remove", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    InvoiceService.Instance.removeHD(a);
                }
                catch (Exception)
                {

                    XtraMessageBox.Show("Xóa dữ liệu không thành công");
                }

            }
            BindingList<HOADON> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
        }

        private void gridView_MasterRowEmpty(object sender, MasterRowEmptyEventArgs e)
        {
            GridView view = sender as GridView;
            HOADON HD = view.GetRow(e.RowHandle) as HOADON;
            List<LoadFormINV> CTHD = InvoiceService.Instance.returnForm(HD.SOCT.ToString());
            if (HD != null && CTHD != null)
            {
                e.IsEmpty = false;
            }
        }

        private void gridView_MasterRowGetChildList(object sender, MasterRowGetChildListEventArgs e)
        {
            GridView view = sender as GridView;
            HOADON HD = view.GetRow(e.RowHandle) as HOADON;
            List<LoadFormINV> CTHD = InvoiceService.Instance.returnForm(HD.SOCT.ToString());
            if (HD != null)
            {
                e.ChildList = CTHD;
            }
        }

        private void gridView_MasterRowGetRelationCount(object sender, MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;

        }

        private void gridView_MasterRowGetRelationName(object sender, MasterRowGetRelationNameEventArgs e)
        {
            e.RelationName = "Detail";

        }
    }
}
