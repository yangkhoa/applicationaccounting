﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL.DBContext;
using BLL.Services;

namespace AccountingApplication.Views.Object
{
    public partial class FormCustomer : DevExpress.XtraEditors.XtraForm
    {
        int statusForm = 0;
        public FormCustomer(int status,string MAKH)
        {
            InitializeComponent();
            statusForm = status;
            if (status == 1 || status == 2)
            {
                if (status == 2)
                {
                    simpleButton1.Enabled = false;
                }
                KHACHHANG KHDetail = ObjectService.Instance.GetCTKH(MAKH);
                textEditMAKH.Text = KHDetail.MAKH.ToString();
                textEditMAKH.Enabled = false;
                textEditDC.Text = KHDetail.DIACHI.ToString();
                textEditEMail.Text = KHDetail.EMAILKH.ToString();
                textEditMST.Text = KHDetail.MST.ToString();
                textEditSDT.Text = KHDetail.SDTKH.ToString();
                textEditTKH.Text = KHDetail.TENKH.ToString();
            }
        }

 
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                KHACHHANG KH = new KHACHHANG()
                {
                    MAKH = textEditMAKH.Text,
                    TENKH = textEditTKH.Text,
                    EMAILKH = textEditEMail.Text,
                    SDTKH = textEditSDT.Text,
                    MST = textEditMST.Text,
                    DIACHI = textEditDC.Text

                };
                if (statusForm == 0)
                {

                    ObjectService.Instance.InsertKH(KH);
                }
                else
                {
                    ObjectService.Instance.UpdateKH(KH);

                }
                simpleButton1.Enabled = false;
                XtraMessageBox.Show("Lưu dữ liệu thành công");
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Không lưu được dữ liệu");
            }

        }
    }
}