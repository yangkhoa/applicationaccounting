﻿namespace AccountingApplication.Views.Object
{
    partial class FormEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEmployee));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lookUpEditGT = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEditNS = new DevExpress.XtraEditors.DateEdit();
            this.textEditSDT = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEditCV = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditPB = new DevExpress.XtraEditors.LookUpEdit();
            this.textEditEMail = new DevExpress.XtraEditors.TextEdit();
            this.textEditQQ = new DevExpress.XtraEditors.TextEdit();
            this.textEditCMND = new DevExpress.XtraEditors.TextEdit();
            this.textEditTNV = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEditMANV = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditGT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditNS.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditNS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditCV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEMail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditQQ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCMND.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMANV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lookUpEditGT);
            this.panelControl1.Controls.Add(this.dateEditNS);
            this.panelControl1.Controls.Add(this.textEditSDT);
            this.panelControl1.Controls.Add(this.lookUpEditCV);
            this.panelControl1.Controls.Add(this.lookUpEditPB);
            this.panelControl1.Controls.Add(this.textEditEMail);
            this.panelControl1.Controls.Add(this.textEditQQ);
            this.panelControl1.Controls.Add(this.textEditCMND);
            this.panelControl1.Controls.Add(this.textEditTNV);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.textEditMANV);
            this.panelControl1.Location = new System.Drawing.Point(13, 13);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(836, 222);
            this.panelControl1.TabIndex = 0;
            // 
            // lookUpEditGT
            // 
            this.lookUpEditGT.Location = new System.Drawing.Point(684, 167);
            this.lookUpEditGT.Name = "lookUpEditGT";
            this.lookUpEditGT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditGT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MAGT", "Giới tính")});
            this.lookUpEditGT.Size = new System.Drawing.Size(125, 22);
            this.lookUpEditGT.TabIndex = 19;
            // 
            // dateEditNS
            // 
            this.dateEditNS.EditValue = null;
            this.dateEditNS.Location = new System.Drawing.Point(456, 170);
            this.dateEditNS.Name = "dateEditNS";
            this.dateEditNS.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditNS.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditNS.Size = new System.Drawing.Size(125, 22);
            this.dateEditNS.TabIndex = 18;
            // 
            // textEditSDT
            // 
            this.textEditSDT.Location = new System.Drawing.Point(488, 63);
            this.textEditSDT.Name = "textEditSDT";
            this.textEditSDT.Size = new System.Drawing.Size(326, 22);
            this.textEditSDT.TabIndex = 17;
            // 
            // lookUpEditCV
            // 
            this.lookUpEditCV.Location = new System.Drawing.Point(684, 18);
            this.lookUpEditCV.Name = "lookUpEditCV";
            this.lookUpEditCV.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditCV.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MACHUCVU", "Mã"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENCHUCVU", "Tên chức vụ")});
            this.lookUpEditCV.Size = new System.Drawing.Size(130, 22);
            this.lookUpEditCV.TabIndex = 16;
            // 
            // lookUpEditPB
            // 
            this.lookUpEditPB.Location = new System.Drawing.Point(453, 18);
            this.lookUpEditPB.Name = "lookUpEditPB";
            this.lookUpEditPB.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPB.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MAPB", "Mã", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENPB", "Tên phòng ban")});
            this.lookUpEditPB.Size = new System.Drawing.Size(125, 22);
            this.lookUpEditPB.TabIndex = 15;
            // 
            // textEditEMail
            // 
            this.textEditEMail.Location = new System.Drawing.Point(488, 114);
            this.textEditEMail.Name = "textEditEMail";
            this.textEditEMail.Size = new System.Drawing.Size(326, 22);
            this.textEditEMail.TabIndex = 14;
            // 
            // textEditQQ
            // 
            this.textEditQQ.Location = new System.Drawing.Point(136, 170);
            this.textEditQQ.Name = "textEditQQ";
            this.textEditQQ.Size = new System.Drawing.Size(202, 22);
            this.textEditQQ.TabIndex = 13;
            // 
            // textEditCMND
            // 
            this.textEditCMND.Location = new System.Drawing.Point(136, 117);
            this.textEditCMND.Name = "textEditCMND";
            this.textEditCMND.Size = new System.Drawing.Size(199, 22);
            this.textEditCMND.TabIndex = 12;
            // 
            // textEditTNV
            // 
            this.textEditTNV.Location = new System.Drawing.Point(136, 66);
            this.textEditTNV.Name = "textEditTNV";
            this.textEditTNV.Size = new System.Drawing.Size(199, 22);
            this.textEditTNV.TabIndex = 11;
            // 
            // labelControl10
            // 
            this.labelControl10.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl10.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl10.ImageOptions.Image")));
            this.labelControl10.Location = new System.Drawing.Point(587, 164);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(85, 36);
            this.labelControl10.TabIndex = 10;
            this.labelControl10.Text = "Giới tính";
            // 
            // labelControl9
            // 
            this.labelControl9.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl9.ImageOptions.Image")));
            this.labelControl9.Location = new System.Drawing.Point(353, 160);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(92, 36);
            this.labelControl9.TabIndex = 9;
            this.labelControl9.Text = "Ngày sinh";
            // 
            // labelControl8
            // 
            this.labelControl8.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl8.ImageOptions.Image")));
            this.labelControl8.Location = new System.Drawing.Point(3, 163);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(92, 36);
            this.labelControl8.TabIndex = 8;
            this.labelControl8.Text = "Quê quán";
            // 
            // labelControl7
            // 
            this.labelControl7.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl7.ImageOptions.Image")));
            this.labelControl7.Location = new System.Drawing.Point(353, 107);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(68, 36);
            this.labelControl7.TabIndex = 7;
            this.labelControl7.Text = "Email";
            // 
            // labelControl6
            // 
            this.labelControl6.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl6.ImageOptions.Image")));
            this.labelControl6.Location = new System.Drawing.Point(0, 100);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(71, 36);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "CMND";
            // 
            // labelControl5
            // 
            this.labelControl5.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl5.ImageOptions.Image")));
            this.labelControl5.Location = new System.Drawing.Point(353, 57);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(116, 36);
            this.labelControl5.TabIndex = 5;
            this.labelControl5.Text = "Số điện thoại";
            // 
            // labelControl4
            // 
            this.labelControl4.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl4.ImageOptions.Image")));
            this.labelControl4.Location = new System.Drawing.Point(-1, 51);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(118, 36);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "Tên nhân viên";
            // 
            // labelControl3
            // 
            this.labelControl3.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl3.ImageOptions.Image")));
            this.labelControl3.Location = new System.Drawing.Point(353, 11);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(97, 36);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Phòng ban";
            // 
            // labelControl2
            // 
            this.labelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl2.ImageOptions.Image")));
            this.labelControl2.Location = new System.Drawing.Point(588, 12);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 36);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Chức vụ";
            // 
            // labelControl1
            // 
            this.labelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.ImageOptions.Image")));
            this.labelControl1.Location = new System.Drawing.Point(6, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(113, 36);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Mã nhân viên";
            // 
            // textEditMANV
            // 
            this.textEditMANV.Location = new System.Drawing.Point(136, 18);
            this.textEditMANV.Name = "textEditMANV";
            this.textEditMANV.Size = new System.Drawing.Size(199, 22);
            this.textEditMANV.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButton1);
            this.panelControl2.Location = new System.Drawing.Point(13, 256);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(836, 39);
            this.panelControl2.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(737, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(94, 29);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Lưu";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // FormEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 309);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "FormEmployee";
            this.Text = "FormEmployee";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditGT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditNS.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditNS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditCV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEMail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditQQ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCMND.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMANV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditGT;
        private DevExpress.XtraEditors.DateEdit dateEditNS;
        private DevExpress.XtraEditors.TextEdit textEditSDT;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditCV;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPB;
        private DevExpress.XtraEditors.TextEdit textEditEMail;
        private DevExpress.XtraEditors.TextEdit textEditQQ;
        private DevExpress.XtraEditors.TextEdit textEditCMND;
        private DevExpress.XtraEditors.TextEdit textEditTNV;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEditMANV;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}