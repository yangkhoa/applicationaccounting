﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL.DBContext;
using BLL.Services;

namespace AccountingApplication.Views.Object
{
    public partial class FormInventory : DevExpress.XtraEditors.XtraForm
    {
        public FormInventory(int Status, string ID)
        {
            InitializeComponent();
            GetDataSource();
            if (Status == 0)
            {
                simpleButton1.Enabled = false;
            }
            if (Status == 1 || Status == 2)
            {
                simpleButton2.Enabled = false;
                if (Status == 2)
                {
                    lookUpEditMADVT.Enabled = false;
                    lookUpEditMLHH.Enabled = false;
                    TENHH.Enabled = false;
                    MOTA.Enabled = false;
                    TKDT.Enabled = false;
                    TKGV.Enabled = false;
                    TKK.Enabled = false;
                    simpleButton1.Enabled = false;
                }
                HANGHOA HHs = ObjectService.Instance.GetCTHH(ID);
                MAHH.Text = HHs.MAHH;
                MAHH.Enabled = false;
                lookUpEditMADVT.Properties.NullText = HHs.MADVT;
                lookUpEditMLHH.Properties.NullText = HHs.MALOAIHH;
                MOTA.Text = HHs.MOTA;
                TENHH.Text = HHs.TENHH;
                TKDT.Properties.NullText = HHs.TKDOANHTHU;
                TKGV.Properties.NullText = HHs.TKGIAVON;
                TKK.Properties.NullText = HHs.TKKHO;
            }
        }

        public void GetDataSource()
        {
            lookUpEditMADVT.Properties.DataSource = ObjectService.Instance.GetDONVITINHs();
            lookUpEditMADVT.Properties.DisplayMember = "TENDVT";
            lookUpEditMADVT.Properties.ValueMember = "MADVT";
            lookUpEditMADVT.Properties.NullText = "";

            lookUpEditMLHH.Properties.DataSource = ObjectService.Instance.GetLOAIHHs();
            lookUpEditMLHH.Properties.DisplayMember = "MALOAIHH";
            lookUpEditMLHH.Properties.ValueMember = "MALOAIHH";
            lookUpEditMLHH.Properties.NullText = "";

            List<TAIKHOAN> TK = ObjectService.Instance.GetTAIKHOANs();
            TKDT.Properties.DataSource = TK;
            TKDT.Properties.DisplayMember = "MATK";
            TKDT.Properties.ValueMember = "MATK";
            TKDT.Properties.NullText = "";

            TKGV.Properties.DataSource = TK;
            TKGV.Properties.DisplayMember = "MATK";
            TKGV.Properties.ValueMember = "MATK";
            TKGV.Properties.NullText = "";

            TKK.Properties.DataSource = TK;
            TKK.Properties.DisplayMember = "MATK";
            TKK.Properties.ValueMember = "MATK";
            TKK.Properties.NullText = "";
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                HANGHOA HH = new HANGHOA() {
                    MAHH = MAHH.Text.ToString(),
                    MALOAIHH = lookUpEditMLHH.Text.ToString(),
                    MADVT = lookUpEditMADVT.Text.ToString(),
                    TENHH = TENHH.Text.ToString(),
                    MOTA = MOTA.Text.ToString(),
                    TKDOANHTHU = TKDT.Text.ToString(),
                    TKGIAVON = TKGV.Text.ToString(),
                    TKKHO = TKK.Text.ToString()
                };
                ObjectService.Instance.InsertHH(HH);
                XtraMessageBox.Show("Lưu dữ liệu thành công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                simpleButton2.Enabled = false;
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Không lưu được dữ liệu, bạn nhập thông tin sai", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                HANGHOA HH = new HANGHOA()
                {
                    MAHH = MAHH.Text.ToString(),
                    MALOAIHH = lookUpEditMLHH.Properties.NullText.ToString(),
                    MADVT = lookUpEditMADVT.Properties.NullText.ToString(),
                    TENHH = TENHH.EditValue.ToString(),
                    MOTA = MOTA.EditValue.ToString(),
                    TKDOANHTHU = TKDT.Properties.NullText.ToString(),
                    TKGIAVON = TKGV.Properties.NullText.ToString(),
                    TKKHO = TKK.Properties.ValueMember.ToString()
                };
                ObjectService.Instance.UpdateHH(HH);
                XtraMessageBox.Show("Lưu dữ liệu thành công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                simpleButton1.Enabled = false;

            }
            catch (Exception)
            {
                XtraMessageBox.Show("Không lưu được dữ liệu, bạn nhập thông tin sai", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        
    }
}