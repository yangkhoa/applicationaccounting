﻿namespace AccountingApplication.Views.Object
{
    partial class FormInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInventory));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.MOTA = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEditMADVT = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditMLHH = new DevExpress.XtraEditors.LookUpEdit();
            this.TENHH = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.colTENHH = new DevExpress.XtraEditors.LabelControl();
            this.MALOAIHH = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.MAHH = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.TKK = new DevExpress.XtraEditors.LookUpEdit();
            this.TKGV = new DevExpress.XtraEditors.LookUpEdit();
            this.TKDT = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MOTA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMADVT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMLHH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TENHH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MAHH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TKK.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TKGV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TKDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.MOTA);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.lookUpEditMADVT);
            this.panelControl1.Controls.Add(this.lookUpEditMLHH);
            this.panelControl1.Controls.Add(this.TENHH);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.colTENHH);
            this.panelControl1.Controls.Add(this.MALOAIHH);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.MAHH);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(751, 111);
            this.panelControl1.TabIndex = 0;
            // 
            // MOTA
            // 
            this.MOTA.Location = new System.Drawing.Point(424, 64);
            this.MOTA.Name = "MOTA";
            this.MOTA.Size = new System.Drawing.Size(321, 22);
            this.MOTA.TabIndex = 9;
            // 
            // labelControl8
            // 
            this.labelControl8.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl8.ImageOptions.Image")));
            this.labelControl8.Location = new System.Drawing.Point(301, 58);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(74, 36);
            this.labelControl8.TabIndex = 8;
            this.labelControl8.Text = "Mô Tả";
            // 
            // lookUpEditMADVT
            // 
            this.lookUpEditMADVT.Location = new System.Drawing.Point(170, 84);
            this.lookUpEditMADVT.Name = "lookUpEditMADVT";
            this.lookUpEditMADVT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditMADVT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MADVT", "Mã Đơn Vị Tính"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENDVT", "Tên Đơn Vị Tính")});
            this.lookUpEditMADVT.Size = new System.Drawing.Size(125, 22);
            this.lookUpEditMADVT.TabIndex = 7;
            // 
            // lookUpEditMLHH
            // 
            this.lookUpEditMLHH.Location = new System.Drawing.Point(170, 40);
            this.lookUpEditMLHH.Name = "lookUpEditMLHH";
            this.lookUpEditMLHH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditMLHH.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MALOAIHH", "Mã Loại Hàng Hóa", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENLOAIHH", "Tên Loại Hàng Hóa")});
            this.lookUpEditMLHH.Size = new System.Drawing.Size(125, 22);
            this.lookUpEditMLHH.TabIndex = 6;
            // 
            // TENHH
            // 
            this.TENHH.Location = new System.Drawing.Point(424, 12);
            this.TENHH.Name = "TENHH";
            this.TENHH.Size = new System.Drawing.Size(322, 22);
            this.TENHH.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl4.ImageOptions.Image")));
            this.labelControl4.Location = new System.Drawing.Point(6, 70);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(107, 36);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "Đơn Vị Tính";
            // 
            // colTENHH
            // 
            this.colTENHH.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.colTENHH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colTENHH.ImageOptions.Image")));
            this.colTENHH.Location = new System.Drawing.Point(300, 5);
            this.colTENHH.Name = "colTENHH";
            this.colTENHH.Size = new System.Drawing.Size(118, 36);
            this.colTENHH.TabIndex = 3;
            this.colTENHH.Text = "Tên Hàng Hóa";
            // 
            // MALOAIHH
            // 
            this.MALOAIHH.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.MALOAIHH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("MALOAIHH.ImageOptions.Image")));
            this.MALOAIHH.Location = new System.Drawing.Point(5, 34);
            this.MALOAIHH.Name = "MALOAIHH";
            this.MALOAIHH.Size = new System.Drawing.Size(150, 36);
            this.MALOAIHH.TabIndex = 2;
            this.MALOAIHH.Text = " Mã Loại Hàng Hóa";
            // 
            // labelControl1
            // 
            this.labelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.ImageOptions.Image")));
            this.labelControl1.Location = new System.Drawing.Point(6, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(113, 36);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Mã Hàng Hóa";
            // 
            // MAHH
            // 
            this.MAHH.Location = new System.Drawing.Point(170, 5);
            this.MAHH.Name = "MAHH";
            this.MAHH.Size = new System.Drawing.Size(125, 22);
            this.MAHH.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.TKK);
            this.panelControl2.Controls.Add(this.TKGV);
            this.panelControl2.Controls.Add(this.TKDT);
            this.panelControl2.Controls.Add(this.labelControl7);
            this.panelControl2.Controls.Add(this.labelControl6);
            this.panelControl2.Controls.Add(this.labelControl5);
            this.panelControl2.Location = new System.Drawing.Point(12, 142);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(750, 49);
            this.panelControl2.TabIndex = 1;
            // 
            // TKK
            // 
            this.TKK.Location = new System.Drawing.Point(620, 11);
            this.TKK.Name = "TKK";
            this.TKK.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TKK.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MATK", "Mã Tài Khoản"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENTK", "Tên Tài Khoản")});
            this.TKK.Size = new System.Drawing.Size(125, 22);
            this.TKK.TabIndex = 12;
            // 
            // TKGV
            // 
            this.TKGV.Location = new System.Drawing.Point(395, 11);
            this.TKGV.Name = "TKGV";
            this.TKGV.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TKGV.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MATK", "Mã Tài Khoản"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENTK", "Tên Tài Khoản")});
            this.TKGV.Size = new System.Drawing.Size(125, 22);
            this.TKGV.TabIndex = 11;
            // 
            // TKDT
            // 
            this.TKDT.Location = new System.Drawing.Point(143, 11);
            this.TKDT.Name = "TKDT";
            this.TKDT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TKDT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MATK", "Mã Tài Khoản"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TENTK", "Tên Tài Khoản")});
            this.TKDT.Size = new System.Drawing.Size(125, 22);
            this.TKDT.TabIndex = 10;
            // 
            // labelControl7
            // 
            this.labelControl7.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl7.ImageOptions.Image")));
            this.labelControl7.Location = new System.Drawing.Point(537, 7);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(77, 36);
            this.labelControl7.TabIndex = 2;
            this.labelControl7.Text = "TK Kho";
            // 
            // labelControl6
            // 
            this.labelControl6.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl6.ImageOptions.Image")));
            this.labelControl6.Location = new System.Drawing.Point(286, 8);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(103, 36);
            this.labelControl6.TabIndex = 1;
            this.labelControl6.Text = "TK Giá Vốn";
            // 
            // labelControl5
            // 
            this.labelControl5.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("labelControl5.ImageOptions.Image")));
            this.labelControl5.Location = new System.Drawing.Point(5, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(118, 36);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "TK Doanh Thu";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.simpleButton2);
            this.panelControl3.Controls.Add(this.simpleButton1);
            this.panelControl3.Location = new System.Drawing.Point(12, 238);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(750, 40);
            this.panelControl3.TabIndex = 2;
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton2.ImageOptions.SvgImage")));
            this.simpleButton2.Location = new System.Drawing.Point(551, 6);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(94, 29);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Lưu";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(651, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(94, 29);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Sửa";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // FormInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 290);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "FormInventory";
            this.Text = "FormInventory";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MOTA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMADVT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditMLHH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TENHH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MAHH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TKK.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TKGV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TKDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl colTENHH;
        private DevExpress.XtraEditors.LabelControl MALOAIHH;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit MAHH;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit MOTA;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditMADVT;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditMLHH;
        private DevExpress.XtraEditors.TextEdit TENHH;
        private DevExpress.XtraEditors.LookUpEdit TKDT;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LookUpEdit TKK;
        private DevExpress.XtraEditors.LookUpEdit TKGV;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}