﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using DAL.DBContext;
using BLL.Services;
using AccountingApplication.Settings;
using BLL.Models;
using AccountingApplication.Views.Voucher;

namespace AccountingApplication.Views.Object
{
    public partial class ListAccount : DevExpress.XtraEditors.XtraUserControl
    {
        public ListAccount()
        {
            InitializeComponent();
            Text = ViewForm.DMTK;

            List<MenuUser> menus = AccountService.Instance.GetListMenuByRole(UserManager.CurrentUser.MACHUCVU);

            MenuUser menu = menus.FirstOrDefault(x => x.TENNQ == ViewForm.DMTK);
            if (menu != null)
            {
                bbiNew.Enabled = menu.THEM == 1 ? true : false;
                bbiEdit.Enabled = menu.SUA == 1 ? true : false;
                bbiDelete.Enabled = menu.XOA == 1 ? true : false;
            }
            BindingList<TAIKHOAN> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            gridView.OptionsView.EnableAppearanceEvenRow = true;
            gridView.OptionsView.EnableAppearanceOddRow = true;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        public BindingList<TAIKHOAN> GetDataSource()
        {
            List<TAIKHOAN> TK = ObjectService.Instance.GetTAIKHOANs();
            BindingList<TAIKHOAN> result = new BindingList<TAIKHOAN>(TK);
            return result;
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (FormAccount dashboard = new FormAccount(0, ""))
            {
                dashboard.ShowDialog();
                this.Show();

                BindingList<TAIKHOAN> dataSource = GetDataSource();
                gridControl.DataSource = dataSource;
            }
        }

        private void bbiEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MATK"));

                using (FormAccount dashboard = new FormAccount(1, a))
                {
                    dashboard.ShowDialog();
                    this.Show();
                }
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Lỗi hệ thống", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void bbiDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MATK"));
                ObjectService.Instance.removeTK(a);
                BindingList<TAIKHOAN> dataSource = GetDataSource();
                gridControl.DataSource = dataSource;
                XtraMessageBox.Show("Xóa dữ liệu thành công");
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Không xóa được dữ liệu, dữ liệu đã được sử dụng", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MATK"));

                using (FormAccount dashboard = new FormAccount(2, a))
                {
                    dashboard.ShowDialog();
                    this.Show();
                }
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Lỗi hệ thống", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
