﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using DAL.DBContext;
using BLL.Services;
using AccountingApplication.Settings;
using BLL.Models;

namespace AccountingApplication.Views.Object
{
    public partial class ListTransType : DevExpress.XtraEditors.XtraUserControl
    {
        public ListTransType()
        {
            InitializeComponent();

            Text = ViewForm.DMLNV;
            List<MenuUser> menus = AccountService.Instance.GetListMenuByRole(UserManager.CurrentUser.MACHUCVU);

            MenuUser menu = menus.FirstOrDefault(x => x.TENNQ == ViewForm.DMLNV);

            gridView.OptionsView.EnableAppearanceEvenRow = true;
            gridView.OptionsView.EnableAppearanceOddRow = true;

            //BindingList<LOAIHOADON> dataSource = GetDataSource();
            //gridControl.DataSource = dataSource;
            //bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        public BindingList<LOAIHOADON> GetDataSource()
        {
            List<LOAIHOADON> LCT = ObjectService.Instance.GetLoaiCTs();
            BindingList<LOAIHOADON> result = new BindingList<LOAIHOADON>(LCT);
            return result;
        }
    }
}
