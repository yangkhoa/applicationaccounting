﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL.DBContext;
using BLL.Services;

namespace AccountingApplication.Views.Object
{
    public partial class FormPaymentMethod : DevExpress.XtraEditors.XtraForm
    {
        int statusForm = 0;
        public FormPaymentMethod(int status, string MAPT)
        {
            InitializeComponent();
            statusForm = status;
            if (status == 1 || status == 2)
            {
                if (status == 2)
                {
                    simpleButton1.Enabled = false;
                }
                PTTHANHTOAN PTDetail = ObjectService.Instance.GetCTPT(MAPT);
                textEditMAPT.Text = PTDetail.MAPTTT;
                numericUpDownSNN.Value = Convert.ToDecimal(PTDetail.SONGAYNO);
                textEditTPT.Text = PTDetail.TENPTTT;
                textEditMAPT.Enabled = false;
            }

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                PTTHANHTOAN PTTT = new PTTHANHTOAN()
                {
                    MAPTTT = textEditMAPT.Text,
                    SONGAYNO = Convert.ToInt32(numericUpDownSNN.Value),
                    TENPTTT = textEditTPT.Text
                };
                if (statusForm == 0)
                {

                    ObjectService.Instance.InsertPTTT(PTTT);
                }
                else
                {
                    ObjectService.Instance.UpdatePTTT(PTTT);

                }
                simpleButton1.Enabled = false;
                XtraMessageBox.Show("Lưu dữ liệu thành công");
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Không lưu được dữ liệu");
                throw;
            }
        }
    }
}