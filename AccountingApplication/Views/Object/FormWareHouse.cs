﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL.DBContext;
using BLL.Services;

namespace AccountingApplication.Views.Object
{
    public partial class FormWareHouse : DevExpress.XtraEditors.XtraForm
    {
        int statusForm = 0;
        public FormWareHouse(int status,string MAKHO)
        {
            InitializeComponent();
            statusForm = status;
            if (status == 1 || status == 2)
            {
                if (status == 2)
                {
                    simpleButton1.Enabled = false;
                }
                KHOHANG KHODetail = ObjectService.Instance.GetKHO(MAKHO);
                textEditMK.Text = KHODetail.MAKHO;
                textEditTK.Text = KHODetail.TENKHO;
                textEditMK.Enabled = false;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                KHOHANG KHO = new KHOHANG()
                {
                    MAKHO = textEditMK.Text,
                    TENKHO = textEditTK.Text
                };
                if (statusForm == 0)
                {

                    ObjectService.Instance.InsertKHO(KHO);
                }
                else
                {
                    ObjectService.Instance.UpdateKHO(KHO);

                }
                simpleButton1.Enabled = false;
                XtraMessageBox.Show("Lưu dữ liệu thành công");
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Không lưu được dữ liệu");
                throw;
            }
        }
    }
}