﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL.Services;
using DAL.DBContext;
using System.Dynamic;

namespace AccountingApplication.Views.Object
{
    public partial class FormEmployee : DevExpress.XtraEditors.XtraForm
    {
        int statusForm = 0;
        public FormEmployee(int status, string MANV)
        {
            InitializeComponent();
            GetDataSource();
            statusForm = status;
            if (status == 1 || status == 2)
            {
                if (status == 2)
                {
                    simpleButton1.Enabled = false;
                }
                NHANVIEN NVDetail = ObjectService.Instance.GetCTNV(MANV);
                textEditMANV.Text = NVDetail.MANV.ToString();
                textEditMANV.Enabled = false;
                textEditTNV.Text = NVDetail.TENNV.ToString();
                textEditCMND.Text = NVDetail.CMND.ToString();
                textEditQQ.Text = NVDetail.QUEQUAN.ToString();
                textEditSDT.Text = NVDetail.SDTNV.ToString();
                textEditEMail.Text = NVDetail.EMAILNV.ToString();
                dateEditNS.DateTime = Convert.ToDateTime(NVDetail.NGAYSINH);
                lookUpEditCV.Properties.NullText = NVDetail.MANV;
                lookUpEditPB.Properties.NullText = NVDetail.MAPB;
                lookUpEditGT.Properties.NullText = NVDetail.GIOITINH;
            }

        }

        public void GetDataSource()
        {
            lookUpEditCV.Properties.DataSource = ObjectService.Instance.GetCHUCVUs();
            lookUpEditCV.Properties.DisplayMember = "MACHUCVU";
            lookUpEditCV.Properties.ValueMember = "MACHUCVU";
            lookUpEditCV.Properties.NullText = "";

            lookUpEditPB.Properties.DataSource = ObjectService.Instance.Getphongbans();
            lookUpEditPB.Properties.DisplayMember = "MAPB";
            lookUpEditPB.Properties.ValueMember = "MAPB";
            lookUpEditPB.Properties.NullText = "";

            dateEditNS.DateTime = DateTime.Now;

            dynamic nam = new ExpandoObject();
            nam.MAGT = "Nam";

            dynamic nu = new ExpandoObject();
            nu.MAGT = "Nữ";

            dynamic all = new ExpandoObject();
            all.MAGT = "All";

            lookUpEditGT.Properties.DataSource = new List<dynamic>
            {
                nam, nu, all
            };
            lookUpEditGT.Properties.ValueMember = "MAGT";
            lookUpEditGT.Properties.DisplayMember = "MAGT";
            lookUpEditGT.Properties.NullText = "";
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                NHANVIEN NV = new NHANVIEN()
                {
                    MANV = textEditMANV.Text,
                    TENNV = textEditTNV.Text,
                    CMND = textEditCMND.Text,
                    SDTNV = textEditSDT.Text,
                    QUEQUAN = textEditQQ.Text,
                    EMAILNV = textEditEMail.Text,
                    NGAYSINH = dateEditNS.DateTime,
                    GIOITINH = lookUpEditGT.Text.ToString(),
                    MAPB = lookUpEditPB.Text.ToString(),
                    MACHUCVU  = lookUpEditCV.Text.ToString()
                };
                if (statusForm == 0)
                {

                    ObjectService.Instance.InsertNV(NV);
                }
                else
                {
                    ObjectService.Instance.UpdateNV(NV);

                }
                simpleButton1.Enabled = false;
                XtraMessageBox.Show("Lưu dữ liệu thành công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Không lưu được dữ liệu, bạn nhập thông tin sai", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
        }
        
    }
}