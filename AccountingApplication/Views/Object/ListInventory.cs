﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using DAL.DBContext;
using BLL.Services;
using DevExpress.XtraSplashScreen;
using AccountingApplication.Settings;
using BLL.Models;

namespace AccountingApplication.Views.Object
{
    public partial class ListInventory : DevExpress.XtraEditors.XtraUserControl
    {
        public ListInventory()
        {
            InitializeComponent();

            /// Thêm ở mỗi form?
            Text = ViewForm.DMHH;

            List<MenuUser> menus = AccountService.Instance.GetListMenuByRole(UserManager.CurrentUser.MACHUCVU);

            MenuUser menu = menus.FirstOrDefault(x => x.TENNQ == ViewForm.DMHH);

            if (menu != null)
            {
                bbiNew.Enabled = menu.THEM == 1 ? true : false;
                bbiEdit.Enabled = menu.SUA == 1 ? true : false;
                bbiDelete.Enabled = menu.XOA == 1 ? true : false;
            }

            BindingList<HANGHOA> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;

            gridView.OptionsView.EnableAppearanceEvenRow = true;
            gridView.OptionsView.EnableAppearanceOddRow = true;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        public BindingList<HANGHOA> GetDataSource()
        {
            List<HANGHOA> HH = ObjectService.Instance.GetHANGHOAs();
            BindingList<HANGHOA> result = new BindingList<HANGHOA>(HH);
            return result;
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                using (FormInventory dashboard = new FormInventory(0, ""))
                {
                    dashboard.ShowDialog();
                    this.Show();

                    BindingList<HANGHOA> dataSource = GetDataSource();
                    gridControl.DataSource = dataSource;
                }
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Lỗi hệ thống", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            BindingList<HANGHOA> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
        }

        private void bbiEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MAHH"));

                using (FormInventory dashboard = new FormInventory(1, a))
                {
                    SplashScreenManager.CloseForm(false);
                    dashboard.ShowDialog();
                    this.Show();
                }
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Lỗi hệ thống", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }
            
        }

        private void View_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MAHH"));

                using (FormInventory dashboard = new FormInventory(2, a))
                {
                    SplashScreenManager.CloseForm(false);
                    dashboard.ShowDialog();
                    this.Show();
                }
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Lỗi hệ thống", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           
        }

        private void bbiDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MAHH"));
                ObjectService.Instance.removeHH(a);
                BindingList<HANGHOA> dataSource = GetDataSource();
                gridControl.DataSource = dataSource;
                XtraMessageBox.Show("Xóa dữ liệu thành công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Mặt hàng đã được sử dụng. Bạn không thể xóa", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }

        }
    }
}
