﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using DAL.DBContext;
using BLL.Services;
using BLL.Models;
using AccountingApplication.Settings;

namespace AccountingApplication.Views.Object
{
    public partial class ListPaymentMethod : DevExpress.XtraEditors.XtraUserControl
    {
        public ListPaymentMethod()
        {
            InitializeComponent();

            Text = ViewForm.DMPTTT;
            List<MenuUser> menus = AccountService.Instance.GetListMenuByRole(UserManager.CurrentUser.MACHUCVU);

            MenuUser menu = menus.FirstOrDefault(x => x.TENNQ == ViewForm.DMPTTT);

            if (menu != null)
            {
                bbiNew.Enabled = menu.THEM == 1 ? true : false;
                bbiEdit.Enabled = menu.SUA == 1 ? true : false;
                bbiDelete.Enabled = menu.XOA == 1 ? true : false;
            }

            BindingList<PTTHANHTOAN> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        public BindingList<PTTHANHTOAN> GetDataSource()
        {
            List<PTTHANHTOAN> PTTTs = ObjectService.Instance.GetPTTTs();
            BindingList<PTTHANHTOAN> result = new BindingList<PTTHANHTOAN>(PTTTs);
            return result;
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (FormPaymentMethod dashboard = new FormPaymentMethod(0, ""))
            {
                dashboard.ShowDialog();
                this.Show();

                BindingList<PTTHANHTOAN> dataSource = GetDataSource();
                gridControl.DataSource = dataSource;
            }
        }

        private void bbiEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MAPTTT"));

            using (FormPaymentMethod dashboard = new FormPaymentMethod(1, a))
            {
                dashboard.ShowDialog();
                this.Show();
            }
        }

        private void bbiDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MAPTTT"));
                ObjectService.Instance.removePTTT(a);
                BindingList<PTTHANHTOAN> dataSource = GetDataSource();
                gridControl.DataSource = dataSource;
                XtraMessageBox.Show("Xóa dữ liệu thành công");
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Xóa dữ liệu không thành công");
            }
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MAPTTT"));

            using (FormPaymentMethod dashboard = new FormPaymentMethod(2, a))
            {
                dashboard.ShowDialog();
                this.Show();
            }
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {

            BindingList<PTTHANHTOAN> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
        }
    }
}
