﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using DAL.DBContext;
using BLL.Services;
using AccountingApplication.Settings;
using BLL.Models;

namespace AccountingApplication.Views.Object
{
    public partial class ListPriceList : DevExpress.XtraEditors.XtraUserControl
    {
        public ListPriceList()
        {
            InitializeComponent();

            Text = ViewForm.DMBG;
            List<MenuUser> menus = AccountService.Instance.GetListMenuByRole(UserManager.CurrentUser.MACHUCVU);

            MenuUser menu = menus.FirstOrDefault(x => x.TENNQ == ViewForm.DMBG);

            if (menu != null)
            {
                bbiNew.Enabled = menu.THEM == 1 ? true : false;
                bbiEdit.Enabled = menu.SUA == 1 ? true : false;
                bbiDelete.Enabled = menu.XOA == 1 ? true : false;
            }

            //BindingList<BANGGIA> dataSource = GetDataSource();
            //gridControl.DataSource = dataSource;
            //bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }

        //public BindingList<BANGGIA> GetDataSource()
        //{
        //    List<BANGGIA> BG = ObjectService.Instance.GetBANGGIAs();
        //    BindingList<BANGGIA> result = new BindingList<BANGGIA>(BG);
        //    return result;
        //}
    }
}
