﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using DAL.DBContext;
using BLL.Services;
using DevExpress.XtraSplashScreen;
using AccountingApplication.Settings;
using BLL.Models;

namespace AccountingApplication.Views.Object
{
    public partial class ListCustomer : DevExpress.XtraEditors.XtraUserControl
    {
        public ListCustomer()
        {
            InitializeComponent();

            Text = ViewForm.DMKH;

            List<MenuUser> menus = AccountService.Instance.GetListMenuByRole(UserManager.CurrentUser.MACHUCVU);

            MenuUser menu = menus.FirstOrDefault(x => x.TENNQ == ViewForm.DMKH);

            if (menu != null)
            {
                bbiNew.Enabled = menu.THEM == 1 ? true : false;
                bbiEdit.Enabled = menu.SUA == 1 ? true : false;
                bbiDelete.Enabled = menu.XOA == 1 ? true : false;
            }

            BindingList<KHACHHANG> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
            bsiRecordsCount.Caption = "RECORDS : " + dataSource.Count;
        }
        void bbiPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl.ShowRibbonPrintPreview();
        }
        public BindingList<KHACHHANG> GetDataSource()
        {
            List<KHACHHANG> KH = ObjectService.Instance.GetKHACHHANGs();
            BindingList<KHACHHANG> result = new BindingList<KHACHHANG>(KH);
            return result;
        }

        private void bbiNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (FormCustomer dashboard = new FormCustomer(0, ""))
            {
                dashboard.ShowDialog();
                this.Show();

                BindingList<KHACHHANG> dataSource = GetDataSource();
                gridControl.DataSource = dataSource;
            }
        }

        private void bbiEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MAKH"));

                using (FormCustomer dashboard = new FormCustomer(1, a))
                {
                    SplashScreenManager.CloseForm(false);
                    dashboard.ShowDialog();
                    this.Show();
                }
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Lỗi hệ thống", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MAKH"));

                using (FormCustomer dashboard = new FormCustomer(2, a))
                {
                    SplashScreenManager.CloseForm(false);
                    dashboard.ShowDialog();
                    this.Show();
                }
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Lỗi hệ thống", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            BindingList<KHACHHANG> dataSource = GetDataSource();
            gridControl.DataSource = dataSource;
        }

        private void bbiDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string a = Convert.ToString(gridView.GetRowCellValue(gridView.FocusedRowHandle, "MAKH"));
                ObjectService.Instance.removeKH(a);
                BindingList<KHACHHANG> dataSource = GetDataSource();
                gridControl.DataSource = dataSource;
                XtraMessageBox.Show("Xóa dữ liệu thành công");
            }
            catch (Exception)
            {
                XtraMessageBox.Show("Không xóa được dữ liệu, dữ liệu đã được sử dụng", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }


        }
    }
}
