﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars.Docking2010.Views;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Navigation;
using System.IO;
using AccountingApplication.Views.Report;
using AccountingApplication.Views.Voucher;
using AccountingApplication.Settings;
using BLL.Services;
using BLL.Models;
using AccountingApplication.Views.Object;
using AccountingApplication.Views.Account;

namespace AccountingApplication.Views
{
    public partial class fDashboard : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        AccountPermissionUC accountPermissionUC;
        ReportRevenueUC reportRevenueUC;
        ListSalesVoucherForm listSalesVoucherForm;
        ListInventory listInventory;
        ListPromotion promotionVoucher;
        SalesHistory salesHistory;
        ListSalesInvoice listSalesInvoice;
        ListExport ListExport;
        ListReceipt listReceipt;
        Phieudinhkhoan phieudinhkhoan;
        ListReturn ListReturn;
        baocaolaigop baocaolaigop;
        ListCustomer ListCustomer;
        ListEmployee ListEmployee;
        ListPaymentMethod listPaymentMethod;
        ListWareHouse listWareHouse;
        ListTransType ListTransType;
        ListAccount listAccount;
        Tonghopnhatxuatton THNXT;
        TienmatTiengui TMTG;
        Congnophaithu CNPT;
        SOCAI sOCAI;
        public fDashboard()
        {
            InitializeComponent();

            /// Khai báo UC - Form khi load dashboard
            accordionControl.SelectedElement = null;

            /// Thu nhỏ ribbon control khi load
            //ribbonControl.AllowMinimizeRibbon = true;
            //ribbonControl.Minimized = true;

            SetupPermission();

            List<MenuUser> listMenuByUsername = AccountService.Instance.GetListMenuByRole(UserManager.CurrentUser.MACHUCVU);

            foreach (MenuUser menu in listMenuByUsername)
            {
                if (ViewForm.isHasPermissionInNghiepVu(menu.TENNQ))
                {
                    this.NghiepVu.Visible = true;
                    accordionControl.GetElements().Where(x => x.Name == menu.MANQ).FirstOrDefault().Visible = true;
                    this.accordionControlSeparator1.Visible = true;
                }

                if (ViewForm.isHasPermissionInDanhMuc(menu.TENNQ))
                {
                    this.DanhMuc.Visible = true;
                    accordionControl.GetElements().Where(x => x.Name == menu.MANQ).FirstOrDefault().Visible = true;
                    this.accordionControlSeparator6.Visible = true;
                }

                if (ViewForm.isHasPermissionInBaoCao(menu.TENNQ))
                {
                    this.BaoCao.Visible = true;
                    accordionControl.GetElements().Where(x => x.Name == menu.MANQ).FirstOrDefault().Visible = true;
                    this.accordionControlSeparator5.Visible = true;
                }

                if (ViewForm.isHasPermissionInPhanQuyen(menu.TENNQ))
                {
                    this.PhanQuyen.Visible = true;
                    accordionControl.GetElements().Where(x => x.Name == menu.MANQ).FirstOrDefault().Visible = true;
                    this.accordionControlSeparator3.Visible = true;
                }

                if (ViewForm.isHasPermissionInTruyVan(menu.TENNQ))
                {
                    this.TruyVan.Visible = true;
                    accordionControl.GetElements().Where(x => x.Name == menu.MANQ).FirstOrDefault().Visible = true;
                    this.accordionControlSeparator2.Visible = true;
                }
            }
        }

        public void accordionControl_SelectedElementChanged(object sender, SelectedElementChangedEventArgs e)
        {
            if (e.Element == null) return;
            Control userControl = null;

            string view = e.Element.Text;

            switch (view)
            {
                case ViewForm.DHB:
                    userControl = new ListSalesVoucherForm();
                    break;                 
                case ViewForm.BCDS:
                    userControl = new ReportRevenueUC();
                    break;
                case ViewForm.PQ:
                    userControl = new AccountPermissionUC();
                    break;
                case ViewForm.DMHH:
                    userControl = new ListInventory();
                    break;
                case ViewForm.GG:
                    userControl = new ListPromotion();
                    break;
                case ViewForm.NKBH:
                    userControl = new SalesHistory();
                    break;
                case ViewForm.HDB:
                    userControl = new ListSalesInvoice();
                    break;
                case ViewForm.XKNK:
                    userControl = new ListExport();
                    break;
                case ViewForm.DHM:
                    userControl = new ListReceipt();
                    break;
                case ViewForm.PDK:
                    userControl = new Phieudinhkhoan();
                    break;
                case ViewForm.NKTL:
                    userControl = new ListReturn();
                    break;
                case ViewForm.BCLG:
                    userControl = new baocaolaigop();
                    break;
                case ViewForm.DMKH:
                    userControl = new ListCustomer();
                    break;
                case ViewForm.DMNV:
                    userControl = new ListEmployee();
                    break;
                case ViewForm.DMPTTT:
                    userControl = new ListPaymentMethod();
                    break;
                case ViewForm.DMK:
                    userControl = new ListWareHouse();
                    break;
                case ViewForm.DMLNV:
                    userControl = new ListTransType();
                    break;
                case ViewForm.DMTK:
                    userControl = new ListAccount();
                    break;
                case ViewForm.THXNT:
                    userControl = new Tonghopnhatxuatton();
                    break;
                case ViewForm.SQTMTGNH:
                    userControl = new TienmatTiengui();
                    break;
                case ViewForm.BCCN:
                    userControl = new Congnophaithu();
                    break;
                case ViewForm.SC:
                    userControl = new SOCAI();
                    break;
            }

            tabbedView.AddDocument(userControl);
            tabbedView.ActivateDocument(userControl);
        }

        public void tabbedView_DocumentClosed(object sender, DocumentEventArgs e)
        {
            //RecreateUserControls(e);
            accordionControl.SelectedElement = null;
        }

        /// <summary>
        /// Load lại User Control
        /// </summary>
        /// <param name="e"></param>
        public void RecreateUserControls(DocumentEventArgs e)
        {
            string view = e.Document.Caption;

            switch (view)
            {
                case ViewForm.DHB:
                    listSalesVoucherForm = new ListSalesVoucherForm();
                    break;
                case ViewForm.DHM:
                    listReceipt = new ListReceipt();
                    break;
                case ViewForm.BCDS:
                    reportRevenueUC = new ReportRevenueUC();
                    break;
                case ViewForm.PQ:
                    accountPermissionUC = new AccountPermissionUC();
                    break;
                case ViewForm.DMHH:
                    listInventory = new ListInventory();
                    break;
                case ViewForm.GG:
                    promotionVoucher = new ListPromotion();
                    break;
                case ViewForm.NKBH:
                    salesHistory = new SalesHistory();
                    break;
                case ViewForm.HDB:
                    listSalesInvoice = new ListSalesInvoice();
                    break;
                case ViewForm.XKNK:
                    ListExport = new ListExport();
                    break;
                case ViewForm.PDK:
                    phieudinhkhoan = new Phieudinhkhoan();
                    break;
                case ViewForm.NKTL:
                    ListReturn = new ListReturn();
                    break;
                case ViewForm.BCLG:
                    baocaolaigop = new baocaolaigop();
                    break;
                case ViewForm.DMKH:
                    ListCustomer = new ListCustomer();
                    break;
                case ViewForm.DMNV:
                    ListEmployee = new ListEmployee();
                    break;
                case ViewForm.DMPTTT:
                    listPaymentMethod = new ListPaymentMethod();
                    break;
                case ViewForm.DMK:
                    listWareHouse = new ListWareHouse();
                    break;
                case ViewForm.DMLNV:
                    ListTransType = new ListTransType();
                    break;
                case ViewForm.DMTK:
                    listAccount = new ListAccount();
                    break;
                case ViewForm.THXNT:
                    THNXT = new Tonghopnhatxuatton();
                    break;
                case ViewForm.SQTMTGNH:
                    TMTG = new TienmatTiengui();
                    break;
                case ViewForm.BCCN:
                    CNPT = new Congnophaithu();
                    break;
                case ViewForm.SC:
                    sOCAI = new SOCAI();
                    break;
            }          
        }

        /// <summary>
        /// Đăng xuất
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barButtonLogout_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }


        /// <summary>
        /// Upload hình
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barButtonUpload_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.CheckFileExists = true;
            openFileDialog.AddExtension = false;
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "Image Files(*.jpeg;*.bmp;*.png;*.jpg)|*.jpeg;*.bmp;*.png;*.jpg";

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var data = System.IO.File.ReadAllBytes(openFileDialog.FileName);

                File.WriteAllBytes(AppDomain.CurrentDomain.BaseDirectory+ System.IO.Path.GetFileName(openFileDialog.FileName),data);

                this.BackgroundImageStore = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + System.IO.Path.GetFileName(openFileDialog.FileName));
            }
            openFileDialog.Dispose();
        }   

        public void SetupPermission()
        {
            this.NghiepVu.Visible = false;
            this.DHB.Visible = false;
            this.DHM.Visible = false;
            this.XKNK.Visible = false;
            this.HDB.Visible = false;
            this.HDM.Visible = false;
            this.NKTL.Visible = false;
            this.accordionControlSeparator1.Visible = false;

            this.TruyVan.Visible = false;
            this.DSDHB.Visible = false;
            this.DSDHM.Visible = false;
            this.DSHDB.Visible = false;
            this.DSHDM.Visible = false;
            this.DSPNX.Visible = false;
            this.accordionControlSeparator4.Visible = false;

            this.DanhMuc.Visible = false;
            this.DMBG.Visible = false;
            this.DMHH.Visible = false;
            this.DMK.Visible = false;
            this.DMKH.Visible = false;
            this.DMLNV.Visible = false;
            this.DMNV.Visible = false;
            this.DMPTTT.Visible = false;
            this.DMTK.Visible = false;
            this.accordionControlSeparator6.Visible = false;

            this.PQ.Visible = false;
            this.PhanQuyen.Visible = false;
            this.accordionControlSeparator3.Visible = false;


            this.BaoCao.Visible = false;
            this.NKBH.Visible = false;
            this.BCCN.Visible = false;
            this.DTBH.Visible = false;
            this.BKPBHPX.Visible = false;
            this.BCDS.Visible = false;
            this.BCLG.Visible = false;
            this.BCTN.Visible = false;
            this.SQTMTGNH.Visible = false;
            this.THXNT.Visible = false;
            this.PDK.Visible = false;
            this.SC.Visible = false;
            this.accordionControlSeparator5.Visible = false;

            this.btnCreateUser.Enabled = UserManager.CurrentUser.MACHUCVU == (int)ChucVu.GiamDoc;
        }

        private void btnChangePass_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormChangePass fChangePass = new FormChangePass();

            fChangePass.ShowDialog();

            if (Common.ChangePassSuccess == true)
            {
                this.Close();
            }
        }

        private void btnCreateUser_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormCreateUser form = new FormCreateUser();

            form.ShowDialog();
        }
    }
}