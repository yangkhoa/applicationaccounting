﻿namespace AccountingApplication.Views
{
    partial class fDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fDashboard));
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.skinRibbonGalleryBarItem = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.employeesBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.customersBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonLogout = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonUpload = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLogout = new DevExpress.XtraBars.BarButtonItem();
            this.btnChangePass = new DevExpress.XtraBars.BarButtonItem();
            this.btnCreateUser = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupNavigation = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.accordionControl = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.mainAccordionGroup = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.NghiepVu = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DHB = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.HDB = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DHM = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.XKNK = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.HDM = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.NKTL = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.GG = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlSeparator1 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
            this.TruyVan = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DSDHM = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DSDHB = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DSHDM = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DSHDB = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DSPNX = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DSHDGG = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlSeparator4 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
            this.DanhMuc = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DMLNV = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DMHH = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DMK = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DMPTTT = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DMNV = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DMBG = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DMTK = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DMKH = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlSeparator6 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
            this.accordionControlSeparator3 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
            this.BaoCao = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.NKBH = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.BCCN = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DTBH = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.BKPBHPX = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.BCDS = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.BCLG = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.BCTN = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.SQTMTGNH = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.THXNT = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.PDK = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.SC = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlSeparator5 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
            this.PhanQuyen = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.PQ = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.tabbedView = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.documentManager = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.accordionControlSeparator2 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
            this.accordionControlElement1 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            this.dockPanel.SuspendLayout();
            this.dockPanel_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl
            // 
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.ribbonControl.SearchEditItem,
            this.skinRibbonGalleryBarItem,
            this.employeesBarButtonItem,
            this.customersBarButtonItem,
            this.barButtonLogout,
            this.barButtonUpload,
            this.barButtonItemLogout,
            this.btnChangePass,
            this.btnCreateUser});
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbonControl.MaxItemId = 53;
            this.ribbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage});
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl.Size = new System.Drawing.Size(1147, 178);
            this.ribbonControl.StatusBar = this.ribbonStatusBar;
            this.ribbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // skinRibbonGalleryBarItem
            // 
            this.skinRibbonGalleryBarItem.Id = 14;
            this.skinRibbonGalleryBarItem.Name = "skinRibbonGalleryBarItem";
            // 
            // employeesBarButtonItem
            // 
            this.employeesBarButtonItem.Caption = "Employees";
            this.employeesBarButtonItem.Id = 46;
            this.employeesBarButtonItem.Name = "employeesBarButtonItem";
            // 
            // customersBarButtonItem
            // 
            this.customersBarButtonItem.Caption = "Cutomers";
            this.customersBarButtonItem.Id = 47;
            this.customersBarButtonItem.Name = "customersBarButtonItem";
            // 
            // barButtonLogout
            // 
            this.barButtonLogout.Caption = "Đăng xuất";
            this.barButtonLogout.Id = 48;
            this.barButtonLogout.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonLogout.ImageOptions.Image")));
            this.barButtonLogout.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonLogout.ImageOptions.LargeImage")));
            this.barButtonLogout.Name = "barButtonLogout";
            this.barButtonLogout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonLogout_ItemClick);
            // 
            // barButtonUpload
            // 
            this.barButtonUpload.Caption = "Hình nền";
            this.barButtonUpload.Id = 49;
            this.barButtonUpload.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonUpload.ImageOptions.Image")));
            this.barButtonUpload.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonUpload.ImageOptions.LargeImage")));
            this.barButtonUpload.Name = "barButtonUpload";
            this.barButtonUpload.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonUpload_ItemClick);
            // 
            // barButtonItemLogout
            // 
            this.barButtonItemLogout.Caption = "Đăng xuất";
            this.barButtonItemLogout.Id = 50;
            this.barButtonItemLogout.Name = "barButtonItemLogout";
            this.barButtonItemLogout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonLogout_ItemClick);
            // 
            // btnChangePass
            // 
            this.btnChangePass.Caption = "Đổi mật khẩu";
            this.btnChangePass.Id = 51;
            this.btnChangePass.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnChangePass.ImageOptions.Image")));
            this.btnChangePass.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnChangePass.ImageOptions.LargeImage")));
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnChangePass_ItemClick);
            // 
            // btnCreateUser
            // 
            this.btnCreateUser.Caption = "Tạo tài khoản";
            this.btnCreateUser.Id = 52;
            this.btnCreateUser.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCreateUser.ImageOptions.Image")));
            this.btnCreateUser.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCreateUser.ImageOptions.LargeImage")));
            this.btnCreateUser.Name = "btnCreateUser";
            this.btnCreateUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCreateUser_ItemClick);
            // 
            // ribbonPage
            // 
            this.ribbonPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup,
            this.ribbonPageGroup1,
            this.ribbonPageGroupNavigation});
            this.ribbonPage.Name = "ribbonPage";
            this.ribbonPage.Text = "Home";
            // 
            // ribbonPageGroup
            // 
            this.ribbonPageGroup.AllowTextClipping = false;
            this.ribbonPageGroup.ItemLinks.Add(this.skinRibbonGalleryBarItem);
            this.ribbonPageGroup.Name = "ribbonPageGroup";
            this.ribbonPageGroup.ShowCaptionButton = false;
            this.ribbonPageGroup.Text = "Giao diện";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnChangePass);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnCreateUser);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Tài khoản";
            // 
            // ribbonPageGroupNavigation
            // 
            this.ribbonPageGroupNavigation.ItemLinks.Add(this.barButtonLogout);
            this.ribbonPageGroupNavigation.ItemLinks.Add(this.barButtonUpload);
            this.ribbonPageGroupNavigation.Name = "ribbonPageGroupNavigation";
            this.ribbonPageGroupNavigation.Text = "Hệ thống";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.barButtonItemLogout);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 690);
            this.ribbonStatusBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbonControl;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1147, 37);
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel});
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dockPanel
            // 
            this.dockPanel.AutoScroll = true;
            this.dockPanel.Controls.Add(this.dockPanel_Container);
            this.dockPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel.ID = new System.Guid("a045df26-1503-4d9a-99c1-a531310af22b");
            this.dockPanel.Location = new System.Drawing.Point(0, 178);
            this.dockPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dockPanel.Name = "dockPanel";
            this.dockPanel.Options.ShowCloseButton = false;
            this.dockPanel.OriginalSize = new System.Drawing.Size(244, 200);
            this.dockPanel.Size = new System.Drawing.Size(244, 512);
            this.dockPanel.Text = "Menu";
            // 
            // dockPanel_Container
            // 
            this.dockPanel_Container.Controls.Add(this.accordionControl);
            this.dockPanel_Container.Location = new System.Drawing.Point(5, 25);
            this.dockPanel_Container.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dockPanel_Container.Name = "dockPanel_Container";
            this.dockPanel_Container.Size = new System.Drawing.Size(232, 482);
            this.dockPanel_Container.TabIndex = 0;
            // 
            // accordionControl
            // 
            this.accordionControl.AllowItemSelection = true;
            this.accordionControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accordionControl.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.mainAccordionGroup});
            this.accordionControl.Location = new System.Drawing.Point(0, 0);
            this.accordionControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.accordionControl.Name = "accordionControl";
            this.accordionControl.Size = new System.Drawing.Size(232, 482);
            this.accordionControl.TabIndex = 0;
            this.accordionControl.Text = "accordionControl";
            this.accordionControl.SelectedElementChanged += new DevExpress.XtraBars.Navigation.SelectedElementChangedEventHandler(this.accordionControl_SelectedElementChanged);
            // 
            // mainAccordionGroup
            // 
            this.mainAccordionGroup.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.NghiepVu,
            this.accordionControlSeparator1,
            this.TruyVan,
            this.accordionControlSeparator4,
            this.DanhMuc,
            this.accordionControlSeparator6,
            this.accordionControlSeparator3,
            this.BaoCao,
            this.accordionControlSeparator5,
            this.PhanQuyen});
            this.mainAccordionGroup.Expanded = true;
            this.mainAccordionGroup.HeaderVisible = false;
            this.mainAccordionGroup.Name = "mainAccordionGroup";
            this.mainAccordionGroup.Text = "mainGroup";
            // 
            // NghiepVu
            // 
            this.NghiepVu.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.DHB,
            this.HDB,
            this.DHM,
            this.XKNK,
            this.HDM,
            this.NKTL,
            this.GG});
            this.NghiepVu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("NghiepVu.ImageOptions.Image")));
            this.NghiepVu.Name = "NghiepVu";
            this.NghiepVu.Text = "Nghiệp vụ";
            // 
            // DHB
            // 
            this.DHB.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DHB.ImageOptions.Image")));
            this.DHB.Name = "DHB";
            this.DHB.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DHB.Text = "Đơn hàng bán";
            // 
            // HDB
            // 
            this.HDB.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("HDB.ImageOptions.Image")));
            this.HDB.Name = "HDB";
            this.HDB.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.HDB.Text = "Hóa đơn bán";
            // 
            // DHM
            // 
            this.DHM.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DHM.ImageOptions.Image")));
            this.DHM.Name = "DHM";
            this.DHM.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DHM.Text = "Phiếu thu";
            // 
            // XKNK
            // 
            this.XKNK.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("XKNK.ImageOptions.Image")));
            this.XKNK.Name = "XKNK";
            this.XKNK.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.XKNK.Text = "Xuất kho - nhập kho";
            // 
            // HDM
            // 
            this.HDM.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("HDM.ImageOptions.Image")));
            this.HDM.Name = "HDM";
            this.HDM.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.HDM.Text = "Hóa đơn mua";
            // 
            // NKTL
            // 
            this.NKTL.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("NKTL.ImageOptions.Image")));
            this.NKTL.Name = "NKTL";
            this.NKTL.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.NKTL.Text = "Nhập kho trả lại";
            // 
            // GG
            // 
            this.GG.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("GG.ImageOptions.Image")));
            this.GG.Name = "GG";
            this.GG.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.GG.Text = "Giảm giá";
            // 
            // accordionControlSeparator1
            // 
            this.accordionControlSeparator1.Name = "accordionControlSeparator1";
            // 
            // TruyVan
            // 
            this.TruyVan.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.DSDHM,
            this.DSDHB,
            this.DSHDM,
            this.DSHDB,
            this.DSPNX,
            this.DSHDGG});
            this.TruyVan.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("TruyVan.ImageOptions.Image")));
            this.TruyVan.Name = "TruyVan";
            this.TruyVan.Text = "Truy vấn";
            // 
            // DSDHM
            // 
            this.DSDHM.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DSDHM.ImageOptions.Image")));
            this.DSDHM.Name = "DSDHM";
            this.DSDHM.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DSDHM.Text = "Danh sách đơn hàng mua";
            // 
            // DSDHB
            // 
            this.DSDHB.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DSDHB.ImageOptions.Image")));
            this.DSDHB.Name = "DSDHB";
            this.DSDHB.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DSDHB.Text = "Danh sách đơn hàng bán";
            // 
            // DSHDM
            // 
            this.DSHDM.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DSHDM.ImageOptions.Image")));
            this.DSHDM.Name = "DSHDM";
            this.DSHDM.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DSHDM.Text = "Danh sách hóa đơn mua";
            // 
            // DSHDB
            // 
            this.DSHDB.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DSHDB.ImageOptions.Image")));
            this.DSHDB.Name = "DSHDB";
            this.DSHDB.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DSHDB.Text = "Danh sách hóa đơn bán";
            // 
            // DSPNX
            // 
            this.DSPNX.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DSPNX.ImageOptions.Image")));
            this.DSPNX.Name = "DSPNX";
            this.DSPNX.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DSPNX.Text = "Danh sách phiếu nhập - xuất";
            // 
            // DSHDGG
            // 
            this.DSHDGG.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DSHDGG.ImageOptions.Image")));
            this.DSHDGG.Name = "DSHDGG";
            this.DSHDGG.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DSHDGG.Text = "Danh sách hóa đơn giảm giá";
            // 
            // accordionControlSeparator4
            // 
            this.accordionControlSeparator4.Name = "accordionControlSeparator4";
            // 
            // DanhMuc
            // 
            this.DanhMuc.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.DMLNV,
            this.DMHH,
            this.DMK,
            this.DMPTTT,
            this.DMNV,
            this.DMBG,
            this.DMTK,
            this.DMKH});
            this.DanhMuc.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DanhMuc.ImageOptions.Image")));
            this.DanhMuc.Name = "DanhMuc";
            this.DanhMuc.Text = "Danh mục";
            // 
            // DMLNV
            // 
            this.DMLNV.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DMLNV.ImageOptions.Image")));
            this.DMLNV.Name = "DMLNV";
            this.DMLNV.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DMLNV.Text = "Danh mục loại nghiệp vụ";
            // 
            // DMHH
            // 
            this.DMHH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DMHH.ImageOptions.Image")));
            this.DMHH.Name = "DMHH";
            this.DMHH.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DMHH.Text = "Danh mục hàng hóa";
            // 
            // DMK
            // 
            this.DMK.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DMK.ImageOptions.Image")));
            this.DMK.Name = "DMK";
            this.DMK.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DMK.Text = "Danh mục kho";
            // 
            // DMPTTT
            // 
            this.DMPTTT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DMPTTT.ImageOptions.Image")));
            this.DMPTTT.Name = "DMPTTT";
            this.DMPTTT.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DMPTTT.Text = "Danh mục phương thức thanh toán";
            // 
            // DMNV
            // 
            this.DMNV.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DMNV.ImageOptions.Image")));
            this.DMNV.Name = "DMNV";
            this.DMNV.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DMNV.Text = "Danh mục nhân viên";
            // 
            // DMBG
            // 
            this.DMBG.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DMBG.ImageOptions.Image")));
            this.DMBG.Name = "DMBG";
            this.DMBG.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DMBG.Text = "Danh mục bảng giá";
            // 
            // DMTK
            // 
            this.DMTK.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DMTK.ImageOptions.Image")));
            this.DMTK.Name = "DMTK";
            this.DMTK.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DMTK.Text = "Danh mục tài khoản";
            // 
            // DMKH
            // 
            this.DMKH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DMKH.ImageOptions.Image")));
            this.DMKH.Name = "DMKH";
            this.DMKH.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DMKH.Text = "Danh mục khách hàng";
            // 
            // accordionControlSeparator6
            // 
            this.accordionControlSeparator6.Name = "accordionControlSeparator6";
            // 
            // accordionControlSeparator3
            // 
            this.accordionControlSeparator3.Name = "accordionControlSeparator3";
            // 
            // BaoCao
            // 
            this.BaoCao.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.NKBH,
            this.BCCN,
            this.DTBH,
            this.BKPBHPX,
            this.BCDS,
            this.BCLG,
            this.BCTN,
            this.SQTMTGNH,
            this.THXNT,
            this.PDK,
            this.SC});
            this.BaoCao.Expanded = true;
            this.BaoCao.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BaoCao.ImageOptions.Image")));
            this.BaoCao.Name = "BaoCao";
            this.BaoCao.Text = "Báo cáo";
            // 
            // NKBH
            // 
            this.NKBH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("NKBH.ImageOptions.Image")));
            this.NKBH.Name = "NKBH";
            this.NKBH.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.NKBH.Text = "Nhật ký bán hàng";
            // 
            // BCCN
            // 
            this.BCCN.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BCCN.ImageOptions.Image")));
            this.BCCN.Name = "BCCN";
            this.BCCN.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.BCCN.Text = "Báo cáo công nợ";
            // 
            // DTBH
            // 
            this.DTBH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DTBH.ImageOptions.Image")));
            this.DTBH.Name = "DTBH";
            this.DTBH.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DTBH.Text = "Doanh thu bán hàng";
            // 
            // BKPBHPX
            // 
            this.BKPBHPX.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BKPBHPX.ImageOptions.Image")));
            this.BKPBHPX.Name = "BKPBHPX";
            this.BKPBHPX.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.BKPBHPX.Text = "Bảng kê phiếu bán hàng - phiếu xuất";
            // 
            // BCDS
            // 
            this.BCDS.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BCDS.ImageOptions.Image")));
            this.BCDS.Name = "BCDS";
            this.BCDS.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.BCDS.Text = "Báo cáo doanh số";
            // 
            // BCLG
            // 
            this.BCLG.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BCLG.ImageOptions.Image")));
            this.BCLG.Name = "BCLG";
            this.BCLG.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.BCLG.Text = "Báo cáo lãi gộp";
            // 
            // BCTN
            // 
            this.BCTN.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BCTN.ImageOptions.Image")));
            this.BCTN.Name = "BCTN";
            this.BCTN.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.BCTN.Text = "Báo cáo tuổi nợ";
            // 
            // SQTMTGNH
            // 
            this.SQTMTGNH.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("SQTMTGNH.ImageOptions.Image")));
            this.SQTMTGNH.Name = "SQTMTGNH";
            this.SQTMTGNH.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.SQTMTGNH.Text = "Sổ quỹ tiền mặt - tiền gửi ngân hàng";
            // 
            // THXNT
            // 
            this.THXNT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("THXNT.ImageOptions.Image")));
            this.THXNT.Name = "THXNT";
            this.THXNT.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.THXNT.Text = "Tổng hợp xuất nhập tồn";
            // 
            // PDK
            // 
            this.PDK.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("PDK.ImageOptions.Image")));
            this.PDK.Name = "PDK";
            this.PDK.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.PDK.Text = "Phiếu định khoản";
            // 
            // SC
            // 
            this.SC.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("SC.ImageOptions.Image")));
            this.SC.Name = "SC";
            this.SC.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.SC.Text = "Sổ cái";
            // 
            // accordionControlSeparator5
            // 
            this.accordionControlSeparator5.Name = "accordionControlSeparator5";
            // 
            // PhanQuyen
            // 
            this.PhanQuyen.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.PQ});
            this.PhanQuyen.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("PhanQuyen.ImageOptions.Image")));
            this.PhanQuyen.Name = "PhanQuyen";
            this.PhanQuyen.Text = "Phân quyền";
            // 
            // PQ
            // 
            this.PQ.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("PQ.ImageOptions.Image")));
            this.PQ.Name = "PQ";
            this.PQ.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.PQ.Text = "Phân quyền tính năng";
            // 
            // tabbedView
            // 
            this.tabbedView.DocumentClosed += new DevExpress.XtraBars.Docking2010.Views.DocumentEventHandler(this.tabbedView_DocumentClosed);
            // 
            // documentManager
            // 
            this.documentManager.MdiParent = this;
            this.documentManager.MenuManager = this.ribbonControl;
            this.documentManager.View = this.tabbedView;
            this.documentManager.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView});
            // 
            // accordionControlSeparator2
            // 
            this.accordionControlSeparator2.Name = "accordionControlSeparator2";
            // 
            // accordionControlElement1
            // 
            this.accordionControlElement1.Name = "accordionControlElement1";
            this.accordionControlElement1.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.accordionControlElement1.Text = "Phân quyền";
            // 
            // fDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Tile;
            this.BackgroundImageStore = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImageStore")));
            this.ClientSize = new System.Drawing.Size(1147, 727);
            this.Controls.Add(this.dockPanel);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbonControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "fDashboard";
            this.Ribbon = this.ribbonControl;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            this.dockPanel.ResumeLayout(false);
            this.dockPanel_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem;
        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel_Container;
        private DevExpress.XtraBars.Navigation.AccordionControl accordionControl;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupNavigation;
        private DevExpress.XtraBars.BarButtonItem employeesBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem customersBarButtonItem;
        private DevExpress.XtraBars.Navigation.AccordionControlElement NghiepVu;
        private DevExpress.XtraBars.Navigation.AccordionControlElement BaoCao;
        private DevExpress.XtraBars.Navigation.AccordionControlElement mainAccordionGroup;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView;
        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement PhanQuyen;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DHB;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DHM;
        private DevExpress.XtraBars.Navigation.AccordionControlElement NKBH;
        private DevExpress.XtraBars.Navigation.AccordionControlElement BCCN;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DTBH;
        private DevExpress.XtraBars.Navigation.AccordionControlElement BKPBHPX;
        private DevExpress.XtraBars.Navigation.AccordionControlElement PQ;
        private DevExpress.XtraBars.Navigation.AccordionControlElement TruyVan;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DSDHM;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DSDHB;
        private DevExpress.XtraBars.BarButtonItem barButtonLogout;
        private DevExpress.XtraBars.BarButtonItem barButtonUpload;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage;
        private DevExpress.XtraBars.Navigation.AccordionControlElement XKNK;
        private DevExpress.XtraBars.Navigation.AccordionControlElement HDB;
        private DevExpress.XtraBars.Navigation.AccordionControlElement HDM;
        private DevExpress.XtraBars.Navigation.AccordionControlElement NKTL;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator4;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator3;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator5;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator2;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement BCDS;
        private DevExpress.XtraBars.Navigation.AccordionControlElement BCLG;
        private DevExpress.XtraBars.Navigation.AccordionControlElement BCTN;
        private DevExpress.XtraBars.Navigation.AccordionControlElement SQTMTGNH;
        private DevExpress.XtraBars.Navigation.AccordionControlElement THXNT;
        private DevExpress.XtraBars.Navigation.AccordionControlElement PDK;
        private DevExpress.XtraBars.Navigation.AccordionControlElement SC;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DSHDM;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DSHDB;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DSPNX;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DanhMuc;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator6;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DMLNV;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DMHH;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DMK;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DMPTTT;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DMNV;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DMBG;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DMTK;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DMKH;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLogout;
        private DevExpress.XtraBars.Navigation.AccordionControlElement GG;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DSHDGG;
        private DevExpress.XtraBars.BarButtonItem btnChangePass;
        private DevExpress.XtraBars.BarButtonItem btnCreateUser;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
    }
}