﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL.Models;
using AccountingApplication.Views.DesignReport;

namespace AccountingApplication.Views.DocumentView
{
    public partial class ListSaleDocument : DevExpress.XtraEditors.XtraForm
    {
        public ListSaleDocument()
        {
            InitializeComponent();
        }

        public void printInvoice(List<RecordSalesHistory> data)
        {
            ListSalesReport report = new ListSalesReport();
            foreach (DevExpress.XtraReports.Parameters.Parameter item in report.Parameters)
                item.Visible = false;
            report.DataSource = data;
            documentViewer1.DocumentSource = report;
            report.CreateDocument();
        }
    }
}