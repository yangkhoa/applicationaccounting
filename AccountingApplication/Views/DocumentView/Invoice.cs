﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using AccountingApplication.Views.DesignReport;
using BLL.Models;
using BLL.Models.ReportFormModel;

namespace AccountingApplication.Views.DocumentView
{
    public partial class Invoice : DevExpress.XtraEditors.XtraForm
    {
        public Invoice()
        {
            InitializeComponent();
        }

        public void printInvoice(List<LoadFormINV> data, LoadPDKMaster dataMaster)
        {
            INVReport report = new INVReport();
            foreach (DevExpress.XtraReports.Parameters.Parameter item in report.Parameters)
                item.Visible = false;
            report.initdata(dataMaster);
            report.DataSource = data;
            documentViewer1.DocumentSource = report;
            report.CreateDocument();
        }
    }
}