﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL.Models.ReportFormModel;
using AccountingApplication.Views.DesignReport;

namespace AccountingApplication.Views.DocumentView
{
    public partial class SOCAIForm : DevExpress.XtraEditors.XtraForm
    {
        public SOCAIForm()
        {
            InitializeComponent();
        }

        public void printInvoice(List<LoadSC> data, string loaiTK, string Year)
        {
            SocaiReport report = new SocaiReport();
            foreach (DevExpress.XtraReports.Parameters.Parameter item in report.Parameters)
                item.Visible = false;
            report.DataSource = data;
            report.initdata(loaiTK, Year);

            documentViewer1.DocumentSource = report;
            report.CreateDocument();
        }

    }
}