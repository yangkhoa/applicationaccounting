﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL.Models.ReportFormModel;
using AccountingApplication.Views.DesignReport;

namespace AccountingApplication.Views.DocumentView
{
    public partial class THNXTDocument : DevExpress.XtraEditors.XtraForm
    {
        public THNXTDocument()
        {
            InitializeComponent();
        }

        public void printInvoice(List<LoadTHNXT> data,string datefrom,string dateto)
        {
            THNXTReport report = new THNXTReport();
            foreach (DevExpress.XtraReports.Parameters.Parameter item in report.Parameters)
                item.Visible = false;
            report.DataSource = data;
            report.initdata(datefrom,dateto);

            documentViewer1.DocumentSource = report;
            report.CreateDocument();
        }
    }
}