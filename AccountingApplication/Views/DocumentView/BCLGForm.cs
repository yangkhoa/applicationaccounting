﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL.Models.ReportFormModel;
using AccountingApplication.Views.DesignReport;

namespace AccountingApplication.Views.DocumentView
{
    public partial class BCLGForm : DevExpress.XtraEditors.XtraForm
    {
        public BCLGForm()
        {
            InitializeComponent();
        }

        public void printInvoice(List<LoadBCLG> data)
        {
            BCLGReport report = new BCLGReport();
            foreach (DevExpress.XtraReports.Parameters.Parameter item in report.Parameters)
                item.Visible = false;
            //report.initdata(dataMaster);
            report.DataSource = data;
            documentViewer1.DocumentSource = report;
            report.CreateDocument();
        }
    }
}