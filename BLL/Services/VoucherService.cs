﻿using BLL.Models;
using BLL.Models.LoadForm;
using DAL.DBContext;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class VoucherService
    {
        private static VoucherService instance;

        public static VoucherService Instance
        {
            get { if (instance == null) instance = new VoucherService(); return instance; }
            private set => instance = value;
        }
        private VoucherService() { }


        public List<DONDATHANG> GetDONDATHANGs(Filter filters)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                if (filters.Datetime1.Year >= 2000)
                {
                    if (filters.VOtext != "")
                    {
                        List<DONDATHANG> DDH = (from a in db.DONDATHANGs
                                                where a.SODDH == filters.VOtext
                                                select a).ToList();
                        DDH.Where(x => x.NGAYHT >= filters.Datetime1 && x.NGAYHT <= filters.datetime2);
                        return DDH;
                    }
                    else
                    {
                        var DDH1 = db.DONDATHANGs.ToList().Where(x => x.NGAYHT >= filters.Datetime1 && x.NGAYHT <= filters.datetime2);
                        return db.DONDATHANGs.ToList();
                    }
                }
                else
                {

                    if (filters.VOtext != "")
                    {
                        List<DONDATHANG> DDH = (from a in db.DONDATHANGs
                                                where a.SODDH == filters.VOtext
                                                && a.NGAYHT.Value.Year * 100 + a.NGAYHT.Value.Month >= filters.periodY1 * 100 + filters.periodM1
                                                && a.NGAYHT.Value.Year * 100 + a.NGAYHT.Value.Month <= filters.periodY2 * 100 + filters.periodM2
                                                select a).ToList();

                        return DDH;
                    }
                    else
                    {
                        return db.DONDATHANGs.ToList();
                    }
                }


            }
        }

        public List<DONDATHANG> RefreshForm()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.DONDATHANGs.ToList();
            }
        }

        public List<Period> GetPeriods()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                List<Period> listPeriod = (from q in db.DONDATHANGs
                                           orderby q.NGAYHT
                                           select new Period
                                           {
                                               Month = q.NGAYHT.Value.Month,
                                               Year = q.NGAYHT.Value.Year,
                                               MY = q.NGAYHT.Value.Month + "/" + q.NGAYHT.Value.Year
                                           }).Distinct().ToList();
                return listPeriod;
            }

        }
        public string getVoucherNo(string LOAIHD)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var voucherno = (from Vc in db.LOAIHOADONs
                                 where Vc.MALOAICT == LOAIHD
                                 select Vc.SOTANGTUDONG).FirstOrDefault().ToString();
                return voucherno;
            }
        }

        public void InsertDDH(List<CTDONDATHANG> CTDDHs, DONDATHANG DDHs, string voucherType, int region)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                if (region == 1)
                {
                    var rmDDH = db.DONDATHANGs.Find(voucherType);
                    List<CTDONDATHANG> rmctDDH = db.CTDONDATHANGs.Where(x => x.SODDH.Contains(voucherType)).ToList();
                    db.DONDATHANGs.Remove(rmDDH);
                    db.CTDONDATHANGs.RemoveRange(rmctDDH);
                    db.SaveChanges();
                }

                db.DONDATHANGs.Add(DDHs);
                db.CTDONDATHANGs.AddRange(CTDDHs);
                db.SaveChanges();

                if (region == 0)
                {
                    LOAIHOADON _up = db.LOAIHOADONs.Find(voucherType);
                    ++_up.SOTANGTUDONG;

                    db.LOAIHOADONs.Attach(_up);
                    db.Entry(_up).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        public List<CTDONDATHANG> FindVoucherDetail(string _SODDH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                List<CTDONDATHANG> ctDH = db.CTDONDATHANGs.Where(x => x.SODDH == _SODDH).ToList();
                return ctDH;
            }
        }
        public DONDATHANG FindVoucher(string _SODDH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                DONDATHANG _DDH = db.DONDATHANGs.Find(_SODDH);
                return _DDH;
            }
        }

        public List<LoadFormINV> returnForm(string _SODDH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                List<LoadFormINV> LF = (from lf in db.CTDONDATHANGs
                                        join hhs in db.HANGHOAs
                                        on lf.MAHH equals hhs.MAHH
                                        where lf.SODDH == _SODDH
                                        select new LoadFormINV
                                        {
                                            SOCT = lf.SODDH,
                                            MAHH = lf.MAHH,
                                            MAKHO = lf.KHO,
                                            TENHH = hhs.TENHH,
                                            MADVT = hhs.MADVT,
                                            SODDH = lf.SODDH,
                                            SOLUONG = lf.SOLUONGDH,
                                            DONGIA = lf.DONGIADH,
                                            THANHTIEN = lf.SOLUONGDH * lf.DONGIADH,
                                            TYLECK = lf.TYLECK,
                                            TIENCK = lf.TIENCK,
                                            MATHUE = lf.MATHUE,
                                            TIENTHUE = lf.TIENTHUE,
                                            TKDT =hhs.TKDOANHTHU
                                            
                                        }).ToList();
                return LF;
            }
        }



        public List<LoadFormDDH> FindCTDH(string _SODDH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                List<LoadFormDDH> LF = (from lf in db.CTDONDATHANGs
                                        join hhs in db.HANGHOAs
                                        on lf.MAHH equals hhs.MAHH
                                        where lf.SODDH == _SODDH
                                        select new LoadFormDDH
                                        {
                                            SODDH = lf.SODDH,
                                            MAHH = lf.MAHH,
                                            TENHH = hhs.TENHH,
                                            MADVT = hhs.MADVT,
                                            MAKHO = lf.KHO,
                                            SOLUONGDH = lf.SOLUONGDH,
                                            DONGIADH = lf.DONGIADH,
                                            TIENCK = lf.TIENCK,
                                            THANHTIENDH = lf.THANHTIENDH,
                                            MATHUE = lf.MATHUE,
                                            TIENTHUE = lf.TIENTHUE,
                                            TYLECK = lf.TYLECK
                                        }).ToList();
                return LF;
            }
        }

        public void removeCTDDH(string SODDH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var rmDDH = db.DONDATHANGs.Find(SODDH);
                List<CTDONDATHANG> rmctDDH = db.CTDONDATHANGs.Where(x => x.SODDH.Contains(SODDH)).ToList();
                db.DONDATHANGs.Remove(rmDDH);
                db.CTDONDATHANGs.RemoveRange(rmctDDH);
                db.SaveChanges();
            }
        }

        public List<LoadGetForm> loadGetForms(string Votext, string DateFrom, string DateTo, int region, string Manv)
        {
            List<LoadGetForm> records = new List<LoadGetForm>();

            using (var connection = new SqlConnection(new CLDLQLBH1Entities().Database.Connection.ConnectionString))
            {
                dynamic result;
                if (region == 0)
                {
                    result = connection.Query<dynamic>("sp_loadchungtu",
                        new { @SoCT = Votext, @DateFrom = DateFrom, @DateTo = DateTo, @IsTime = 1, @TypeOfFilter = 0, @Manv = Manv },
                        commandType: System.Data.CommandType.StoredProcedure);
                }
                else
                {
                    result = connection.Query<dynamic>("sp_loadchungtu",
                        new { @SoCT = Votext, @DateFrom = DateFrom, @DateTo = DateTo, @IsTime = 1, @TypeOfFilter = 1, @Manv = Manv },
                        commandType: System.Data.CommandType.StoredProcedure);
                }
                foreach (var item in result)
                {
                    LoadGetForm record = new LoadGetForm
                    {
                        checkID = item.checkID,
                        SOCT = item.SOCT,
                        MANV = item.MANV,
                        MAKH = item.MAKH,
                        NGAYLAPCT = item.NGAYLAPCT ?? DateTime.Now
                    };

                    records.Add(record);
                }
            }
            return records;
        }
        public bool AllowEdit(string _SODDH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                int _allow = (from CTHD in db.CTHOADONs
                              where CTHD.SODDH == _SODDH
                              select CTHD.SODDH).Count();
                if (_allow > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
