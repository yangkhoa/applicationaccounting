﻿using DAL.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ObjectService
    {
        private static ObjectService instance;

        public static ObjectService Instance
        {
            get { if (instance == null) instance = new ObjectService(); return instance; }
            private set => instance = value;
        }
        private ObjectService() { }

        public List<KHACHHANG> GetKHACHHANGs()
        {
            using(var db = new CLDLQLBH1Entities())
            {
                return db.KHACHHANGs.ToList();
            }
        }

        public List<NHANVIEN> GetNHANVIENs()
        {
            using(var db = new CLDLQLBH1Entities())
            {
                return db.NHANVIENs.ToList();
            }
        }

        public List<HANGHOA> GetHANGHOAs()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.HANGHOAs.ToList();
            }
        }
        public HANGHOA GetCTHH(string id)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                HANGHOA HHs = db.HANGHOAs.Find(id);
                return HHs;
            }
        }

        public KHACHHANG GetCTKH(string id)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                KHACHHANG KHs = db.KHACHHANGs.Find(id);
                return KHs;
            }
        }

        public NHANVIEN GetCTNV(string id)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                NHANVIEN NVs = db.NHANVIENs.Find(id);
                return NVs;
            }
        }

        public PTTHANHTOAN GetCTPT(string id)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                PTTHANHTOAN PTs = db.PTTHANHTOANs.Find(id);
                return PTs;
            }
        }

        public TAIKHOAN GetCTTK(string id)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                TAIKHOAN PTs = db.TAIKHOANs.Find(id);
                return PTs;
            }
        }

        public KHOHANG GetKHO(string id)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                KHOHANG KHOes = db.KHOHANGs.Find(id);
                return KHOes;
            }
        }

        public void InsertHH(HANGHOA HH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                db.HANGHOAs.Add(HH);
                db.SaveChanges();
            }
        }
        public void InsertKH(KHACHHANG KH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                db.KHACHHANGs.Add(KH);
                db.SaveChanges();
            }
        }
 
        public void InsertNV(NHANVIEN NV)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                db.NHANVIENs.Add(NV);
                db.SaveChanges();
            }
        }

        public void InsertPTTT(PTTHANHTOAN pTTHANHTOAN)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                db.PTTHANHTOANs.Add(pTTHANHTOAN);
                db.SaveChanges();
            }
        }
        public void InsertTK(TAIKHOAN TKs)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                db.TAIKHOANs.Add(TKs);
                db.SaveChanges();
            }
        }
        public void InsertKHO(KHOHANG KHs)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                db.KHOHANGs.Add(KHs);
                db.SaveChanges();
            }
        }

        public void UpdateHH(HANGHOA HHs)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var hhUd = db.HANGHOAs.Find(HHs.MAHH);
                hhUd.TENHH = HHs.TENHH;
                hhUd.MOTA = HHs.MOTA;
                hhUd.LOAIHH = HHs.LOAIHH;
                hhUd.MADVT = HHs.MADVT;
                hhUd.TKDOANHTHU = HHs.TKDOANHTHU;
                hhUd.TKGIAVON = HHs.TKGIAVON;
                hhUd.TKKHO = HHs.TKKHO;
                db.SaveChanges();
                   
            }
        }
        public void UpdateKH(KHACHHANG KHs)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var khUd = db.KHACHHANGs.Find(KHs.MAKH);
                khUd.TENKH = KHs.TENKH;
                khUd.EMAILKH = KHs.EMAILKH;
                khUd.DIACHI = KHs.DIACHI;
                khUd.MST = KHs.MST;
                khUd.SDTKH = KHs.SDTKH;
                db.SaveChanges();

            }
        }
        public void UpdateNV(NHANVIEN NVs)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var nvUd = db.NHANVIENs.Find(NVs.MANV);
                nvUd.TENNV = NVs.TENNV;
                nvUd.EMAILNV = NVs.EMAILNV;
                nvUd.CMND = NVs.CMND;
                nvUd.GIOITINH = NVs.GIOITINH;
                nvUd.MAPB = NVs.MAPB;
                nvUd.NGAYSINH = NVs.NGAYSINH;
                nvUd.MACHUCVU = NVs.MACHUCVU;
                nvUd.SDTNV = NVs.SDTNV;
                nvUd.QUEQUAN = NVs.QUEQUAN;

                db.SaveChanges();

            }
        }

        public void UpdatePTTT(PTTHANHTOAN PTTTs)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var ptUd = db.PTTHANHTOANs.Find(PTTTs.MAPTTT);
                ptUd.TENPTTT = PTTTs.TENPTTT;
                ptUd.SONGAYNO = PTTTs.SONGAYNO;
                db.SaveChanges();

            }
        }

        public void UpdateTK(TAIKHOAN TKs)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var tkUd = db.TAIKHOANs.Find(TKs.MATK);
                tkUd.TENTK = TKs.TENTK;
                tkUd.TKME = TKs.TKME;
                tkUd.BACTK = TKs.BACTK;
                db.SaveChanges();

            }
        }

        public void UpdateKHO(KHOHANG KHs)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var KHOUd = db.KHOHANGs.Find(KHs.MAKHO);
                KHOUd.TENKHO = KHs.TENKHO;
                db.SaveChanges();

            }
        }

        public void removeHH(string MAHH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var HH = db.HANGHOAs.Find(MAHH);
                db.HANGHOAs.Remove(HH);
                db.SaveChanges();
            }
        }

        public void removeKH(string MAKH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var KH = db.KHACHHANGs.Find(MAKH);
                db.KHACHHANGs.Remove(KH);
                db.SaveChanges();
            }
        }

        public void removeNV(string MANV)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var NV = db.NHANVIENs.Find(MANV);
                db.NHANVIENs.Remove(NV);
                db.SaveChanges();
            }
        }

        public void removePTTT(string MAPT)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var PTTT = db.PTTHANHTOANs.Find(MAPT);
                db.PTTHANHTOANs.Remove(PTTT);
                db.SaveChanges();
            }
        }
        public void removeTK(string MATK)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var TK = db.TAIKHOANs.Find(MATK);
                db.TAIKHOANs.Remove(TK);
                db.SaveChanges();
            }
        }

        public void removeKHO(string MAKHO)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var KHO = db.KHOHANGs.Find(MAKHO);
                db.KHOHANGs.Remove(KHO);
                db.SaveChanges();
            }
        }


        public List<KHOHANG> GetKHOs()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.KHOHANGs.ToList();
            }
        }
        public List<LOAIHOADON> GetLoaiCTs()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.LOAIHOADONs.ToList();
            }
        }
        public List<PTTHANHTOAN> GetPTTTs()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.PTTHANHTOANs.ToList();
            }
        }

        public List<DONVITINH> GetDONVITINHs()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.DONVITINHs.ToList();
            }
        }
        public List<LOAIHH> GetLOAIHHs()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.LOAIHHs.ToList();
            }
        }

        public List<TAIKHOAN> GetTAIKHOANs()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.TAIKHOANs.ToList();
            }
        }


        public List<THUEGTGT> GetTHUEGTGTs()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.THUEGTGTs.ToList();
            }
        }
        public List<PHONGBAN> Getphongbans()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.PHONGBANs.ToList();
            }
        }

        public List<CHUCVU> GetCHUCVUs()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.CHUCVUs.ToList();
            }
        }

    }
}
