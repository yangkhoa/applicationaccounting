﻿using BLL.Models;
using DAL.DBContext;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ReceiptService
    {
        private static ReceiptService instance;

        public static ReceiptService Instance
        {
            get { if (instance == null) instance = new ReceiptService(); return instance; }
            private set => instance = value;
        }
        private ReceiptService() { }

        public List<PHIEUTHU> GetPHIEUTHUs()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.PHIEUTHUs.ToList();
            }
        }
        public PHIEUTHU FindReceipt(string _SOPT)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                PHIEUTHU _PT = db.PHIEUTHUs.Find(_SOPT);
                return _PT;
            }
        }
        public List<LoadReceipForm> returnForm(string _SOPT)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                List<LoadReceipForm> LF = (from lf in db.CTPHIEUTHUs

                                           where lf.SOPTHU == _SOPT
                                           select new LoadReceipForm
                                           {
                                               SOCT = lf.SOCT,
                                               TKCOPT = lf.TKCOPT,
                                               SOTIEN = lf.DATHU ?? 0
                                           }).ToList();
                return LF;
            }
        }
        public void InsertPhieuThu(List<CTPHIEUTHU> CTHDs, PHIEUTHU HDs, string voucherType, int region)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                if (region == 1)
                {
                    var rmDDH = db.PHIEUTHUs.Find(voucherType);
                    List<CTPHIEUTHU> rmctDDH = db.CTPHIEUTHUs.Where(x => x.SOPTHU.Contains(voucherType)).ToList();
                    db.PHIEUTHUs.Remove(rmDDH);
                    db.CTPHIEUTHUs.RemoveRange(rmctDDH);
                    db.SaveChanges();
                }

                db.PHIEUTHUs.Add(HDs);
                db.CTPHIEUTHUs.AddRange(CTHDs);
                db.SaveChanges();

                int subType = voucherType.Length - 4;

                voucherType = voucherType.Remove(voucherType.Length - subType);
                if (region == 0)
                {
                    LOAIHOADON _up = db.LOAIHOADONs.Find(voucherType);
                    ++_up.SOTANGTUDONG;

                    db.LOAIHOADONs.Attach(_up);
                    db.Entry(_up).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
        public List<LoadReceipForm> loadGetForms(string Votext, string DateFrom, string DateTo, int region, string makh)
        {
            List<LoadReceipForm> records = new List<LoadReceipForm>();

            using (var connection = new SqlConnection(new CLDLQLBH1Entities().Database.Connection.ConnectionString))
            {
                dynamic result;
                if (region == 0)
                {
                    result = connection.Query<dynamic>("sp_loadhoadonphaithu",
                        new { @SoCT = Votext, @DateFrom = DateFrom, @DateTo = DateTo, @IsTime = 1, @MAKH = makh },
                        commandType: System.Data.CommandType.StoredProcedure);
                }
                else
                {
                    result = connection.Query<dynamic>("sp_loadhoadonphaithu",
                        new { @SoCT = Votext, @DateFrom = DateFrom, @DateTo = DateTo, @IsTime = 1, @MAKH = makh },
                        commandType: System.Data.CommandType.StoredProcedure);
                }
                foreach (var item in result)
                {
                    LoadReceipForm record = new LoadReceipForm
                    {
                        SOCT = item.SOCT,
                        MAKH = item.MAKH,
                        NGAYLAPCT = item.NGAYLAPCT ?? DateTime.Now,
                        THANHTIEN = item.THANHTIEN
                    };

                    records.Add(record);
                }
            }
            return records;
        }
        public void removePT(string _SOPT)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var rmHD = db.PHIEUTHUs.Find(_SOPT);
                List<CTPHIEUTHU> rmctHD = db.CTPHIEUTHUs.Where(x => x.SOPTHU.Contains(_SOPT)).ToList();
                db.PHIEUTHUs.Remove(rmHD);
                db.CTPHIEUTHUs.RemoveRange(rmctHD);
                db.SaveChanges();
            }
        }
        public List<Period> GetPeriods()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                List<Period> listPeriod = (from q in db.PHIEUTHUs
                                           orderby q.NGAYHT
                                           select new Period
                                           {
                                               Month = q.NGAYHT.Value.Month,
                                               Year = q.NGAYHT.Value.Year,
                                               MY = q.NGAYHT.Value.Month + "/" + q.NGAYHT.Value.Year
                                           }).Distinct().ToList();
                return listPeriod;
            }

        }
    }
}
