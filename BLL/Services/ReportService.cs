﻿using BLL.Models;
using BLL.Models.ReportFormModel;
using DAL.DBContext;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ReportService
    {
        private static ReportService instance;

        public static ReportService Instance
        {
            get { if (instance == null) instance = new ReportService(); return instance; }
            private set => instance = value;
        }

        private ReportService() { }

        //public List<RecordRevenue> GetBaoCaoDoanhThu()
        //{
        //    List<RecordRevenue> records = new List<RecordRevenue>();

        //    using(var db = new CLDLQLBH1Entities())
        //    {
        //        dynamic result = db.sp_ThongkeDoanhthu(null, null, string.Empty);
        //        foreach (var item in result)
        //        {
        //            RecordRevenue record = new RecordRevenue
        //            {
        //                DIACHI = item.DIACHI,
        //                DOANHTHUBH = item.DOANHTHUBH,
        //                CK = item.CK,
        //                DOANHTHUTHUAN = item.DOANHTHUTHUAN,
        //                SDTKH = item.SDTKH,
        //                MAKH = item.MAKH,
        //                MST = item.MST,
        //                TENKH = item.TENKH
        //            };

        //            records.Add(record);
        //        }
        //        return records;
        //    }
        //}

        public List<RecordSalesHistory> GetBaoCaoBanHang(string dateFrom, string dateTo)
        {
            List<RecordSalesHistory> records = new List<RecordSalesHistory>();

            using (var connection = new SqlConnection(new CLDLQLBH1Entities().Database.Connection.ConnectionString))
            {
                dynamic result;

                result = connection.Query<dynamic>("sp_nhatkybanhang_01",
                    new { @TranMonth = 0, @TranYear = 0, @DateFrom = dateFrom, @DateTo = dateTo, @IsTime = 2 },
                    commandType: System.Data.CommandType.StoredProcedure);


                foreach (var item in result)
                {
                    RecordSalesHistory record = new RecordSalesHistory
                    {
                        NGAYGHISO = item.NGAYGHISO,
                        SOCT = item.SOCT,
                        NGAYLAPCT = item.NGAYLAPCT,
                        DIENGIAICT = item.DIENGIAICT,
                        TONGTHANHTOAN = item.TONGTHANHTOAN ?? 0,
                        MANV = item.MANV,
                        HH = item.HH ?? 0
                    };

                    records.Add(record);
                }
            }
            return records;

        }

        public List<LoadPDK> getPDK(string _SOCT)
        {
            List<LoadPDK> records = new List<LoadPDK>();
            using (var connection = new SqlConnection(new CLDLQLBH1Entities().Database.Connection.ConnectionString))
            {
                dynamic result;

                result = connection.Query<dynamic>("sp_phieudinhkhoan",
                    new { @SoCT = _SOCT },
                    commandType: System.Data.CommandType.StoredProcedure);


                foreach (var item in result)
                {
                    LoadPDK record = new LoadPDK
                    {
                        TK = item.TK,
                        MAKH = item.MAKH,
                        PSNO = item.PSNO,
                        PSCO = item.PSCO,
                        TENTK = item.TENTK
                    };

                    records.Add(record);
                }
            }
            return records;

        }

        public List<LoadBCLG> GetBCLG(string _SOCT,string DateFrom,string DateTo)
        {
            List<LoadBCLG> records = new List<LoadBCLG>();
            using (var connection = new SqlConnection(new CLDLQLBH1Entities().Database.Connection.ConnectionString))
            {
                dynamic result;

                result = connection.Query<dynamic>("sp_baocaolaigop",
                    new { @SoCT = _SOCT , @DateFrom = DateFrom , @DateTo = DateTo},
                    commandType: System.Data.CommandType.StoredProcedure);


                foreach (var item in result)
                {
                    LoadBCLG record = new LoadBCLG
                    {
                        LOAIHANG = item.LOAIHANG,
                        TENHH = item.TENHH,
                        MAHH = item.MAHH,
                        GIAMGIA = item.GIAMGIA ?? 0,
                        GIAVON = item.GIAVON ?? 0,
                        DOANHTHU = item.DOANHTHU ?? 0,
                        DVT = item.DVT ?? 0,
                        LAIGOP = item.LAIGOP ?? 0,
                        SL = item.SL ?? 0
                    };

                    records.Add(record);
                }
            }
            return records;

        }

        public LoadPDKMaster GetPDKMaster(string _SOCT)
        {
            LoadPDKMaster  records = new LoadPDKMaster();
            using (var connection = new SqlConnection(new CLDLQLBH1Entities().Database.Connection.ConnectionString))
            {
                dynamic result;

                result = connection.Query<dynamic>("sp_phieudinhkhoanMaster",
                    new { @SoCT = _SOCT },
                    commandType: System.Data.CommandType.StoredProcedure);
               
                foreach (var item in result)
                {
                    records.DIENGIAICT = item.DIENGIAICT;
                    records.SOCT = _SOCT;
                    records.TENKH = item.TENKH;
                    records.NGAYLAPCT = item.NGAYLAPCT;
                    records.DIACHI = item.DIACHI;
                    records.MST = item.MST;
                }

            }
            return records;
        }

        public List<LoadTHNXT> GetTHNXT(string MAHH, string dateFrom, string dateTo)
        {
            List<LoadTHNXT> records = new List<LoadTHNXT>();
            using (var connection = new SqlConnection(new CLDLQLBH1Entities().Database.Connection.ConnectionString))
            {
                dynamic result;

                result = connection.Query<dynamic>("sp_Tonghopxuat",
                    new { @MAHH = MAHH , @DateFrom = dateFrom, @DateTo  = dateTo},
                    commandType: System.Data.CommandType.StoredProcedure);


                foreach (var item in result)
                {
                    LoadTHNXT record = new LoadTHNXT
                    {
                        MAHH = item.MAHH,
                        TENHH = item.TENHH,
                        DVT = item.DVT,
                        KHO = item.KHO,
                        SOLUONGTON = item.SOLUONGTON,
                        TONGSOLUONG = item.TONGSOLUONG,
                        TONCUOIKY = item.TONCUOIKY
                    };

                    records.Add(record);
                }
            }
            return records;
        }

        public List<LoadSC> GetSC(string MATK, string dateFrom, string dateTo)
        {
            List<LoadSC> records = new List<LoadSC>();
            using (var connection = new SqlConnection(new CLDLQLBH1Entities().Database.Connection.ConnectionString))
            {
                dynamic result;

                result = connection.Query<dynamic>("sp_SOCAI",
                    new { @MATK = MATK, @DateFrom = dateFrom, @DateTo = dateTo },
                    commandType: System.Data.CommandType.StoredProcedure);


                foreach (var item in result)
                {
                    LoadSC record = new LoadSC
                    {
                        SOPHIEU = item.SOPHIEU,
                        NGAYLAPCT = item.NGAYLAPCT,
                        TKDU = item.TKDU,
                        DIENGIAI = item.DIENGIAI,
                        PSCO = item.PSCO ,
                        PSNO = item.PSNO 
                    };

                    records.Add(record);
                }
            }
            return records;
        }


        public List<LoadCNPT> GetCNPT(string MAHH, string dateFrom, string dateTo)
        {
            List<LoadCNPT> records = new List<LoadCNPT>();
            using (var connection = new SqlConnection(new CLDLQLBH1Entities().Database.Connection.ConnectionString))
            {
                dynamic result;

                result = connection.Query<dynamic>("sp_congnophaithu",
                    new { @MAHH = MAHH, @DateFrom = dateFrom, @DateTo = dateTo },
                    commandType: System.Data.CommandType.StoredProcedure);


                foreach (var item in result)
                {
                    LoadCNPT record = new LoadCNPT
                    {
                        SOCT = item.SOCT,
                        NGAYLAPCT = item.NGAYLAPCT,
                        KYHIEUHD = item.KYHIEUHD,
                        SOSERI = item.SOSERI,
                        DIENGIAICT = item.DIENGIAICT,
                        TKDOIUNG = item.TKDOIUNG,
                        PSNO = item.PSNO,
                        PSCO = Convert.ToDecimal(item.PSCO),
                        SODUNO = item.SODUNO,
                        SODUCO = item.SODUCO
                    };

                    records.Add(record);
                }
            }
            return records;
        }

        public List<LoadTMTG> GETTMTG(string MACT)
        {
            List<LoadTMTG> records = new List<LoadTMTG>();
            using (var connection = new SqlConnection(new CLDLQLBH1Entities().Database.Connection.ConnectionString))
            {
                dynamic result;

                result = connection.Query<dynamic>("sp_SQtienmattiengui",
                    new { @SoCT = MACT },
                    commandType: System.Data.CommandType.StoredProcedure);


                foreach (var item in result)
                {
                    LoadTMTG record = new LoadTMTG
                    {
                        NGAYPTHU = item.NGAYPTHU,
                        SOPTHU = item.SOPTHU,
                        DIENGIAIPT = item.DIENGIAIPT,
                        TONGTIENPTHU = item.TONGTIENPTHU ?? 0
                    };

                    records.Add(record);
                }
            }
            return records;
        }

        public List<HOADON> GetHOADONXUATKHO(string MAHH)
        {
            List<HOADON> records = new List<HOADON>();
            using (var connection = new SqlConnection(new CLDLQLBH1Entities().Database.Connection.ConnectionString))
            {
                dynamic result;

                result = connection.Query<dynamic>("sp_Getphieuxuat",
                    new { @MAHH = MAHH },
                    commandType: System.Data.CommandType.StoredProcedure);


                foreach (var item in result)
                {
                    HOADON record = new HOADON
                    {
                        SOCT = item.SOCT,
                        NGAYLAPCT = item.NGAYLAPCT,
                        DIENGIAICT = item.DIENGIAICT,
                        MAKH = item.MAKHO,
                        TONGSLUONG = item.SOLUONG
                    };

                    records.Add(record);
                }
            }
            return records;
        }


    }
}
