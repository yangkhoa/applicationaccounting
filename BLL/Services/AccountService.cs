﻿using BLL.Models;
using DAL.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class AccountService
    {
        private static AccountService instance;

        public static AccountService Instance
        {
            get { if (instance == null) instance = new AccountService(); return instance; }
            private set => instance = value;
        }

        private AccountService() { }

        public List<TAIKHOANNV> GetFullListTKNV()
        {
            using(var db = new CLDLQLBH1Entities())
            {
                return db.TAIKHOANNVs.ToList();
            }
        }

        public List<TAIKHOANNV> GetListTKNVWithoutCurrentUser(string userName)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.TAIKHOANNVs.Where(x=>x.TAIKHOAN != userName).ToList();
            }
        }

        public TAIKHOANNV Login(string username,string password)
        {
            using(var db = new CLDLQLBH1Entities())
            {
                TAIKHOANNV user = db.TAIKHOANNVs.FirstOrDefault(x => x.TAIKHOAN == username && x.MATKHAU == password);
                return user;
            }
        }

        public List<Role> GetListRole()
        {
            using(var db = new CLDLQLBH1Entities())
            {
                return db.CHUCVUs.Select(x => new Role()
                {
                    IDCV = x.IDCV,
                    MACHUCVU = x.MACHUCVU,
                    TENCHUCVU = x.TENCHUCVU
                }).ToList();
            }
        }

        public NHOMQUYEN GetNhomQuyen(string maNQ)
        {
            using(var db = new CLDLQLBH1Entities())
            {
                return db.NHOMQUYENs.FirstOrDefault(x => x.MANQ == maNQ);
            }
        }

        public List<MenuUser> GetListMenu()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.NHOMQUYENs.Select(x => new MenuUser()
                {
                    MANQ = x.MANQ,
                    TENNQ = x.TENNQ
                }).ToList();
            }
        }

        public List<MenuUser> GetListMenuByRole(int idChucVu)
        {
            List<MenuUser> listMenu = new List<MenuUser>();

            using(var db = new CLDLQLBH1Entities())
            {
                listMenu = (from q in db.QUYENs
                             join nq in db.NHOMQUYENs on q.MANQ equals nq.MANQ
                             join cv in db.CHUCVUs on q.IDCHUCVU equals cv.IDCV
                             where nq.MANQ == q.MANQ
                             && q.IDCHUCVU == cv.IDCV
                             && q.IDCHUCVU == idChucVu
                             select new MenuUser
                             {
                                 MANQ = q.MANQ,
                                 TENNQ = nq.TENNQ,
                                 THEM = (int)q.MOI,
                                 XOA = (int)q.XOA,
                                 SUA = (int)q.SUA,
                                 IDCV = (int)q.IDCHUCVU
                             }).ToList();

                return listMenu;
            }
        }

        public MessageModel InsertNewPermission(string maNQ,int idChucVu)
        {
            MessageModel msg = new MessageModel
            {
                Message = "Thêm mới thất bại",
                Type = MessageTypeEnum.Error
            };
            try
            {
                using(var db = new CLDLQLBH1Entities())
                {
                    var menuByRole = GetListMenuByRole(idChucVu).ToDictionary(x => x.MANQ);

                    if (menuByRole.ContainsKey(maNQ))
                    {
                        msg.Message = "Chức vụ đã bao gồm quyền này";
                        return msg;
                    }

                    QUYEN quyen = new QUYEN
                    {
                        IDCHUCVU = idChucVu,
                        MOI = 1,
                        XOA = 1,
                        SUA = 1,
                        MANQ = maNQ,
                        INCT = 1,
                        THUCHIEN = 1,
                        XEM = 1
                    };

                    db.QUYENs.Add(quyen);

                    if (db.SaveChanges() > 0)
                    {
                        msg.Message = "Thêm thành công";
                        msg.Type = MessageTypeEnum.Success;
                        return msg;
                    }
                }
            }
            catch (Exception ex)
            {
                msg.Message = ex.Message.ToString();
            }
            return msg;
        }

        public MessageModel EditPermission(int idChucVu, QUYEN quyenEdit)
        {
            MessageModel msg = new MessageModel
            {
                Message = "Cập nhật thất bại",
                Type = MessageTypeEnum.Error
            };
            try
            {
                using (var db = new CLDLQLBH1Entities())
                {
                    QUYEN quyendb = db.QUYENs.FirstOrDefault(x => x.MANQ == quyenEdit.MANQ && x.IDCHUCVU == idChucVu);

                    quyendb.MOI = quyenEdit.MOI;
                    quyendb.XOA = quyenEdit.XOA;
                    quyendb.SUA = quyenEdit.SUA;

                    if (db.SaveChanges() > 0)
                    {
                        msg.Message = "Cập nhật thành công";
                        msg.Type = MessageTypeEnum.Success;
                        return msg;
                    }
                }
            }
            catch (Exception ex)
            {
                msg.Message = ex.Message.ToString();
            }
            return msg;
        }

        public MessageModel DeletePermission(int idChucVu, string maNQ)
        {
            MessageModel msg = new MessageModel
            {
                Message = "Xóa thất bại",
                Type = MessageTypeEnum.Error
            };
            try
            {
                using (var db = new CLDLQLBH1Entities())
                {
                    QUYEN quyendb = db.QUYENs.FirstOrDefault(x => x.MANQ == maNQ && x.IDCHUCVU == idChucVu);

                    db.QUYENs.Remove(quyendb);

                    if (db.SaveChanges() > 0)
                    {
                        msg.Message = "Xóa thành công";
                        msg.Type = MessageTypeEnum.Success;
                        return msg;
                    }
                }
            }
            catch(Exception ex)
            {
                msg.Message = ex.Message.ToString();
            }
            return msg;
        }

        public bool ChangePass(string newPassword, string username)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var user = db.TAIKHOANNVs.FirstOrDefault(x => x.TAIKHOAN == username);
                user.MATKHAU = newPassword;

                return db.SaveChanges() > 0;
            }
        }

        public bool CreateNhanVien(string maNV, string username,string password, string tenChucVu)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                int idChucVu = db.CHUCVUs.FirstOrDefault(x => x.TENCHUCVU == tenChucVu).IDCV;

                TAIKHOANNV taiKhoan = new TAIKHOANNV
                {
                    MACHUCVU = idChucVu,
                    MANV = maNV,
                    MATKHAU = password,
                    TAIKHOAN = username
                };

                db.TAIKHOANNVs.Add(taiKhoan);

                return db.SaveChanges() > 0;
            }
        }

        public TAIKHOANNV GetUserByUsername(string username)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var user = db.TAIKHOANNVs.FirstOrDefault(x => x.TAIKHOAN == username);

                return user;
            }
        }
    }
}
