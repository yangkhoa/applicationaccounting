﻿using BLL.Models;
using DAL.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class InvoiceService
    {
        private static InvoiceService instance;

        public static InvoiceService Instance
        {
            get { if (instance == null) instance = new InvoiceService(); return instance; }
            private set => instance = value;
        }
        private InvoiceService() { }

        public void InsertHoadon(List<CTHOADON> CTHDs, HOADON HDs, string voucherType, int region)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                if (region == 1)
                {
                    var rmDDH = db.HOADONs.Find(voucherType);
                    List<CTHOADON> rmctDDH = db.CTHOADONs.Where(x => x.SOCT.Contains(voucherType)).ToList();
                    db.HOADONs.Remove(rmDDH);
                    db.CTHOADONs.RemoveRange(rmctDDH);
                    db.SaveChanges();
                }

                db.HOADONs.Add(HDs);
                db.CTHOADONs.AddRange(CTHDs);
                db.SaveChanges();
                if (region == 0)
                {
                    LOAIHOADON _up = db.LOAIHOADONs.Find(voucherType);
                    ++_up.SOTANGTUDONG;

                    db.LOAIHOADONs.Attach(_up);
                    db.Entry(_up).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
        public List<LoadFormINV> returnForm(string _SODDH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                List<LoadFormINV> LF = (from lf in db.CTHOADONs
                                        join hhs in db.HANGHOAs
                                        on lf.MAHH equals hhs.MAHH
                                        where lf.SOCT == _SODDH
                                        select new LoadFormINV
                                        {
                                            SOCT = lf.SOCT,
                                            MAHH = lf.MAHH,
                                            TENHH = hhs.TENHH,
                                            MADVT = hhs.MADVT,
                                            SODDH = lf.SOCT,
                                            SOLUONG = lf.SOLUONG,
                                            DONGIA = lf.DONGIA,
                                            MAKHO = lf.MAKHO,
                                            THANHTIEN = lf.THANHTIEN,
                                            TYLECK = lf.TYLECK,
                                            TIENCK = lf.TIENCK,
                                            MATHUE = lf.MATHUE,
                                            TIENTHUE = lf.TIENTHUE,
                                            TKDT = lf.TKDT,
                                            GIAVON = lf.GIAVON,
                                            TIENVON = lf.TIENVON,
                                            TKVT = hhs.TKKHO,
                                            TKGV = lf.TKGV,
                                            TKCK = lf.TKCK,
                                            TKTHUE = lf.TKTHUE,
                                            TKDUTHUE = lf.TKDUTHUE,
                                            TONKHO = lf.TONKHO
                                        }).ToList();
                return LF;
            }
        }

        public List<LoadFormINV> returnFormInvoice(string _SODDH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                List<LoadFormINV> LF = (from lf in db.CTHOADONs
                                        join hhs in db.HANGHOAs
                                        on lf.MAHH equals hhs.MAHH
                                        where lf.SOCT == _SODDH
                                        select new LoadFormINV
                                        {
                                            SOCT = lf.SOCT,
                                            MAHH = lf.MAHH,
                                            TENHH = hhs.TENHH,
                                            MADVT = hhs.MADVT,
                                            SODDH = lf.SOCT,
                                            SOLUONG = lf.SOLUONG,
                                            DONGIA = lf.DONGIA,
                                            MAKHO = lf.MAKHO,
                                            THANHTIEN = lf.THANHTIEN,
                                            TYLECK = lf.TYLECK,
                                            TIENCK = lf.TIENCK,
                                            MATHUE = lf.MATHUE,
                                            TIENTHUE = lf.TIENTHUE,
                                            TKDT = lf.TKDT,
                                            GIAVON = lf.GIAVON,
                                            TIENVON = lf.THANHTIEN - (lf.THANHTIEN - lf.TIENCK) + (lf.TIENTHUE - lf.TIENCK),
                                            TKVT = hhs.TKKHO,
                                            TKGV = lf.TKGV,
                                            TKCK = lf.TKCK,
                                            TKTHUE = lf.TKTHUE,
                                            TKDUTHUE = lf.TKDUTHUE,
                                            TONKHO = lf.TONKHO
                                        }).ToList();
                return LF;
            }
        }

        public List<HOADON> GetHOADONs(Filter filters, string status)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                if (string.IsNullOrEmpty(filters.Datetime1.ToString()))
                {
                    if (filters.VOtext != "")
                    {
                        if (status == "HDB")
                        {
                            List<HOADON> HD = (from a in db.HOADONs
                                               where a.SOCT == filters.VOtext
                                               select a).ToList();
                            HD.Where(x => x.NGAYHT >= filters.Datetime1 && x.NGAYHT <= filters.datetime2 && (x.MALOAICT == "HDBCN" || x.MALOAICT == "HDBTM"));
                            return HD;
                        }
                        else if (status == "PXK")
                        {
                            List<HOADON> HD = (from a in db.HOADONs
                                               where a.SOCT == filters.VOtext
                                               select a).ToList();
                            HD.Where(x => x.NGAYHT >= filters.Datetime1 && x.NGAYHT <= filters.datetime2 && (x.MALOAICT == "PXK"));
                            return HD;
                        }
                        else if (status == "HDGG")
                        {
                            List<HOADON> HD = (from a in db.HOADONs
                                               where a.SOCT == filters.VOtext
                                               select a).ToList();
                            HD.Where(x => x.NGAYHT >= filters.Datetime1 && x.NGAYHT <= filters.datetime2 && (x.MALOAICT == "HDGG"));
                            return HD;
                        }
                        else if (status == "PNHBTL")
                        {
                            List<HOADON> HD = (from a in db.HOADONs
                                               where a.SOCT == filters.VOtext
                                               select a).ToList();
                            HD.Where(x => x.NGAYHT >= filters.Datetime1 && x.NGAYHT <= filters.datetime2 && (x.MALOAICT == "PNHBTL"));
                            return HD;
                        }
                        else
                        {
                            List<HOADON> HD = (from a in db.HOADONs
                                               where a.SOCT == filters.VOtext
                                               select a).ToList();
                            HD.Where(x => x.NGAYHT >= filters.Datetime1 && x.NGAYHT <= filters.datetime2);
                            return HD;

                        }


                    }
                    else
                    {
                        if (status == "HDB")
                        {

                            var DDH1 = db.HOADONs.ToList().Where(x => x.NGAYHT >= filters.Datetime1 && x.NGAYHT <= filters.datetime2 && (x.MALOAICT == "HDBCN" || x.MALOAICT == "HDBTM"));
                            return db.HOADONs.ToList();
                        }
                        else if (status == "PXK")
                        {

                            var DDH1 = db.HOADONs.ToList().Where(x => x.NGAYHT >= filters.Datetime1 && x.NGAYHT <= filters.datetime2 && (x.MALOAICT == "PXK"));
                            return db.HOADONs.ToList();
                        }
                        else if (status == "HDGG")
                        {

                            var DDH1 = db.HOADONs.ToList().Where(x => x.NGAYHT >= filters.Datetime1 && x.NGAYHT <= filters.datetime2 && (x.MALOAICT == "HDGG"));
                            return db.HOADONs.ToList();
                        }
                        else if (status == "PNHBTL")
                        {

                            var DDH1 = db.HOADONs.ToList().Where(x => x.NGAYHT >= filters.Datetime1 && x.NGAYHT <= filters.datetime2 && (x.MALOAICT == "PNHBTL"));
                            return db.HOADONs.ToList();
                        }
                        else
                        {

                            var DDH1 = db.HOADONs.ToList().Where(x => x.NGAYHT >= filters.Datetime1 && x.NGAYHT <= filters.datetime2);
                            return db.HOADONs.ToList();
                        }
                    }
                }
                else
                {

                    if (filters.VOtext != "")
                    {
                        if (status == "HDB")
                        {
                            List<HOADON> HD = (from a in db.HOADONs
                                               where a.SOCT == filters.VOtext
                                               && a.NGAYHT.Value.Year * 100 + a.NGAYHT.Value.Month >= filters.periodY1 * 100 + filters.periodM1
                                               && a.NGAYHT.Value.Year * 100 + a.NGAYHT.Value.Month <= filters.periodY2 * 100 + filters.periodM2
                                               && (a.MALOAICT == "HDBCN" || a.MALOAICT == "HDBTM")
                                               select a).ToList();

                            return HD;
                        }
                        else if (status == "PXK")
                        {
                            List<HOADON> HD = (from a in db.HOADONs
                                               where a.SOCT == filters.VOtext
                                               && a.NGAYHT.Value.Year * 100 + a.NGAYHT.Value.Month >= filters.periodY1 * 100 + filters.periodM1
                                               && a.NGAYHT.Value.Year * 100 + a.NGAYHT.Value.Month <= filters.periodY2 * 100 + filters.periodM2
                                               && (a.MALOAICT == "PXK")
                                               select a).ToList();

                            return HD;
                        }
                        else if (status == "HDGG")
                        {
                            List<HOADON> HD = (from a in db.HOADONs
                                               where a.SOCT == filters.VOtext
                                               && a.NGAYHT.Value.Year * 100 + a.NGAYHT.Value.Month >= filters.periodY1 * 100 + filters.periodM1
                                               && a.NGAYHT.Value.Year * 100 + a.NGAYHT.Value.Month <= filters.periodY2 * 100 + filters.periodM2
                                               && (a.MALOAICT == "HDGG")
                                               select a).ToList();
                            return HD;
                        }
                        else if (status == "PNHBTL")
                        {
                            List<HOADON> HD = (from a in db.HOADONs
                                               where a.SOCT == filters.VOtext
                                               && a.NGAYHT.Value.Year * 100 + a.NGAYHT.Value.Month >= filters.periodY1 * 100 + filters.periodM1
                                               && a.NGAYHT.Value.Year * 100 + a.NGAYHT.Value.Month <= filters.periodY2 * 100 + filters.periodM2
                                               && (a.MALOAICT == "PNHBTL")
                                               select a).ToList();
                            return HD;
                        }
                        else
                        {
                            List<HOADON> HD = (from a in db.HOADONs
                                               where a.SOCT == filters.VOtext
                                               && a.NGAYHT.Value.Year * 100 + a.NGAYHT.Value.Month >= filters.periodY1 * 100 + filters.periodM1
                                               && a.NGAYHT.Value.Year * 100 + a.NGAYHT.Value.Month <= filters.periodY2 * 100 + filters.periodM2

                                               select a).ToList();

                            return HD;
                        }

                    }
                    else
                    {
                        if (status == "HDB")
                        {
                            return db.HOADONs.Where(x => x.MALOAICT == "HDBCN" || x.MALOAICT == "HDBTM").ToList();
                        }
                        else if (status == "PXK")
                        {
                            return db.HOADONs.Where(x => x.MALOAICT == "PXK").ToList();
                        }
                        else if (status == "HDGG")
                        {
                            return db.HOADONs.Where(x => x.MALOAICT == "HDGG").ToList();
                        }
                        else if (status == "PNHBTL")
                        {
                            return db.HOADONs.Where(x => x.MALOAICT == "PNHBTL").ToList();
                        }
                        else
                        {
                            return db.HOADONs.ToList();
                        }

                    }
                }


            }
        }

        public List<HOADON> RefreshForm()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                return db.HOADONs.ToList();
            }
        }

        public List<Period> GetPeriods()
        {
            using (var db = new CLDLQLBH1Entities())
            {
                List<Period> listPeriod = (from q in db.HOADONs
                                           orderby q.NGAYHT
                                           select new Period
                                           {
                                               Month = q.NGAYHT.Value.Month,
                                               Year = q.NGAYHT.Value.Year,
                                               MY = q.NGAYHT.Value.Month + "/" + q.NGAYHT.Value.Year
                                           }).Distinct().ToList();
                return listPeriod;
            }

        }
        public HOADON FindVoucher(string _SODDH)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                HOADON _DDH = db.HOADONs.Find(_SODDH);
                return _DDH;
            }
        }
        public void removeHD(string _SOCT)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                var rmHD = db.HOADONs.Find(_SOCT);
                List<CTHOADON> rmctHD = db.CTHOADONs.Where(x => x.SOCT.Contains(_SOCT)).ToList();
                db.HOADONs.Remove(rmHD);
                db.CTHOADONs.RemoveRange(rmctHD);
                db.SaveChanges();
            }
        }
        public bool AllowEdit(string _SOCT)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                int _allow = (from CTHD in db.CTPHIEUTHUs
                              where CTHD.SOCT == _SOCT
                              select CTHD.SOCT).Count();
                if (_allow > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    }


}

