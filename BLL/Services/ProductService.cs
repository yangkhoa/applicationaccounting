﻿using DAL.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ProductService
    {
        private static ProductService instance;

        public static ProductService Instance
        {
            get { if (instance == null) instance = new ProductService(); return instance; }
            private set => instance = value;
        }

        private ProductService() { }

        public List<HANGHOA> GetListHangHoa()
        {
            using(var db = new CLDLQLBH1Entities())
            {
                return db.HANGHOAs.ToList();
            }
        }

        public HANGHOA GetHangHoaByMaHH(string maHH)
        {
            using(var db = new CLDLQLBH1Entities())
            {
                return db.HANGHOAs.FirstOrDefault(x => x.MAHH == maHH);
            }
        }

        public List<KHOHANG> GetListKho()
        {
            using(var db = new CLDLQLBH1Entities())
            {
                return db.KHOHANGs.ToList();
            }
        }

        public double getTonkho(string mahh,string makho)
        {
            using(var db = new CLDLQLBH1Entities())
            {
                var TONKHO = Convert.ToDouble((from x in db.TONKHOes
                                               where x.MAHH == mahh && x.MAKHO == makho
                                               select x.SOLUONGTON).FirstOrDefault());
                return TONKHO;
            }
        }

        public double getTongxuatkho(string mahh, string makho)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                double _CTHDs = Convert.ToDouble((from x in db.CTHOADONs
                                                  where x.MAHH == mahh && x.MAKHO == makho && x.SOCT.Contains("PXK")
                                                  select x.SOLUONG).Sum());
                return _CTHDs;
            }
        }

        public double getNhapkhotralai(string mahh, string makho)
        {
            using (var db = new CLDLQLBH1Entities())
            {
                double _CTHDs = Convert.ToDouble((from x in db.CTHOADONs
                                                  where x.MAHH == mahh && x.MAKHO == makho && x.SOCT.Contains("PNHBTL")
                                                  select x.SOLUONG).Sum());
                return _CTHDs;
            }
        }

        
    }
}
