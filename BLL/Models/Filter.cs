﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class Filter
    {
        public string VOtext { get; set; }
        public int periodM1 { get; set; }
        public int  periodM2 { get; set; }

        public int periodY1 { get; set; }
        public int periodY2 { get; set; }
        public DateTime Datetime1 { get; set; }
        public DateTime datetime2 { get; set; }
    }
}
