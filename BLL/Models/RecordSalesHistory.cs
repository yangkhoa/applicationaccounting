﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class RecordSalesHistory
    {
        [Display(Name = "Ngày tháng ghi sổ")]
        public DateTime NGAYGHISO { get; set; }
        [Display(Name = "Số hiệu")]
        public string SOCT { get; set; }
        [Display(Name = "Ngày tháng")]
        public DateTime NGAYLAPCT { get; set; }
        [Display(Name = "Diễn giải")]
        public string DIENGIAICT { get; set; }
        [Display(Name = "Phải thu từ người mua")]
        public double TONGTHANHTOAN { get; set; }
        [Display(Name = "Ghi có tk doanh thu")]
        public double HH { get; set; }
        [Display(Name = "Người lập phiếu")]
        public string MANV { get; set; }

    }
}
