﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class MessageModel
    {
        public string Message { get; set; }
        public MessageTypeEnum Type { get; set; }
        public dynamic ResultReturn { get; set; }
    }

    public enum MessageTypeEnum
    {
        Information = 1,
        Success = 2,
        Warning = 3,
        Error = 4,
    }
}
    