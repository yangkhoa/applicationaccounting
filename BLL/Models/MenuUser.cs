﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class MenuUser
    {
        [Display(Name = "Mã menu")]
        public string MANQ { get; set; }
        [Display(Name = "Tên menu")]
        public string TENNQ { get; set; }
        public int THEM { get; set; }
        public int XOA { get; set; }
        public int SUA { get; set; }
        public int IDCV { get; set; }
    }
}
