﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class LoadFormINV
    {
        public string SOCT { get; set; }
        public string TENHH { get; set; }
        public string MADVT { get; set; }
        public string MAHH { get; set; }
        public string MAKHO { get; set; }
        public Nullable<double> TONKHO { get; set; }
        public Nullable<double> SOLUONG { get; set; }
        public Nullable<double> DONGIA { get; set; }
        public Nullable<double> THANHTIEN { get; set; }
        public Nullable<double> TYLECK { get; set; }
        public Nullable<double> TIENCK { get; set; }
        public string MATHUE { get; set; }
        public Nullable<double> TIENTHUE { get; set; }
        public string TKDT { get; set; }
        public Nullable<double> GIAVON { get; set; }
        public Nullable<double> TIENVON { get; set; }
        public string TKGV { get; set; }
        public string TKVT { get; set; }
        public string TKCK { get; set; }

        public string TKTHUE { get; set; }
        public string TKDUTHUE { get; set; }

        public string SODDH { get; set; }


    }
}
