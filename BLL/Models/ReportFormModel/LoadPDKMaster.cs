﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ReportFormModel
{
    public class LoadPDKMaster
    {
        public string SOCT { get; set; }
        public string TENKH { get; set; }
        public DateTime NGAYLAPCT { get; set; }
        public string DIENGIAICT { get; set; }
        public string DIACHI { get; set; }
        public string MST { get; set; }
    }
}
