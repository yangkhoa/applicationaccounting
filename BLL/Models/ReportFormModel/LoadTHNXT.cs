﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ReportFormModel
{
    public class LoadTHNXT
    {
        public string MAHH { get; set; }
        public string TENHH { get; set; }
        public string DVT { get; set; }
        public string KHO { get; set; }
        public double SOLUONGTON { get; set; }
        public double SOLUONGNHAP { get; set; }
        public double TONGSOLUONG { get; set; }

        public double TONCUOIKY { get; set; }

    }
}
