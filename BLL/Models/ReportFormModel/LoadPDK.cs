﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ReportFormModel
{
    public class LoadPDK
    {
        public string TK { get; set; }
        public string MAKH { get; set; }
        public double PSNO { get; set; }
        public double PSCO { get; set; }
        public string TENTK { get; set; }
    }
}
