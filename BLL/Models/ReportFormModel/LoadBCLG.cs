﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ReportFormModel
{
    public class LoadBCLG
    {
        [Display(Name = "Loại Hàng")]
        public string LOAIHANG { get; set; }
        [Display(Name = "Mã hàng hóa")]
        public string MAHH { get; set; }
        [Display(Name = "Tên hàng hóa")]
        public string TENHH { get; set; }
        [Display(Name = "Đơn vị tính")]
        public string DVT { get; set; }
        [Display(Name = "Số lượng")]
        public double SL { get; set; }
        [Display(Name = "Doanh Thu")]
        public double DOANHTHU { get; set; }
        [Display(Name = "Giá vốn")]
        public double GIAVON { get; set; }
        [Display(Name = "Giảm giá")]
        public double GIAMGIA { get; set; }
        [Display(Name = "Lãi gộp")]
        public decimal LAIGOP { get; set; }
    }
}
