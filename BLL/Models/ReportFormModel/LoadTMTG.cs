﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ReportFormModel
{
    public class LoadTMTG
    {
        public string NGAYPTHU { get; set; }
        public string SOPTHU { get; set; }
        public string DIENGIAIPT { get; set; }
        public double TONGTIENPTHU { get; set; }
    }
}
