﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ReportFormModel
{
   public class LoadSC
    {
        public Nullable<DateTime> NGAYLAPCT { get; set; }
        public string SOPHIEU { get; set; }

        public string DIENGIAI { get; set; }
        public string TKDU { get; set; }
        public Nullable<double> PSNO { get; set; }
        public Nullable<double> PSCO { get; set; }
    }
}
