﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.ReportFormModel
{
    public class LoadCNPT
    {
        public string SOCT { get; set; }
        public DateTime NGAYLAPCT { get; set; }
        public string KYHIEUHD { get; set; }
        public string SOSERI { get; set; }
        public string DIENGIAICT { get; set; }
        public string TKDOIUNG { get; set; }
        public decimal PSNO { get; set; }
        public decimal PSCO { get; set; }
        public decimal SODUNO { get; set; }
        public decimal SODUCO { get; set; }
    }
}
