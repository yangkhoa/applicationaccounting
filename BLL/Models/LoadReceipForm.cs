﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class LoadReceipForm
    {
        public string SOCT { get; set; }
        public string MAKH { get; set; }
        public DateTime NGAYLAPCT { get; set; }
        public double THANHTIEN { get; set; }
        public double SOTIEN { get; set; }

        public string TKCOPT { get; set; }

    }
}
