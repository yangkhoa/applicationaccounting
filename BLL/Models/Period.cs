﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class Period
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public string MY { get; set; }
    }
}
