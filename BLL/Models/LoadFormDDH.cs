﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class LoadFormDDH
    {
        public string SODDH { get; set; }
        public string TENHH { get; set; }
        public string MADVT { get; set; }
        public string MAHH { get; set; }
        public string MAKHO { get; set; }
        public Nullable<double> SOLUONGDH { get; set; }
        public Nullable<double> DONGIADH { get; set; }
        public Nullable<double> THANHTIENDH { get; set; }
        public Nullable<double> TYLECK { get; set; }
        public Nullable<double> TIENCK { get; set; }
        public string MATHUE { get; set; }
        public Nullable<double> TIENTHUE { get; set; }
    }
}
