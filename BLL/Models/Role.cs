﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class Role
    {
        public int IDCV { get; set; }
        public string MACHUCVU { get; set; }
        public string TENCHUCVU { get; set; }
    }
}
