﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class RecordRevenue
    {
        [Display(Name = "Mã khách hàng")]
        public string MAKH { get; set; }
        [Display(Name = "Tên khách hàng")]
        public string TENKH { get; set; }
        [Display(Name = "Địa chỉ")]
        public string DIACHI { get; set; }
        [Display(Name = "Số điện thoại")]
        public string SDTKH { get; set; }
        [Display(Name = "Doanh thu bán hàng")]
        public double DOANHTHUBH { get; set; }
        [Display(Name = "Doanh thu thuần")]
        public double DOANHTHUTHUAN { get; set; }
        [Display(Name = "Mã số thuế")]
        public string MST { get; set; }
        [Display(Name = "Chiết khấu")]
        public double CK { get; set; }
    }
}
