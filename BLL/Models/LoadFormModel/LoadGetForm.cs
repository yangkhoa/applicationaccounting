﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models.LoadForm
{
    public class LoadGetForm
    {
        public bool checkID { get; set; }
        public string SOCT { get; set; }
        public string MANV { get; set; }
        public string MAKH { get; set; }
        public DateTime NGAYLAPCT { get; set; }

    }
}
