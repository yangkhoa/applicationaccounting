//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.DBContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class SODUDKTK
    {
        public string MATKC2 { get; set; }
        public int THANGSD { get; set; }
        public int NAMSD { get; set; }
        public Nullable<double> SOTIENNODK { get; set; }
        public Nullable<double> SOTIENCODK { get; set; }
        public string LOAITKSD { get; set; }
    
        public virtual TAIKHOANCHITIET TAIKHOANCHITIET { get; set; }
    }
}
